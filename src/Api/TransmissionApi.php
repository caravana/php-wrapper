<?php

namespace Caravana\Core\Api;


use Caravana\Core\Models\Requests\Contracts\GetTransmissionsRequest;
use Caravana\Core\Models\Responses\Collections\TransmissionCollection;
use Caravana\Core\Models\Responses\Transmission;
use Caravana\Core\Utilities\Data\TransmissionDataUtil;

class TransmissionApi extends BaseApi
{

    /**
     * @param   GetTransmissionsRequest|array       $request
     * @return  TransmissionCollection
     */
    public function index($request)
    {
        $this->tryValidation($request);

        $result                 = $this->api->get('transmissions', $this->serializeRequest($request));
        return new TransmissionCollection($result);
    }

    /**
     * @param   int         $id
     * @return  Transmission
     */
    public function show($id)
    {
        $result                 = $this->api->get('transmissions/' . $id);
        return new Transmission($result);
    }

    /**
     * @return  Transmission
     */
    public function getAutomatic()
    {
        return $this->show(TransmissionDataUtil::getAutomaticId());
    }

    /**
     * @return  Transmission
     */
    public function getManual()
    {
        return $this->show(TransmissionDataUtil::getManualId());
    }
}