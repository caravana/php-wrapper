<?php

namespace Caravana\Core\Api;


use Caravana\Core\Models\Requests\Contracts\GetVehicleModelsRequest;
use Caravana\Core\Models\Responses\Collections\VehicleModelCollection;
use Caravana\Core\Models\Responses\VehicleModel;

class VehicleModelApi extends BaseApi
{

    /**
     * @param   GetVehicleModelsRequest|array    $request
     * @return  VehicleModelCollection
     */
    public function index($request)
    {
        $this->tryValidation($request);

        $result                 = $this->api->get('vehicleModels', $this->serializeRequest($request));
        return new VehicleModelCollection($result);
    }

    /**
     * @param   $id
     * @return  VehicleModel
     */
    public function show($id)
    {
        $result                 = $this->api->get('vehicleMakes/' . $id);
        return new VehicleModel($result);
    }
    
}