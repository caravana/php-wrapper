<?php

namespace Caravana\Core\Api;


use Caravana\Core\Models\Responses\Collections\VehicleClassCollection;
use Caravana\Core\Models\Responses\VehicleClass;

class VehicleClassApi extends BaseApi
{

    /**
     * @param   GetVehicle|[]      $request
     * @return  VehicleClassCollection
     */
    public function index($request)
    {
        $this->tryValidation($request);

        $result                 = $this->api->get('vehicleClasses', $this->serializeRequest($request));
        return new VehicleClassCollection($result);
    }

    /**
     * @param   int         $id
     * @return  VehicleClass
     */
    public function show($id)
    {
        $result                 = $this->api->get('vehicleClasses/' . $id);
        return new VehicleClass($result);
    }
    
}