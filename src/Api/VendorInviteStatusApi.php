<?php

namespace Caravana\Core\Api;


use Caravana\Core\Models\Requests\Contracts\GetVendorInviteStatusesRequest;
use Caravana\Core\Models\Responses\Collections\VendorInviteStatusCollection;
use Caravana\Core\Models\Responses\VendorInviteStatus;

class VendorInviteStatusApi extends BaseApi
{

    /**
     * @param  GetVendorInviteStatusesRequest|array        $request
     * @return VendorInviteStatusCollection
     */
    public function index($request)
    {
        $this->tryValidation($request);

        $result                 = $this->api->get('vendorInviteStatuses', $this->serializeRequest($request));
        return new VendorInviteStatusCollection($result);
    }

    /**
     * @param   int     $id
     * @return  VendorInviteStatus
     */
    public function show($id)
    {
        $result                 = $this->api->get('vendorInviteStatuses/' . $id);
        return new VendorInviteStatus($result);
    }

}