<?php

namespace Caravana\Core\Api;


use Caravana\Core\Models\Requests\Contracts\CreateVehicleRequest;
use Caravana\Core\Models\Requests\Contracts\GetVehiclesRequest;
use Caravana\Core\Models\Responses\Collections\VehicleCollection;
use Caravana\Core\Models\Responses\Vehicle;

class VehicleApi extends BaseApi
{

    /**
     * @param  GetVehiclesRequest|array     $request
     * @return VehicleCollection
     */
    public function index($request)
    {
        $this->tryValidation($request);

        $result                 = $this->api->get('vehicles', $this->serializeRequest($request));
        return new VehicleCollection($result);
    }

    /**
     * @param   int     $id
     * @return  Vehicle
     */
    public function show($id)
    {
        $result                 = $this->api->get('vehicles/' . $id);
        return new Vehicle($result);
    }

    /**
     * @param   CreateVehicleRequest|array     $request
     * @return  Vehicle
     */
    public function store($request)
    {
        $this->tryValidation($request);

        $result                 = $this->api->post('vehicles', $this->serializeRequest($request));
        return new Vehicle($result);
    }

}