<?php

namespace Caravana\Core\Api;


use Caravana\API\Exceptions\UnauthorizedClientException;
use Caravana\Core\Models\Requests\Contracts\GetFuelTypesRequest;
use Caravana\Core\Models\Responses\Collections\FuelTypeCollection;
use Caravana\Core\Models\Responses\FuelType;
use Caravana\Core\Utilities\Data\FuelTypeDataUtil;

class FuelTypeApi extends BaseApi
{

    /**
     * @param   GetFuelTypesRequest|array   $request
     * @return  FuelTypeCollection
     * @throws  UnauthorizedClientException
     */
    public function index($request)
    {
        $this->tryValidation($request);
        
        try
        {
            $result                 = $this->api->get('fuelTypes', $this->serializeRequest($request));
            return new FuelTypeCollection($result);
        }
        catch (UnauthorizedClientException $ex)
        {
            throw $ex;
        }
    }

    /**
     * @param   int     $id
     * @return  FuelType
     * @throws  UnauthorizedClientException
     */
    public function show($id)
    {
        $result                 = $this->api->get('fuelTypes/' . $id);
        return new FuelType($result);
    }

    /**
     * @return  FuelType
     * @throws  UnauthorizedClientException
     */
    public function getDiesel()
    {
        return $this->show(FuelTypeDataUtil::getDieselId());
    }

    /**
     * @return  FuelType
     * @throws  UnauthorizedClientException
     */
    public function getGasoline()
    {
        return $this->show(FuelTypeDataUtil::getGasolineId());
    }
    
}