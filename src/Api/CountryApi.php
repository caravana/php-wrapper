<?php

namespace Caravana\Core\Api;


use Caravana\API\Exceptions\Factories\CaravanaExceptionFactory;
use Caravana\API\Exceptions\UnauthorizedClientException;
use Caravana\Core\Models\Requests\Contracts\GetCountriesRequest;
use Caravana\Core\Models\Responses\Collections\CountryCollection;
use Caravana\Core\Models\Responses\Country;
use Caravana\Core\Utilities\Data\CountryDataUtil;
use GuzzleHttp\Exception\RequestException;


class CountryApi extends BaseApi
{

    /**
     * @param   GetCountriesRequest|array   $request
     * @return  CountryCollection
     * @throws  UnauthorizedClientException
     */
    public function index($request)
    {
        $this->tryValidation($request);
        
        try 
        {
            $result                 = $this->api->get('countries', $this->serializeRequest($request));
            return new CountryCollection($result);
        }
        catch (UnauthorizedClientException $ex)
        {
            throw $ex;
        }
    }

    /**
     * @param   int     $id
     * @return  Country
     * @throws  UnauthorizedClientException
     */
    public function show($id)
    {
        try 
        {
            $result                 = $this->api->get('countries/' . $id);
            return new Country($result);
        }
        catch (UnauthorizedClientException $ex)
        {
            throw $ex;
        }
    }

    /**
     * @return  Country
     * @throws  UnauthorizedClientException
     */
    public function getUnitedStates()
    {
        return $this->show(CountryDataUtil::getUnitedStatesId());
    }
    
}