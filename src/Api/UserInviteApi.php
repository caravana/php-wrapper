<?php

namespace Caravana\Core\Api;


use Caravana\Core\Models\Requests\Contracts\CreateUserInviteRequest;
use Caravana\Core\Models\Requests\Contracts\GetUserInvitesRequest;
use Caravana\Core\Models\Responses\Collections\UserInviteCollection;
use Caravana\Core\Models\Responses\UserInvite;

class UserInviteApi extends BaseApi
{

    /**
     * @param  GetUserInvitesRequest|array  $request
     * @return UserInviteCollection
     */
    public function index($request)
    {
        $this->tryValidation($request);

        $result                 = $this->api->get('userInvites', $this->serializeRequest($request));
        return new UserInviteCollection($result);
    }

    /**
     * @param   int     $id
     * @return  UserInvite
     */
    public function show($id)
    {
        $result                 = $this->api->get('userInvites/' . $id);
        return new UserInvite($result);
    }

    /**
     * @param   CreateUserInviteRequest|array   $request
     * @return  UserInvite
     */
    public function store($request)
    {
        $this->tryValidation($request);

        $result                 = $this->api->post('userInvites', $request);
        return new UserInvite($result);
    }

}