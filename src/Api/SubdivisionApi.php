<?php

namespace Caravana\Core\Api;


use Caravana\Core\Models\Requests\Contracts\GetSubdivisionsRequest;
use Caravana\Core\Models\Responses\Collections\SubdivisionCollection;
use Caravana\Core\Models\Responses\Subdivision;

class SubdivisionApi extends BaseApi
{

    /**
     * @param  GetSubdivisionsRequest|array     $request
     * @return SubdivisionCollection
     */
    public function index($request)
    {
        $this->tryValidation($request);

        $result                 = $this->api->get('subdivisions', $this->serializeRequest($request));
        return new SubdivisionCollection($result);
    }

    /**
     * @param   int     $id
     * @return  Subdivision
     */
    public function show($id)
    {
        $result                 = $this->api->get('subdivisions/' . $id);
        return new Subdivision($result);
    }
    
}