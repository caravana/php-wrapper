<?php

namespace Caravana\Core\Api;


use Caravana\Core\Models\Requests\Contracts\GetVehicleTypesRequest;
use Caravana\Core\Models\Responses\Collections\VehicleTypeCollection;
use Caravana\Core\Models\Responses\VehicleType;

class VehicleTypeApi extends BaseApi
{

    /**
     * @param   GetVehicleTypesRequest|array    $request
     * @return  VehicleTypeCollection
     */
    public function index($request)
    {
        $this->tryValidation($request);

        $result                 = $this->api->get('vehicleTypes', $this->serializeRequest($request));
        return new VehicleTypeCollection($result);
    }

    /**
     * @param   $id
     * @return  VehicleType
     */
    public function show($id)
    {
        $result                 = $this->api->get('vehicleMakes/' . $id);
        return new VehicleType($result);
    }
    
}