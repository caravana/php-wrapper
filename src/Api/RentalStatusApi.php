<?php

namespace Caravana\Core\Api;


use Caravana\Core\Models\Requests\Contracts\GetRentalStatusesRequest;
use Caravana\Core\Models\Responses\Collections\RentalStatusCollection;
use Caravana\Core\Models\Responses\RentalStatus;

class RentalStatusApi extends BaseApi
{

    /**
     * @param   GetRentalStatusesRequest|array   $request
     * @return  RentalStatusCollection
     */
    public function index($request)
    {
        $this->tryValidation($request);

        $result                 = $this->api->get('rentalStatuses', $this->serializeRequest($request));
        return new RentalStatusCollection($result);
    }

    /**
     * @param   int                 $id
     * @return  RentalStatus
     */
    public function show($id)
    {
        $result                 = $this->api->get('rentalStatuses/' . $id);
        return new RentalStatus($result);
    }
    
}