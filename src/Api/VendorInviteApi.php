<?php

namespace Caravana\Core\Api;


use Caravana\Core\Models\Requests\Contracts\CreateVendorInviteRequest;
use Caravana\Core\Models\Requests\Contracts\GetVendorInvitesRequest;
use Caravana\Core\Models\Responses\Collections\VendorInviteCollection;
use Caravana\Core\Models\Responses\VendorInvite;

class VendorInviteApi extends BaseApi
{

    /**
     * @param  GetVendorInvitesRequest|array        $request
     * @return VendorInviteCollection
     */
    public function index($request)
    {
        $this->tryValidation($request);

        $result                 = $this->api->get('vendorInvites', $this->serializeRequest($request));
        return new VendorInviteCollection($result);
    }

    /**
     * @param   int     $id
     * @return  VendorInvite
     */
    public function show($id)
    {
        $result                 = $this->api->get('vendorInvites/' . $id);
        return new VendorInvite($result);
    }

    /**
     * @param   CreateVendorInviteRequest|array     $request
     * @return  VendorInvite
     */
    public function store($request)
    {
        $this->tryValidation($request);

        $result                 = $this->api->post('vendorInvites', $this->serializeRequest($request));
        return new VendorInvite($result);
    }

}