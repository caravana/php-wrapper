<?php

namespace Caravana\Core\Api;


use Caravana\Core\Models\Requests\Contracts\CreateRentalRequest;
use Caravana\Core\Models\Requests\Contracts\GetRentalsRequest;
use Caravana\Core\Models\Responses\Collections\RentalCollection;
use Caravana\Core\Models\Responses\Rental;

class RentalApi extends BaseApi
{

    /**
     * @param   GetRentalsRequest|array $request
     * @return  RentalCollection
     */
    public function index($request)
    {
        $this->tryValidation($request);

        $result                 = $this->api->get('rentals', $this->serializeRequest($request));
        return new RentalCollection($result);
    }

    /**
     * @param   int $request
     * @return  Rental
     */
    public function show($request)
    {
        $result                 = $this->api->get('rentals/' . $request);
        return new Rental($result);
    }

    /**
     * @param   CreateRentalRequest $request
     * @return  Rental
     */
    public function store($request)
    {
        $this->tryValidation($request);

        $result                 = $this->api->post('rentals', $this->serializeRequest($request));
        return new Rental($result);
    }

}