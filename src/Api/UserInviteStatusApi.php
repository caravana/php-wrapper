<?php

namespace Caravana\Core\Api;


use Caravana\Core\Models\Requests\Contracts\GetUserInviteStatusesRequest;
use Caravana\Core\Models\Responses\Collections\UserInviteStatusCollection;
use Caravana\Core\Models\Responses\UserInviteStatus;
use Caravana\Core\Utilities\Data\UserInviteStatusDataUtil;

class UserInviteStatusApi extends BaseApi
{

    /**
     * @param   GetUserInviteStatusesRequest|array   $request
     * @return  UserInviteStatusCollection
     */
    public function index($request)
    {
        $this->tryValidation($request);

        $result                 = $this->api->get('userInviteStatuses', $this->serializeRequest($request));
        return new UserInviteStatusCollection($result);
    }

    /**
     * @param   int                 $id
     * @return  UserInviteStatus
     */
    public function show($id)
    {
        $result                 = $this->api->get('userInviteStatuses/' . $id);
        return new UserInviteStatus($result);
    }

    /**
     * @return UserInviteStatus
     */
    public function getCreated()
    {
        return $this->show(UserInviteStatusDataUtil::getCreatedId());
    }

    /**
     * @return UserInviteStatus
     */
    public function getEmailSent()
    {
        return $this->show(UserInviteStatusDataUtil::getEmailSentId());
    }

    /**
     * @return UserInviteStatus
     */
    public function getTokenUsed()
    {
        return $this->show(UserInviteStatusDataUtil::getTokenUsedId());
    }

    /**
     * @return UserInviteStatus
     */
    public function getTokenExpired()
    {
        return $this->show(UserInviteStatusDataUtil::getTokenExpiredId());
    }

    /**
     * @return UserInviteStatus
     */
    public function getUserCreated()
    {
        return $this->show(UserInviteStatusDataUtil::getCreatedId());
    }
}