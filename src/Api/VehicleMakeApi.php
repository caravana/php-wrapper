<?php

namespace Caravana\Core\Api;


use Caravana\Core\Models\Requests\Contracts\GetVehicleMakesRequest;
use Caravana\Core\Models\Responses\Collections\VehicleMakeCollection;
use Caravana\Core\Models\Responses\VehicleMake;

class VehicleMakeApi extends BaseApi
{

    /**
     * @param   GetVehicleMakesRequest|array        $request
     * @return  VehicleMakeCollection
     */
    public function index($request)
    {
        $this->tryValidation($request);

        $result                 = $this->api->get('vehicleMakes', $this->serializeRequest($request));
        return new VehicleMakeCollection($result);
    }

    /**
     * @param   $id
     * @return  VehicleMake
     */
    public function show($id)
    {
        $result                 = $this->api->get('vehicleMakes/' . $id);
        return new VehicleMake($result);
    }
    
}