<?php

namespace Caravana\Core\Api;


use Caravana\Core\Models\Requests\Contracts\CreateVendorLocationRequest;
use Caravana\Core\Models\Requests\Contracts\CreateVendorRequest;
use Caravana\Core\Models\Requests\Contracts\GetVendorsRequest;
use Caravana\Core\Models\Responses\Collections\VendorCollection;
use Caravana\Core\Models\Responses\Collections\VendorLocationCollection;
use Caravana\Core\Models\Responses\Vendor;
use Caravana\Core\Models\Responses\VendorLocation;

class VendorApi extends BaseApi
{

    /**
     * @param  GetVendorsRequest|array        $request
     * @return VendorCollection
     */
    public function index($request)
    {
        $this->tryValidation($request);

        $result                 = $this->api->get('vendors', $this->serializeRequest($request));
        return new VendorCollection($result);
    }

    /**
     * @param   int     $id
     * @return  Vendor
     */
    public function show($id)
    {
        $result                 = $this->api->get('vendors/' . $id);
        return new Vendor($result);
    }

    /**
     * @param   CreateVendorRequest|array     $request
     * @return  Vendor
     */
    public function store($request)
    {
        $this->tryValidation($request);

        $result                 = $this->api->post('vendors', $this->serializeRequest($request));
        return new Vendor($result);
    }

    /**
     * @param   int                                 $vendorId
     * @param   CreateVendorLocationRequest|array         $request
     * @return  VendorLocation
     */
    public function addLocation($vendorId, $request)
    {
        $this->tryValidation($request);
        
        $result                 = $this->api->post('vendors/' . $vendorId . '/locations', $this->serializeRequest($request));
        return new VendorLocation($result);
    }

    /**
     * @param   int                                 $vendorId
     * @return  VendorLocation[]
     */
    public function getLocations($vendorId)
    {
        $result                 = $this->api->get('vendors/' . $vendorId . '/locations');
        
        $response               = [];
        foreach ($result AS $item)
        {
            $response[]         = new VendorLocation($item);
        }
        return $response;
    }

}