<?php

namespace Caravana\Core\Api;


use Caravana\API\Api;
use Caravana\Core\Models\Requests\Contracts\Validatable;

abstract class BaseApi
{

    /**
     * @var Api
     */
    protected $api;

    /**
     * @var bool
     */
    protected $validateRequests;

    /**
     * @param   Api     $api
     * @param   bool    $validateRequests
     */
    public function __construct(Api $api, $validateRequests = true)
    {
        $this->api                      = $api;
        $this->validateRequests         = $validateRequests;
    }

    /**
     * @param $payload
     */
    protected function tryValidation($payload)
    {
        if ($payload instanceof Validatable)
            $payload->validate();
    }

    /**
     * @param   \JsonSerializable|array     $request
     * @return  array
     */
    protected function serializeRequest($request)
    {
        return ($request instanceof \JsonSerializable) ? $request->jsonSerialize() : $request;
    }
}