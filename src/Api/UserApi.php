<?php

namespace Caravana\Core\Api;


use Caravana\Core\Models\Requests\Contracts\CreateCardRequest;
use Caravana\Core\Models\Requests\Contracts\CreateUserRequest;
use Caravana\Core\Models\Requests\Contracts\GetUsersRequest;
use Caravana\Core\Models\Requests\Contracts\UpdatePasswordRequest;
use Caravana\Core\Models\Responses\Card;
use Caravana\Core\Models\Responses\Collections\UserCollection;
use Caravana\Core\Models\Responses\User;
use Caravana\Core\Models\Responses\Vendor;

class UserApi extends BaseApi
{

    /**
     * @param  GetUsersRequest|array        $request
     * @return UserCollection
     */
    public function index($request)
    {
        $this->tryValidation($request);

        $result                 = $this->api->get('users', $this->serializeRequest($request));
        return new UserCollection($result);
    }

    /**
     * @param   int     $id
     * @return  User
     */
    public function show($id)
    {
        $result                 = $this->api->get('users/' . $id);
        return new User($result);
    }

    /**
     * @return User
     */
    public function me()
    {
        $result                 = $this->api->get('users/me');
        return new User($result);
    }

    /**
     * @param   CreateUserRequest|array     $request
     * @return  User
     */
    public function store($request)
    {
        $this->tryValidation($request);
        
        $result                 = $this->api->post('users', $this->serializeRequest($request));
        return new User($result);
    }

    /**
     * @param   int                         $id
     * @param   CreateUserRequest|array     $request
     * @return  User
     */
    public function update($id, $request)
    {
        $this->tryValidation($request);

        $result                 = $this->api->put('users/' . $id, $this->serializeRequest($request));
        return new User($result);
    }

    /**
     * @param   int                         $id
     * @param   UpdatePasswordRequest       $request
     * @return  bool
     */
    public function updatePassword($id, $request)
    {
        $this->tryValidation($request);

        $result                 = $this->api->put('users/' . $id . '/password', $this->serializeRequest($request));
        return $result;
    }

    /**
     * @param   int                         $userId
     * @param   CreateCardRequest           $request
     * @return  Card
     */
    public function storeCard($userId, $request)
    {
        $this->tryValidation($request);

        $result                 = $this->api->post('users/' . $userId . '/' . 'cards', $this->serializeRequest($request));
        return new Card($result);
    }

    /**
     * @param   int                         $userId
     * @return  Card[]
     */
    public function getCards($userId)
    {
        $result                 = $this->api->get('users/' . $userId . '/' . 'cards');

        $cards                  = [];
        foreach ($result AS $item)
            $cards[]            = new Card($item);

        return $cards;
    }

    /**
     * @param   int                         $id
     * @return  Vendor|null
     */
    public function getVendors($id)
    {
        $result                 = $this->api->get('users/' . $id . '/' . 'vendors');
        if (empty($result))
            return null;
        else
            return new Vendor($result);
    }
    
}