<?php

namespace Caravana\Core\Utilities\Data;


class TransmissionDataUtil
{

    /**
     * @return int
     */
    public static function getAutomaticId()
    {
        return 1;
    }

    /**
     * @return int
     */
    public static function getManualId()
    {
        return 2;
    }
    
}