<?php

namespace Caravana\Core\Utilities\Data;


class RVFeatureTypeDataUtil
{

    /**
     * @return int
     */
    public static function getGeneralId()
    {
        return 1;
    }

    /**
     * @return int
     */
    public static function getInteriorId()
    {
        return 2;
    }

    /**
     * @return int
     */
    public static function getExteriorId()
    {
        return 3;
    }

    /**
     * @return int
     */
    public static function getSpecialRequestId()
    {
        return 4;
    }

    /**
     * @return int
     */
    public static function getTankCapacitiesId()
    {
        return 5;
    }
    
    
}