<?php

namespace Caravana\Core\Utilities\Data;


class UserInviteStatusDataUtil
{

    /**
     * @return int
     */
    public static function getCreatedId()
    {
        return 1;
    }

    /**
     * @return int
     */
    public static function getEmailSentId()
    {
        return 2;
    }

    /**
     * @return int
     */
    public static function getTokenUsedId()
    {
        return 3;
    }

    /**
     * @return int
     */
    public static function getTokenExpiredId()
    {
        return 4;
    }

    /**
     * @return int
     */
    public static function getUserCreatedId()
    {
        return 5;
    }
}