<?php

namespace Caravana\Core\Utilities\Data;


class RentalStatusDataUtil
{

    /**
     * @return int
     */
    public static function getPendingId()
    {
        return 1;
    }

    /**
     * @return int
     */
    public static function getAvailableId()
    {
        return 2;
    }

    /**
     * @return int
     */
    public static function getInBookingId()
    {
        return 3;
    }
    
}