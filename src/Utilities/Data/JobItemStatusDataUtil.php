<?php

namespace Caravana\Core\Utilities\Data;


class JobItemStatusDataUtil
{

    /**
     * @return string
     */
    public static function getSuccess()
    {
        return 'Success';
    }

    /**
     * @return string
     */
    public static function getNotProcessed()
    {
        return 'Not Processed';
    }
    
}