<?php

namespace Caravana\Core\Utilities\Data;


class VendorStatusDataUtil
{

    /**
     * @return int
     */
    public static function getActiveId()
    {
        return 1;
    }

    /**
     * @return int
     */
    public static function getSuspendedId()
    {
        return 2;
    }

    /**
     * @return int
     */
    public static function getDeletedId()
    {
        return 3;
    }

}