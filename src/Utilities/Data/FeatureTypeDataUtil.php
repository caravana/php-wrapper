<?php

namespace Caravana\Core\Utilities\Data;


class FeatureTypeDataUtil
{

    /**
     * @return int
     */
    public static function getInteriorId()
    {
        return 1;
    }

    /**
     * @return int
     */
    public static function getExteriorId()
    {
        return 2;
    }
}