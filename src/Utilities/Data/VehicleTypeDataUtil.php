<?php

namespace Caravana\Core\Utilities\Data;


class VehicleTypeDataUtil
{

    /**
     * @return  int
     */
    public static function getRVId()
    {
        return 1;
    }

    /**
     * @return  int
     */
    public static function getCarId()
    {
        return 2;
    }

    /**
     * @return  int
     */
    public static function getSUVId()
    {
        return 3;
    }

    /**
     * @return  int
     */
    public static function getTruckId()
    {
        return 4;
    }

    /**
     * @return  int
     */
    public static function getVanId()
    {
        return 5;
    }
    
}