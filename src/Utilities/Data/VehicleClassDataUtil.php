<?php

namespace Caravana\Core\Utilities\Data;


class VehicleClassDataUtil
{

    /**
     * @return  int
     */
    public static function getAId()
    {
        return 1;
    }

    /**
     * @return  int
     */
    public static function getBId()
    {
        return 2;
    }

    /**
     * @return  int
     */
    public static function getCId()
    {
        return 3;
    }

    /**
     * @return  int
     */
    public static function getTravelTrailerId()
    {
        return 4;
    }

    /**
     * @return  int
     */
    public static function getFoldingTrailerId()
    {
        return 5;
    }

    /**
     * @return  int
     */
    public static function getFifthWheelId()
    {
        return 6;
    }

    /**
     * @return  int
     */
    public static function getToyHaulerId()
    {
        return 7;
    }

    /**
     * @return  int
     */
    public static function getTruckCamperId()
    {
        return 8;
    }
}