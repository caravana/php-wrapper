<?php

namespace Caravana\Core\Utilities\Data;


class VendorInviteStatusDataUtil
{

    /**
     * @return int
     */
    public static function getCreatedId()
    {
        return 1;
    }

    /**
     * @return int
     */
    public static function getEmailSentId()
    {
        return 2;
    }

    /**
     * @return int
     */
    public static function getDeniedId()
    {
        return 3;
    }

    /**
     * @return int
     */
    public static function getActiveId()
    {
        return 4;
    }

    /**
     * @return int
     */
    public static function getDeletedId()
    {
        return 5;
    }
    
}