<?php

namespace Caravana\Core\Utilities\Data;


class FuelTypeDataUtil
{

    /**
     * @return int
     */
    public static function getGasolineId()
    {
        return 1;
    }

    /**
     * @return int
     */
    public static function getDieselId()
    {
        return 2;
    }
    
}