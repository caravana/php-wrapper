<?php

namespace Caravana\Core\Utilities\Data;


class JobStatusDataUtil
{

    /**
     * @return int
     */
    public static function getCreatingId()
    {
        return 1;
    }

    /**
     * @return int
     */
    public static function getConvertingId()
    {
        return 2;
    }

    /**
     * @return int
     */
    public static function getValidatingId()
    {
        return 3;
    }

    /**
     * @return int
     */
    public static function getProcessingId()
    {
        return 4;
    }

    /**
     * @return int
     */
    public static function getQueuedId()
    {
        return 5;
    }

    /**
     * @return int
     */
    public static function getValidatingFailureId()
    {
        return 6;
    }

    /**
     * @return int
     */
    public static function getConversionFailureId()
    {
        return 7;
    }

    /**
     * @return int
     */
    public static function getProcessingFailureId()
    {
        return 8;
    }

    /**
     * @return int
     */
    public static function getPartialSuccessId()
    {
        return 9;
    }

    /**
     * @return int
     */
    public static function getSuccessId()
    {
        return 10;
    }

    /**
     * @return int
     */
    public static function getReproducingId()
    {
        return 11;
    }
    
}