<?php

namespace Caravana\Core\Validation;


use Caravana\API\Exceptions\EntityValidationException;
use Respect\Validation\Validator as v;

class BaseUserValidation
{

    /**
     * @param   $param
     * @return  string
     * @throws  EntityValidationException
     */
    public function firstName($param)
    {
        if (!v::notEmpty()->length(2, 100)->validate($param))
            throw new EntityValidationException('User', 'firstName', $param, 'length must be between 2 and 100');
        return $param;
    }

    /**
     * @param   $param
     * @return  string
     * @throws  EntityValidationException
     */
    public function lastName($param)
    {
        if (!v::notEmpty()->length(2, 100)->validate($param))
            throw new EntityValidationException('User', 'lastName', $param, 'length must be between 2 and 100');
        return $param;
    }

    /**
     * @param   $param
     * @return  string
     * @throws  EntityValidationException
     */
    public function email($param)
    {
        if (!v::email()->validate($param))
            throw new EntityValidationException('User', 'email', $param, 'Invalid email');
        return $param;
    }

    /**
     * @param   $param
     * @return  string
     * @throws  EntityValidationException
     */
    public function password($param)
    {
        if (!v::notEmpty()->length(2, 100)->validate($param))
            throw new EntityValidationException('User', 'password', $param, 'length must be between 2 and 100');
        return $param;
    }
    
}