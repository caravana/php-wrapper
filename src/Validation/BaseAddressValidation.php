<?php

namespace Caravana\Core\Validation;


use Caravana\API\Exceptions\EntityValidationException;
use Respect\Validation\Validator as v;

class BaseAddressValidation
{

    /**
     * @param   $param
     * @return  string
     * @throws  EntityValidationException
     */
    public function street1($param)
    {
        if (!v::notEmpty()->length(2, 100)->validate($param))
            throw new EntityValidationException('Address', 'street1', $param, 'length must be between 2 and 100');
        return $param;
    }

    /**
     * @param   $param
     * @return  string
     * @throws  EntityValidationException
     */
    public function street2($param)
    {
        if (is_null($param))
            return $param;
        else if (empty(trim($param)))
            return trim($param);
        else if (!v::notEmpty()->length(2, 100)->validate($param))
            throw new EntityValidationException('Address', 'street2', $param, 'length must be between 2 and 100');
        return $param;
    }

    /**
     * @param   $param
     * @return  string
     * @throws  EntityValidationException
     */
    public function city($param)
    {
        if (!v::notEmpty()->length(2, 100)->validate($param))
            throw new EntityValidationException('Address', 'city', $param, 'length must be between 2 and 100');
        return $param;
    }

    /**
     * @param   $param
     * @return  string
     * @throws  EntityValidationException
     */
    public function postalCode($param)
    {
        if (!v::notEmpty()->length(2, 100)->validate($param))
            throw new EntityValidationException('Address', 'postalCode', $param, 'length must be between 2 and 50');
        return $param;
    }
    
}