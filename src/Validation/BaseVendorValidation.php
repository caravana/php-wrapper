<?php

namespace Caravana\Core\Validation;


use Caravana\API\Exceptions\EntityValidationException;
use Respect\Validation\Validator as v;

class BaseVendorValidation
{

    /**
     * @param   $param
     * @return  string
     * @throws  EntityValidationException
     */
    public function name($param)
    {
        if (!v::notEmpty()->length(2, 100)->validate($param))
            throw new EntityValidationException('Vendor', 'name', $param, 'length must be between 2 and 100');
        return $param;
    }

    /**
     * @param   $param
     * @return  string
     * @throws  EntityValidationException
     */
    public function uniqueReference($param)
    {
        if (empty($param) || is_null($param))
            return $param;

        if (!v::notEmpty()->length(1, 100)->validate((string)$param))
            throw new EntityValidationException('Vendor', 'uniqueReference', $param, 'length must be between 1 and 100');
        return $param;
    }
    
    
}