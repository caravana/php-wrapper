<?php

namespace Caravana\Core\Validation;


use Caravana\API\Exceptions\EntityValidationException;
use Respect\Validation\Validator as v;

class BaseVehicleCSVFormatValidation
{

    /**
     * @param   $param
     * @return  string
     * @throws  EntityValidationException
     */
    public function vehicleTypeName($param)
    {
        if (!v::notEmpty()->length(1, 100)->validate($param))
            throw new EntityValidationException('VehicleCSV', 'vehicleTypeName', $param, 'length must be between 1 and 100');
        return $param;
    }

    /**
     * @param   $param
     * @return  string
     * @throws  EntityValidationException
     */
    public function vehicleMakeName($param)
    {
        if (!v::notEmpty()->length(1, 100)->validate($param))
            throw new EntityValidationException('VehicleCSV', 'vehicleMakeName', $param, 'length must be between 1 and 100');
        return $param;
    }

    /**
     * @param   $param
     * @return  string
     * @throws  EntityValidationException
     */
    public function vehicleModelName($param)
    {
        if (!v::notEmpty()->length(1, 100)->validate($param))
            throw new EntityValidationException('VehicleCSV', 'vehicleModelName', $param, 'length must be between 1 and 100');
        return $param;
    }

    /**
     * @param   $param
     * @return  string
     * @throws  EntityValidationException
     */
    public function vehicleClassName($param)
    {
        if (!v::notEmpty()->length(1, 100)->validate($param))
            throw new EntityValidationException('VehicleCSV', 'vehicleClassName', $param, 'length must be between 1 and 100');
        return $param;
    }

    /**
     * @param   $param
     * @return  string
     * @throws  EntityValidationException
     */
    public function fuelTypeName($param)
    {
        if (!v::notEmpty()->length(1, 100)->validate($param))
            throw new EntityValidationException('VehicleCSV', 'fuelTypeName', $param, 'length must be between 1 and 100');
        return $param;
    }

    /**
     * @param   $param
     * @return  string
     * @throws  EntityValidationException
     */
    public function yearCreated($param)
    {
        if (!v::numeric()->positive()->validate($param))
            throw new EntityValidationException('VehicleCSV', 'yearCreated', $param, 'must be positive integer');
        return $param;
    }

    /**
     * @param   $param
     * @return  string
     * @throws  EntityValidationException
     */
    public function feetLong($param)
    {
        if (!v::numeric()->positive()->validate($param))
            throw new EntityValidationException('VehicleCSV', 'feetLong', $param, 'must be positive integer');
        return $param;
    }
}