<?php

namespace Caravana\Core\Tests;


use Caravana\Core\CaravanaApi;

class SubdivisionApiTests extends \PHPUnit_Framework_TestCase
{

    public function testIndex()
    {
        $client                 = new CaravanaApi('./');
        $response               = $client->subdivisionApi()->index([]);
        $this->assertInstanceOf('Caravana\Core\Models\Responses\Collections\SubdivisionCollection', $response);

        foreach ($response->getData() AS $item)
        {
            $this->assertInstanceOf('Caravana\Core\Models\Responses\Subdivision', $item);
        }
    }


    public function testShow()
    {
        $client                 = new CaravanaApi('./');
        $response               = $client->subdivisionApi()->show(1);
        $this->assertInstanceOf('Caravana\Core\Models\Responses\Subdivision', $response);
    }
    
}