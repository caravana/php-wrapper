<?php

namespace Caravana\Core\Tests;


use Caravana\Core\CaravanaApi;

class VehicleMakeApiTests extends \PHPUnit_Framework_TestCase
{

    public function testIndex()
    {
        $client                 = new CaravanaApi('./');
        $result                 = $client->vehicleMakeApi()->index([]);
        $this->assertInstanceOf('Caravana\Core\Models\Responses\Collections\VehicleMakeCollection', $result);
        
        foreach ($result->getData() AS $item)
        {
            $this->assertInstanceOf('Caravana\Core\Models\Responses\VehicleMake', $item);
        }
        
    }
    
    public function testShow()
    {
        $client                 = new CaravanaApi('./');
        $result                 = $client->vehicleMakeApi()->show(1);
        $this->assertInstanceOf('Caravana\Core\Models\Responses\VehicleMake', $result);
    }
}