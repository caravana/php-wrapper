<?php

namespace Caravana\Core\Tests;


use Caravana\Core\CaravanaApi;
use Caravana\Core\Models\Requests\CreateUserRequest;
use Caravana\Core\Models\Requests\UpdatePasswordRequest;

class UserApiTests extends \PHPUnit_Framework_TestCase
{
    
    public function testIndex()
    {
        $client                 = new CaravanaApi('./');
        $result                 = $client->userApi()->index([]);
        
        $this->assertInstanceOf('Caravana\Core\Models\Responses\Collections\UserCollection', $result);
        
        if ($result->getTotal() > 1)
        {
            foreach ($result->getData() AS $user)
            {
                $this->assertInstanceOf('Caravana\Core\Models\Responses\User', $user);
            }
        }
    }
    
    public function testShow()
    {
        $client                 = new CaravanaApi('./');
        $user                   = $client->userApi()->show(1);

        $this->assertInstanceOf('Caravana\Core\Models\Responses\User', $user);
    }

    public function testStore()
    {
        $createUserRequest      = new CreateUserRequest();
        $createUserRequest->setFirstName('john');
        $createUserRequest->setLastName('doe');
        $password               = substr( md5(rand()), 0, 7);
        $createUserRequest->setPassword($password);
        $createUserRequest->setEmail(substr( md5(rand()), 0, 7) . '@whatever.com');

        $client                 = new CaravanaApi('./');
        $user                   = $client->userApi()->store($createUserRequest);
        $this->assertInstanceOf('Caravana\Core\Models\Responses\User', $user);
        
        $updatePasswordRequest  = new UpdatePasswordRequest();
        $updatePasswordRequest->setCurrentPassword($password);
        $updatePasswordRequest->setNewPassword('dl3ladj');
        
        $updatePasswordResult   = $client->userApi()->updatePassword($user->getId(), $updatePasswordRequest);
        
        $this->assertEquals(null, $updatePasswordResult);
    }
    
}