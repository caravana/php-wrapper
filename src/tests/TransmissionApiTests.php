<?php

namespace Caravana\Core\Tests;


use Caravana\Core\CaravanaApi;

class TransmissionApiTests extends \PHPUnit_Framework_TestCase
{

    public function testIndex()
    {
        $client                 = new CaravanaApi('./');
        $response               = $client->transmissionApi()->index([]);
        $this->assertInstanceOf('Caravana\Core\Models\Responses\Collections\TransmissionCollection', $response);
    }
    
    public function testShow()
    {
        $client                 = new CaravanaApi('./');
        $response               = $client->transmissionApi()->show(1);
        $this->assertInstanceOf('Caravana\Core\Models\Responses\Transmission', $response);
    }
    
    public function testAutomatic()
    {
        $client                 = new CaravanaApi('./');
        $response               = $client->transmissionApi()->getAutomatic();
        $this->assertInstanceOf('Caravana\Core\Models\Responses\Transmission', $response);
    }
    
    public function testGetManual()
    {
        $client                 = new CaravanaApi('./');
        $response               = $client->transmissionApi()->getManual();
        $this->assertInstanceOf('Caravana\Core\Models\Responses\Transmission', $response);
    }
}