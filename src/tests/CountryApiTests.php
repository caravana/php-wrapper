<?php

namespace Caravana\Core\Tests;


use Caravana\Core\CaravanaApi;
use Caravana\Core\Models\Requests\GetCountriesRequest;
use Caravana\Core\Utilities\Data\CountryDataUtil;

class CountryApiTests extends \PHPUnit_Framework_TestCase
{

    public function testIndexArray()
    {
        $client                 = new CaravanaApi('./');
        $response               = $client->countryApi()->index([]);
        $this->assertInstanceOf('Caravana\Core\Models\Responses\Collections\CountryCollection', $response);

        foreach ($response->getData() AS $item)
        {
            $this->assertInstanceOf('Caravana\Core\Models\Responses\Country', $item);
        }
    }
    
    public function testIndexObject()
    {
        $getCountriesRequest    = new GetCountriesRequest();
        $getCountriesRequest->setIds('1,2,3');
        $getCountriesRequest->setLimit(50);

        $client                 = new CaravanaApi('./');
        $response               = $client->countryApi()->index($getCountriesRequest);
        
        $this->assertInstanceOf('Caravana\Core\Models\Responses\Collections\CountryCollection', $response);

        foreach ($response->getData() AS $item)
        {
            $this->assertInstanceOf('Caravana\Core\Models\Responses\Country', $item);
        }
    }


    public function testShow()
    {
        $client                 = new CaravanaApi('./');
        $response               = $client->countryApi()->show(1);
        $this->assertInstanceOf('Caravana\Core\Models\Responses\Country', $response);
    }


    public function testUnitedStates()
    {
        $client                 = new CaravanaApi('./');
        $response               = $client->countryApi()->getUnitedStates();
        $this->assertInstanceOf('Caravana\Core\Models\Responses\Country', $response);
        $this->assertEquals($response->getId(), CountryDataUtil::getUnitedStatesId());
        $this->assertEquals($response->getName(), 'United States');
    }
}