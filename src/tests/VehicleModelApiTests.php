<?php

namespace Caravana\Core\Tests;


use Caravana\Core\CaravanaApi;

class VehicleModelApiTests extends \PHPUnit_Framework_TestCase
{
    
    public function testIndex()
    {
        $client                 = new CaravanaApi('./');
        $result                 = $client->vehicleModelApi()->index([]);
        $this->assertInstanceOf('Caravana\Core\Models\Responses\Collections\VehicleModelCollection', $result);
        
        foreach ($result->getData() AS $item)
        {
            $this->assertInstanceOf('Caravana\Core\Models\Responses\VehicleModel', $item);
        }
    }
    
    public function testShow()
    {
        $client                 = new CaravanaApi('./');
        $result                 = $client->vehicleModelApi()->show(1);

        $this->assertInstanceOf('Caravana\Core\Models\Responses\VehicleModel', $result);
    }
}