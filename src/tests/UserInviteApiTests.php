<?php

namespace Caravana\Core\Tests;


use Caravana\Core\CaravanaApi;

class UserInviteApiTests extends \PHPUnit_Framework_TestCase
{

    public function testIndex()
    {
        $client                 = new CaravanaApi('./');

        $response               = $client->userInviteApi()->index([]);
        $this->assertInstanceOf('Caravana\Core\Models\Responses\Collections\UserInviteCollection', $response);

        foreach ($response->getData() AS $item)
        {
            $this->assertInstanceOf('Caravana\Core\Models\Responses\UserInvite', $item);
        }
    }
    
}