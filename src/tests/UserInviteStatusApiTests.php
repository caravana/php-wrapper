<?php

namespace Caravana\Core\Tests;


use Caravana\Core\CaravanaApi;

class UserInviteStatusApiTests extends \PHPUnit_Framework_TestCase
{

    public function testIndex()
    {
        $client                 = new CaravanaApi('./');

        $response               = $client->userInviteStatusApi()->index([]);
        $this->assertInstanceOf('Caravana\Core\Models\Responses\Collections\UserInviteStatusCollection', $response);
    }
    
    public function testShow()
    {
        $client                 = new CaravanaApi('./');
        $response               = $client->userInviteStatusApi()->show(1);
        
        $this->assertInstanceOf('Caravana\Core\Models\Responses\UserInviteStatus', $response);
    }
    
    public function testGetCreated()
    {
        $client                 = new CaravanaApi('./');
        $response               = $client->userInviteStatusApi()->getCreated();
        $this->assertInstanceOf('Caravana\Core\Models\Responses\UserInviteStatus', $response);
        $this->assertEquals($response->getId(), 1);
        $this->assertEquals($response->getName(), 'Created');
    }
}