<?php

namespace Caravana\Core\Tests;


use Caravana\Core\CaravanaApi;

class VehicleClassApiTests extends \PHPUnit_Framework_TestCase
{

    public function testIndex()
    {
        $client                 = new CaravanaApi('./');
        $response               = $client->vehicleClassApi()->index([]);
        $this->assertInstanceOf('Caravana\Core\Models\Responses\Collections\VehicleClassCollection', $response);
    }
    
    public function testShow()
    {
        $client                 = new CaravanaApi('./');
        $response               = $client->vehicleClassApi()->show(1);
        $this->assertInstanceOf('Caravana\Core\Models\Responses\VehicleClass', $response);
    }
    
}