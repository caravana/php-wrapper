<?php

namespace Caravana\Core\Tests;


use Caravana\Core\CaravanaApi;

class VehicleTypeApiTests extends \PHPUnit_Framework_TestCase
{

    public function testIndex()
    {
        $client                 = new CaravanaApi('./');
        $result                 = $client->vehicleTypeApi()->index([]);
        $this->assertInstanceOf('Caravana\Core\Models\Responses\Collections\VehicleTypeCollection', $result);
        
        foreach ($result->getData() AS $item)
        {
            $this->assertInstanceOf('Caravana\Core\Models\Responses\VehicleType', $item);
        }
    }
    
    public function testShow()
    {
        $client                 = new CaravanaApi('./');
        $result                 = $client->vehicleTypeApi()->show(1);
        $this->assertInstanceOf('Caravana\Core\Models\Responses\VehicleType', $result);
    }
}