<?php

namespace Caravana\Core\Tests;


use Caravana\Core\CaravanaApi;
use Caravana\Core\Models\Requests\CreateVendorLocationRequest;
use Caravana\Core\Models\Requests\CreateVendorRequest;
use Caravana\Core\Utilities\Data\SubdivisionDataUtil;

class VendorApiTests extends \PHPUnit_Framework_TestCase
{

    public function testIndex()
    {
        $client                     = new CaravanaApi('./');
        $result                     = $client->vendorApi()->index([]);

        $this->assertInstanceOf('Caravana\Core\Models\Responses\Collections\VendorCollection', $result);

        if ($result->getTotal() > 1)
        {
            foreach ($result->getData() AS $vendor)
            {
                $this->assertInstanceOf('Caravana\Core\Models\Responses\Vendor', $vendor);
            }
        }
    }

    public function testShow()
    {
        $client                     = new CaravanaApi('./');
        $vendor                     = $client->vendorApi()->show(1);
        $this->assertInstanceOf('Caravana\Core\Models\Responses\Vendor', $vendor);
    }

    public function testStore()
    {
        $createVendorRequest        = new CreateVendorRequest();
        $createVendorRequest->setName(substr( md5(rand()), 0, 7));
        $createVendorRequest->setOwnerId(1);

        $client                     = new CaravanaApi('./');
        $vendor                     = $client->vendorApi()->store($createVendorRequest);
        $this->assertInstanceOf('Caravana\Core\Models\Responses\Vendor', $vendor);
    }
    
    public function testAddLocation()
    {
        $client                     = new CaravanaApi('./');
        
        //  Get the Vendor
        $vendor                     = $client->vendorApi()->show(1);
        $this->assertInstanceOf('Caravana\Core\Models\Responses\Vendor', $vendor);
        
        //  Create the Location
        $createLocationRequest      = new CreateVendorLocationRequest();

        $createLocationRequest->setReference1(substr( md5(rand()), 0, 7));
        $createLocationRequest->setCity('San Diego');
        $createLocationRequest->setPostalCode('92101');
        $createLocationRequest->setStreet1('111 W Harbor Dr');
        $createLocationRequest->setSubdivisionId(SubdivisionDataUtil::getCaliforniaId());
        
        $vendorLocation             = $client->vendorApi()->addLocation($vendor->getId(), $createLocationRequest);
        $this->assertInstanceOf('Caravana\Core\Models\Responses\VendorLocation', $vendorLocation);
    }
    
    public function testGetLocations()
    {
        $client                     = new CaravanaApi('./');
        $vendorLocations            = $client->vendorApi()->getLocations(1);
        
        foreach ($vendorLocations AS $item)
        {
            $this->assertInstanceOf('Caravana\Core\Models\Responses\VendorLocation', $item);
        }
    }
    
}