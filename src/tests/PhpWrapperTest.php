<?php

namespace Caravana\Core\Tests;


use Caravana\Core\CaravanaApi;
use Dotenv\Dotenv;

/**
 * @package Caravana\API\Tests
 */
class PhpWrapperTest extends \PHPUnit_Framework_TestCase
{
    
    public function testENVInstantiation()
    {
        $caravanaClient = new CaravanaApi('./');
        $this->assertInstanceOf('Caravana\Core\CaravanaApi', $caravanaClient);
    }
    
    public function testArrayInstantiation()
    {
        $dotEnv                         = new Dotenv('./');
        $dotEnv->load();

        $data = [
            'username'                  => getenv('CARAVANA_USERNAME'),
            'password'                  => getenv('CARAVANA_PASSWORD'),
            'clientId'                  => getenv('CARAVANA_CLIENT_ID'),
            'clientSecret'              => getenv('CARAVANA_CLIENT_SECRET'),
            'grantType'                 => getenv('CARAVANA_GRANT_TYPE'),
            'accessToken'               => getenv('CARAVANA_ACCESS_TOKEN'),
            'authEndPoint'              => getenv('CARAVANA_AUTH_ENDPOINT'),
            'apiEndPoint'               => getenv('CARAVANA_API_ENDPOINT'),
        ];

        $caravanaClient = new CaravanaApi($data);
        $this->assertInstanceOf('Caravana\Core\CaravanaApi', $caravanaClient);
    }
    
}