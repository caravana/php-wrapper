<?php

namespace Caravana\Core\Tests;


use Caravana\Core\CaravanaApi;

class FuelTypeApiTests extends \PHPUnit_Framework_TestCase
{

    public function testIndex()
    {
        $client                 = new CaravanaApi('./');
        $response               = $client->fuelTypeApi()->index([]);
        $this->assertInstanceOf('Caravana\Core\Models\Responses\Collections\FuelTypeCollection', $response);
    }
    
    
    public function testShow()
    {
        $client                 = new CaravanaApi('./');
        $response               = $client->fuelTypeApi()->show(1);
        $this->assertInstanceOf('Caravana\Core\Models\Responses\FuelType', $response);
    }
    
    public function testGetGasoline()
    {
        $client                 = new CaravanaApi('./');
        $response               = $client->fuelTypeApi()->getGasoline();
        $this->assertInstanceOf('Caravana\Core\Models\Responses\FuelType', $response);
    }
    
    public function testGetDiesel()
    {
        $client                 = new CaravanaApi('./');
        $response               = $client->fuelTypeApi()->getDiesel();
        $this->assertInstanceOf('Caravana\Core\Models\Responses\FuelType', $response);
    }
}