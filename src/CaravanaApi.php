<?php

namespace Caravana\Core;


use Caravana\API\Api;
use Caravana\API\Exceptions\Factories\CaravanaExceptionFactory;
use Caravana\API\Exceptions\RequiredFieldMissingException;
use Caravana\API\Exceptions\UnauthorizedClientException;
use Caravana\API\Models\Responses\OAuth\CreateAccessTokenResponse;
use Caravana\Core\Api\CountryApi;
use Caravana\Core\Api\FuelTypeApi;
use Caravana\Core\Api\RentalApi;
use Caravana\Core\Api\RentalStatusApi;
use Caravana\Core\Api\SubdivisionApi;
use Caravana\Core\Api\TransmissionApi;
use Caravana\Core\Api\UserApi;
use Caravana\Core\Api\UserInviteApi;
use Caravana\Core\Api\UserInviteStatusApi;
use Caravana\Core\Api\VehicleApi;
use Caravana\Core\Api\VehicleClassApi;
use Caravana\Core\Api\VehicleMakeApi;
use Caravana\Core\Api\VehicleModelApi;
use Caravana\Core\Api\VehicleTypeApi;
use Caravana\Core\Api\VendorApi;
use Caravana\Core\Api\VendorInviteApi;
use Caravana\Core\Api\VendorInviteStatusApi;
use GuzzleHttp\Exception\RequestException;

class CaravanaApi
{

    /**
     * @var Api
     */
    protected $api;

    /**
     * @var bool
     */
    protected $validateRequests;

    
    /**
     * @var CountryApi
     */
    protected $countryApi;

    /**
     * @var FuelTypeApi
     */
    protected $fuelTypeApi;

    /**
     * @var RentalApi
     */
    protected $rentalApi;

    /**
     * @var RentalStatusApi
     */
    protected $rentalStatusApi;

    /**
     * @var SubdivisionApi
     */
    protected $subdivisionApi;

    /**
     * @var TransmissionApi
     */
    protected $transmissionApi;

    /**
     * @var UserApi
     */
    protected $userApi;

    /**
     * @var UserInviteApi
     */
    protected $userInviteApi;

    /**
     * @var UserInviteStatusApi
     */
    protected $userInviteStatusApi;

    /**
     * @var VehicleApi
     */
    protected $vehicleApi;

    /**
     * @var VehicleClassApi
     */
    protected $vehicleClassApi;

    /**
     * @var VehicleMakeApi
     */
    protected $vehicleMakeApi;
    
    /**
     * @var VehicleModelApi
     */
    protected $vehicleModelApi;

    /**
     * @var VehicleTypeApi
     */
    protected $vehicleTypeApi;

    /**
     * @var VendorApi
     */
    protected $vendorApi;

    /**
     * @var VendorInviteApi
     */
    protected $vendorInviteApi;

    /**
     * @var VendorInviteStatusApi
     */
    protected $vendorInviteStatusApi;
    
    
    public function __construct($apiConfiguration, $validateRequests = true)
    {
        $this->api                  = new Api($apiConfiguration);
        $this->validateRequests     = $validateRequests;
    }

    /**
     * @param       bool            $returnResponse     If true the CreateAccessTokenResponse will be returned
     * @return      CreateAccessTokenResponse
     * @throws      RequiredFieldMissingException|UnauthorizedClientException
     */
    public function requestAccessToken($returnResponse = false)
    {
        try
        {
            $accessTokenResponse        = $this->api->requestAccessToken($returnResponse);

            if ($returnResponse)
                return $accessTokenResponse;
            else
                return null;
        }
        catch (RequestException $ex)
        {
            $caravanaException          = CaravanaExceptionFactory::parseRequestException($ex);
            throw $caravanaException;
        }

    }

    /**
     * @return CountryApi
     */
    public function countryApi()
    {
        if (is_null($this->countryApi))
            $this->countryApi       = new CountryApi($this->api, $this->validateRequests);
        
        return $this->countryApi;
    }

    /**
     * @return FuelTypeApi
     */
    public function fuelTypeApi()
    {
        if (is_null($this->fuelTypeApi))
            $this->fuelTypeApi       = new FuelTypeApi($this->api, $this->validateRequests);

        return $this->fuelTypeApi;
    }

    /**
     * @return RentalApi
     */
    public function rentalApi()
    {
        if (is_null($this->rentalApi))
            $this->rentalApi       = new RentalApi($this->api, $this->validateRequests);

        return $this->rentalApi;
    }

    /**
     * @return RentalStatusApi
     */
    public function rentalStatusApi()
    {
        if (is_null($this->rentalStatusApi))
            $this->rentalStatusApi       = new RentalStatusApi($this->api, $this->validateRequests);

        return $this->rentalStatusApi;
    }

    /**
     * @return SubdivisionApi
     */
    public function subdivisionApi()
    {
        if (is_null($this->subdivisionApi))
            $this->subdivisionApi       = new SubdivisionApi($this->api, $this->validateRequests);

        return $this->subdivisionApi;
    }

    /**
     * @return TransmissionApi
     */
    public function transmissionApi()
    {
        if (is_null($this->transmissionApi))
            $this->transmissionApi       = new TransmissionApi($this->api, $this->validateRequests);

        return $this->transmissionApi;
    }

    /**
     * @return UserApi
     */
    public function userApi()
    {
        if (is_null($this->userApi))
            $this->userApi       = new UserApi($this->api, $this->validateRequests);

        return $this->userApi;
    }

    /**
     * @return UserInviteApi
     */
    public function userInviteApi()
    {
        if (is_null($this->userInviteApi))
            $this->userInviteApi        = new UserInviteApi($this->api, $this->validateRequests);

        return $this->userInviteApi;
    }

    /**
     * @return UserInviteStatusApi
     */
    public function userInviteStatusApi()
    {
        if (is_null($this->userInviteStatusApi))
            $this->userInviteStatusApi  = new UserInviteStatusApi($this->api, $this->validateRequests);

        return $this->userInviteStatusApi;
    }

    /**
     * @return VehicleApi
     */
    public function vehicleApi()
    {
        if (is_null($this->vehicleApi))
            $this->vehicleApi           = new VehicleApi($this->api, $this->validateRequests);

        return $this->vehicleApi;
    }

    /**
     * @return VehicleClassApi
     */
    public function vehicleClassApi()
    {
        if (is_null($this->vehicleClassApi))
            $this->vehicleClassApi       = new VehicleClassApi($this->api, $this->validateRequests);

        return $this->vehicleClassApi;
    }

    /**
     * @return VehicleMakeApi
     */
    public function vehicleMakeApi()
    {
        if (is_null($this->vehicleMakeApi))
            $this->vehicleMakeApi       = new VehicleMakeApi($this->api, $this->validateRequests);

        return $this->vehicleMakeApi;
    }

    /**
     * @return VehicleModelApi
     */
    public function vehicleModelApi()
    {
        if (is_null($this->vehicleModelApi))
            $this->vehicleModelApi       = new VehicleModelApi($this->api, $this->validateRequests);

        return $this->vehicleModelApi;
    }

    /**
     * @return VehicleTypeApi
     */
    public function vehicleTypeApi()
    {
        if (is_null($this->vehicleTypeApi))
            $this->vehicleTypeApi       = new VehicleTypeApi($this->api, $this->validateRequests);

        return $this->vehicleTypeApi;
    }

    /**
     * @return VendorApi
     */
    public function vendorApi()
    {
        if (is_null($this->vendorApi))
            $this->vendorApi            = new VendorApi($this->api, $this->validateRequests);

        return $this->vendorApi;
    }

    /**
     * @return VendorInviteApi
     */
    public function vendorInviteApi()
    {
        if (is_null($this->vendorInviteApi))
            $this->vendorInviteApi      = new VendorInviteApi($this->api, $this->validateRequests);

        return $this->vendorInviteApi;
    }

    /**
     * @return VendorInviteStatusApi
     */
    public function vendorInviteStatusApi()
    {
        if (is_null($this->vendorInviteStatusApi))
            $this->vendorInviteStatusApi    = new VendorInviteStatusApi($this->api, $this->validateRequests);

        return $this->vendorInviteStatusApi;
    }
    
}