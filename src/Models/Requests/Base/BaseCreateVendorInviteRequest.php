<?php

namespace Caravana\Core\Models\Requests\Base;


use Caravana\Core\Models\Requests\Contracts\CreateVendorInviteRequest AS CreateVendorInviteRequestContract;

abstract class BaseCreateVendorInviteRequest implements CreateVendorInviteRequestContract
{

    /**
     * @var int
     */
    protected $vendorId;

    /**
     * @var int
     */
    protected $userId;

    /**
     * @return int
     */
    public function getVendorId()
    {
        return $this->vendorId;
    }

    /**
     * @param int $vendorId
     */
    public function setVendorId($vendorId)
    {
        $this->vendorId = $vendorId;
    }

    /**
     * @return int
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * @param int $userId
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;
    }
    
}