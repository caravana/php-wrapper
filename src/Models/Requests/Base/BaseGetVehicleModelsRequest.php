<?php

namespace Caravana\Core\Models\Requests\Base;


use Caravana\Core\Models\Requests\Contracts\GetVehicleModelsRequest AS GetVehicleModelsRequestContract;

abstract class BaseGetVehicleModelsRequest implements GetVehicleModelsRequestContract
{

    /**
     * @var string|null
     */
    protected $ids;

    /**
     * @var string|null
     */
    protected $names;

    /**
     * @var string|null
     */
    protected $vehicleMakeIds;
    
    /**
     * @var int
     */
    protected $limit;

    /**
     * @var int
     */
    protected $page;

    
    /**
     * @return null|string
     */
    public function getIds()
    {
        return $this->ids;
    }

    /**
     * @param null|string $ids
     */
    public function setIds($ids)
    {
        $this->ids = $ids;
    }

    /**
     * @return null|string
     */
    public function getNames()
    {
        return $this->names;
    }

    /**
     * @param null|string $names
     */
    public function setNames($names)
    {
        $this->names = $names;
    }

    /**
     * @return null|string
     */
    public function getVehicleMakeIds()
    {
        return $this->vehicleMakeIds;
    }

    /**
     * @param null|string $vehicleMakeIds
     */
    public function setVehicleMakeIds($vehicleMakeIds)
    {
        $this->vehicleMakeIds = $vehicleMakeIds;
    }

    /**
     * @return int
     */
    public function getLimit()
    {
        return $this->limit;
    }

    /**
     * @param int $limit
     */
    public function setLimit($limit)
    {
        $this->limit = $limit;
    }

    /**
     * @return int
     */
    public function getPage()
    {
        return $this->page;
    }

    /**
     * @param int $page
     */
    public function setPage($page)
    {
        $this->page = $page;
    }
    
}