<?php

namespace Caravana\Core\Models\Requests\Base;


use Caravana\Core\Models\Requests\Contracts\GetVendorsRequest AS GetVendorsRequestContract;

abstract class BaseGetVendorsRequest implements GetVendorsRequestContract
{

    /**
     * String of comma separated integers or a single integer
     * @var string|int|null
     */
    protected $ids;

    /**
     * String of comma separated names
     * @var string|null
     */
    protected $names;

    /**
     * String of User ids
     * @var string|int|null
     */
    protected $ownerIds;

    /**
     * Defaults to 80
     * @var int
     */
    protected $limit;

    /**
     * Defaults to 1
     * @var int
     */
    protected $page;

    /**
     * @return int|null|string
     */
    public function getIds()
    {
        return $this->ids;
    }

    /**
     * @param int|null|string $ids
     */
    public function setIds($ids)
    {
        $this->ids = $ids;
    }

    /**
     * @return null|string
     */
    public function getNames()
    {
        return $this->names;
    }

    /**
     * @param null|string $names
     */
    public function setNames($names)
    {
        $this->names = $names;
    }

    /**
     * @return int|null|string
     */
    public function getOwnerIds()
    {
        return $this->ownerIds;
    }

    /**
     * @param int|null|string $ownerIds
     */
    public function setOwnerIds($ownerIds)
    {
        $this->ownerIds = $ownerIds;
    }

    /**
     * @return int
     */
    public function getLimit()
    {
        return $this->limit;
    }

    /**
     * @param int $limit
     */
    public function setLimit($limit)
    {
        $this->limit = $limit;
    }

    /**
     * @return int
     */
    public function getPage()
    {
        return $this->page;
    }

    /**
     * @param int $page
     */
    public function setPage($page)
    {
        $this->page = $page;
    }
    
}