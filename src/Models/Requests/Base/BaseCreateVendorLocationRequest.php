<?php

namespace Caravana\Core\Models\Requests\Base;


use Caravana\Core\Models\Requests\Contracts\CreateVendorLocationRequest AS CreateVendorLocationRequestContract;

abstract class BaseCreateVendorLocationRequest extends BaseCreateLocationRequest implements CreateVendorLocationRequestContract
{

    /**
     * @var string|null
     */
    protected $reference1;

    /**
     * @return string
     */
    public function getReference1()
    {
        return $this->reference1;
    }

    /**
     * @param string|null $reference1
     */
    public function setReference1($reference1)
    {
        $this->reference1 = $reference1;
    }
    
}