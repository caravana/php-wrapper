<?php

namespace Caravana\Core\Models\Requests\Base;


use Caravana\Core\Models\Requests\Contracts\CreateVehicleFeatureRequest AS CreateVehicleFeatureRequestContract;

abstract class BaseCreateVehicleFeatureRequest implements CreateVehicleFeatureRequestContract
{

    /**
     * @var int
     */
    protected $featureId;

    /**
     * @var string
     */
    protected $value;

    /**
     * @return int
     */
    public function getFeatureId()
    {
        return $this->featureId;
    }

    /**
     * @param int $featureId
     */
    public function setFeatureId($featureId)
    {
        $this->featureId = $featureId;
    }

    /**
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param string $value
     */
    public function setValue($value)
    {
        $this->value = $value;
    }

}