<?php

namespace Caravana\Core\Models\Requests\Base;


use Caravana\Core\Models\Requests\Contracts\GetCountriesRequest AS GetCountriesRequestContract;

abstract class BaseGetCountriesRequest implements GetCountriesRequestContract
{
    
    /**
     * String of comma separated integers or a single integer
     * @var string|int
     */
    protected $ids;

    /**
     * String of comma separated names
     * @var string
     */
    protected $names;

    /**
     * String of comma separated iso2s
     * @var string
     */
    protected $iso2s;

    /**
     * String of comma separated iso3s
     * @var string
     */
    protected $iso3s;

    /**
     * Defaults to 80
     * @var int
     */
    protected $limit;

    /**
     * Defaults to 1
     * @var int
     */
    protected $page;


    /**
     * @return int|string
     */
    public function getIds()
    {
        return $this->ids;
    }

    /**
     * @param int|string $ids
     */
    public function setIds($ids)
    {
        $this->ids = $ids;
    }

    /**
     * @return string
     */
    public function getNames()
    {
        return $this->names;
    }

    /**
     * @param string $names
     */
    public function setNames($names)
    {
        $this->names = $names;
    }

    /**
     * @return string
     */
    public function getIso2s()
    {
        return $this->iso2s;
    }

    /**
     * @param string $iso2s
     */
    public function setIso2s($iso2s)
    {
        $this->iso2s = $iso2s;
    }

    /**
     * @return string
     */
    public function getIso3s()
    {
        return $this->iso3s;
    }

    /**
     * @param string $iso3s
     */
    public function setIso3s($iso3s)
    {
        $this->iso3s = $iso3s;
    }

    /**
     * @return int
     */
    public function getLimit()
    {
        return $this->limit;
    }

    /**
     * @param int $limit
     */
    public function setLimit($limit)
    {
        $this->limit = $limit;
    }

    /**
     * @return int
     */
    public function getPage()
    {
        return $this->page;
    }

    /**
     * @param int $page
     */
    public function setPage($page)
    {
        $this->page = $page;
    }
    
}