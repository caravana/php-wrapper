<?php

namespace Caravana\Core\Models\Requests\Base;


use Caravana\Core\Models\Requests\Contracts\GetVendorLocationsRequest AS GetVendorLocationsRequestContract;

abstract class BaseGetVendorLocationsRequest implements GetVendorLocationsRequestContract
{

    /**
     * @var string|int|null
     */
    protected $ids;

    /**
     * @var string|null
     */
    protected $labels;

    /**
     * @var string|int|null
     */
    protected $vendorIds;

    /**
     * @var int
     */
    protected $limit;

    /**
     * @var int
     */
    protected $page;

    /**
     * @return int|null|string
     */
    public function getIds()
    {
        return $this->ids;
    }

    /**
     * @param int|null|string $ids
     */
    public function setIds($ids)
    {
        $this->ids = $ids;
    }

    /**
     * @return null|string
     */
    public function getLabels()
    {
        return $this->labels;
    }

    /**
     * @param null|string $labels
     */
    public function setLabels($labels)
    {
        $this->labels = $labels;
    }

    /**
     * @return int|null|string
     */
    public function getVendorIds()
    {
        return $this->vendorIds;
    }

    /**
     * @param int|null|string $vendorIds
     */
    public function setVendorIds($vendorIds)
    {
        $this->vendorIds = $vendorIds;
    }

    /**
     * @return int
     */
    public function getLimit()
    {
        return $this->limit;
    }

    /**
     * @param int $limit
     */
    public function setLimit($limit)
    {
        $this->limit = $limit;
    }

    /**
     * @return int
     */
    public function getPage()
    {
        return $this->page;
    }

    /**
     * @param int $page
     */
    public function setPage($page)
    {
        $this->page = $page;
    }
    
}