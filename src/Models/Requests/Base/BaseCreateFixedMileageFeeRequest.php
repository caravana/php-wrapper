<?php

namespace Caravana\Core\Models\Requests\Base;


use Caravana\Core\Models\Requests\Contracts\CreateFixedMileageFeeRequest AS CreateFixedMileageFeeRequestContract;

abstract class BaseCreateFixedMileageFeeRequest implements CreateFixedMileageFeeRequestContract
{

    /**
     * @var int
     */
    protected $freeMiles;

    /**
     * @var float
     */
    protected $mileageFee;

    /**
     * @return mixed
     */
    public function getFreeMiles()
    {
        return $this->freeMiles;
    }

    /**
     * @param mixed $freeMiles
     */
    public function setFreeMiles($freeMiles)
    {
        $this->freeMiles = $freeMiles;
    }

    /**
     * @return mixed
     */
    public function getMileageFee()
    {
        return $this->mileageFee;
    }

    /**
     * @param mixed $mileageFee
     */
    public function setMileageFee($mileageFee)
    {
        $this->mileageFee = $mileageFee;
    }

}