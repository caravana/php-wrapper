<?php

namespace Caravana\Core\Models\Requests\Base;


use Caravana\Core\Models\Requests\Contracts\CreateConsignedVehicleRequest AS CreateConsignedVehicleRequestContract;

abstract class BaseCreateConsignedVehicleRequest implements CreateConsignedVehicleRequestContract
{

    /**
     * @var int
     */
    protected $vendorId;

    /**
     * @var int
     */
    protected $bookingPolicyId;

    /**
     * @var int
     */
    protected $cancellationPolicyId;

    /**
     * @var int|null
     */
    protected $fixedMileageFeeId;

    /**
     * @var int
     */
    protected $vehicleTypeId;

    /**
     * @var int
     */
    protected $vehicleClassId;

    /**
     * @var float
     */
    protected $rate;

    /**
     * @var string
     */
    protected $name;

    /**
     * @var string
     */
    protected $description;

    /**
     * @return int
     */
    public function getVendorId()
    {
        return $this->vendorId;
    }

    /**
     * @param int $vendorId
     */
    public function setVendorId($vendorId)
    {
        $this->vendorId = $vendorId;
    }

    /**
     * @return int
     */
    public function getBookingPolicyId()
    {
        return $this->bookingPolicyId;
    }

    /**
     * @param int $bookingPolicyId
     */
    public function setBookingPolicyId($bookingPolicyId)
    {
        $this->bookingPolicyId = $bookingPolicyId;
    }

    /**
     * @return int
     */
    public function getCancellationPolicyId()
    {
        return $this->cancellationPolicyId;
    }

    /**
     * @param int $cancellationPolicyId
     */
    public function setCancellationPolicyId($cancellationPolicyId)
    {
        $this->cancellationPolicyId = $cancellationPolicyId;
    }

    /**
     * @return int|null
     */
    public function getFixedMileageFeeId()
    {
        return $this->fixedMileageFeeId;
    }

    /**
     * @param int|null $fixedMileageFeeId
     */
    public function setFixedMileageFeeId($fixedMileageFeeId)
    {
        $this->fixedMileageFeeId = $fixedMileageFeeId;
    }

    /**
     * @return int
     */
    public function getVehicleTypeId()
    {
        return $this->vehicleTypeId;
    }

    /**
     * @param int $vehicleTypeId
     */
    public function setVehicleTypeId($vehicleTypeId)
    {
        $this->vehicleTypeId = $vehicleTypeId;
    }

    /**
     * @return int
     */
    public function getVehicleClassId()
    {
        return $this->vehicleClassId;
    }

    /**
     * @param int $vehicleClassId
     */
    public function setVehicleClassId($vehicleClassId)
    {
        $this->vehicleClassId = $vehicleClassId;
    }

    /**
     * @return float
     */
    public function getRate()
    {
        return $this->rate;
    }

    /**
     * @param float $rate
     */
    public function setRate($rate)
    {
        $this->rate = $rate;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

}