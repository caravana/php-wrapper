<?php

namespace Caravana\Core\Models\Requests\Base;


use Caravana\Core\Models\Requests\Contracts\CreateBankAccountRequest AS CreateBankAccountRequestContract;

abstract class BaseCreateBankAccountRequest implements CreateBankAccountRequestContract
{

    /**
     * @var string
     */
    protected $accountNumber;

    /**
     * @var string
     */
    protected $accountHolderName;

    /**
     * This can be either "individual" or "company"
     * @var string
     */
    protected $accountHolderType;

    /**
     * @var string
     */
    protected $routingNumber;

    /**
     * @var int
     */
    protected $countryId;

    /**
     * @return string
     */
    public function getAccountNumber()
    {
        return $this->accountNumber;
    }

    /**
     * @param string $accountNumber
     */
    public function setAccountNumber($accountNumber)
    {
        $this->accountNumber = $accountNumber;
    }

    /**
     * @return string
     */
    public function getAccountHolderName()
    {
        return $this->accountHolderName;
    }

    /**
     * @param string $accountHolderName
     */
    public function setAccountHolderName($accountHolderName)
    {
        $this->accountHolderName = $accountHolderName;
    }

    /**
     * @return string
     */
    public function getAccountHolderType()
    {
        return $this->accountHolderType;
    }

    /**
     * @param string $accountHolderType
     */
    public function setAccountHolderType($accountHolderType)
    {
        $this->accountHolderType = $accountHolderType;
    }

    /**
     * @return string
     */
    public function getRoutingNumber()
    {
        return $this->routingNumber;
    }

    /**
     * @param string $routingNumber
     */
    public function setRoutingNumber($routingNumber)
    {
        $this->routingNumber = $routingNumber;
    }

    /**
     * @return int
     */
    public function getCountryId()
    {
        return $this->countryId;
    }

    /**
     * @param int $countryId
     */
    public function setCountryId($countryId)
    {
        $this->countryId = $countryId;
    }

}