<?php

namespace Caravana\Core\Models\Requests\Base;


use Caravana\Core\Models\Requests\Contracts\CreateVehicleRequest AS CreateVehicleRequestContract;

abstract class BaseCreateVehicleRequest implements CreateVehicleRequestContract
{

    /**
     * @var int|null
     */
    protected $vendorId;
    
    /**
     * @var int
     */
    protected $vehicleTypeId;

    /**
     * @var int
     */
    protected $vendorLocationId;

    /**
     * @var int
     */
    protected $vehicleModelId;

    /**
     * @var int
     */
    protected $vehicleClassId;

    /**
     * @var int|null
     */
    protected $transmissionId;

    /**
     * @var int
     */
    protected $fuelTypeId;

    /**
     * @var int|null
     */
    protected $accommodates;

    /**
     * @var int
     */
    protected $yearCreated;

    /**
     * @var int
     */
    protected $feetLong;

    /**
     * @var string|null
     */
    protected $vin;

    /**
     * @var string|null
     */
    protected $uniqueReference;

    /**
     * @var string|null
     */
    protected $reference1;

    /**
     * @var string|null
     */
    protected $reference2;

    /**
     * @var string|null
     */
    protected $reference3;

    /**
     * @var string|null
     */
    protected $reference4;

    /**
     * @var string|null
     */
    protected $reference5;

    /**
     * @return int|null
     */
    public function getVendorId()
    {
        return $this->vendorId;
    }

    /**
     * @param int|null $vendorId
     */
    public function setVendorId($vendorId)
    {
        $this->vendorId = $vendorId;
    }
    
    /**
     * @return int
     */
    public function getVehicleTypeId()
    {
        return $this->vehicleTypeId;
    }

    /**
     * @param int $vehicleTypeId
     */
    public function setVehicleTypeId($vehicleTypeId)
    {
        $this->vehicleTypeId = $vehicleTypeId;
    }

    /**
     * @return int
     */
    public function getVendorLocationId()
    {
        return $this->vendorLocationId;
    }

    /**
     * @param int $vendorLocationId
     */
    public function setVendorLocationId($vendorLocationId)
    {
        $this->vendorLocationId = $vendorLocationId;
    }

    /**
     * @return int
     */
    public function getVehicleModelId()
    {
        return $this->vehicleModelId;
    }

    /**
     * @param int $vehicleModelId
     */
    public function setVehicleModelId($vehicleModelId)
    {
        $this->vehicleModelId = $vehicleModelId;
    }

    /**
     * @return int
     */
    public function getVehicleClassId()
    {
        return $this->vehicleClassId;
    }

    /**
     * @param int $vehicleClassId
     */
    public function setVehicleClassId($vehicleClassId)
    {
        $this->vehicleClassId = $vehicleClassId;
    }

    /**
     * @return int|null
     */
    public function getTransmissionId()
    {
        return $this->transmissionId;
    }

    /**
     * @param int|null $transmissionId
     */
    public function setTransmissionId($transmissionId)
    {
        $this->transmissionId = $transmissionId;
    }

    /**
     * @return int
     */
    public function getFuelTypeId()
    {
        return $this->fuelTypeId;
    }

    /**
     * @param int $fuelTypeId
     */
    public function setFuelTypeId($fuelTypeId)
    {
        $this->fuelTypeId = $fuelTypeId;
    }

    /**
     * @return int|null
     */
    public function getAccommodates()
    {
        return $this->accommodates;
    }

    /**
     * @param int|null $accommodates
     */
    public function setAccommodates($accommodates)
    {
        $this->accommodates = $accommodates;
    }

    /**
     * @return int
     */
    public function getYearCreated()
    {
        return $this->yearCreated;
    }

    /**
     * @param int $yearCreated
     */
    public function setYearCreated($yearCreated)
    {
        $this->yearCreated = $yearCreated;
    }

    /**
     * @return int
     */
    public function getFeetLong()
    {
        return $this->feetLong;
    }

    /**
     * @param int $feetLong
     */
    public function setFeetLong($feetLong)
    {
        $this->feetLong = $feetLong;
    }

    /**
     * @return null|string
     */
    public function getVin()
    {
        return $this->vin;
    }

    /**
     * @param null|string $vin
     */
    public function setVin($vin)
    {
        $this->vin = $vin;
    }

    /**
     * @return null|string
     */
    public function getUniqueReference()
    {
        return $this->uniqueReference;
    }

    /**
     * @param null|string $uniqueReference
     */
    public function setUniqueReference($uniqueReference)
    {
        $this->uniqueReference = $uniqueReference;
    }

    /**
     * @return null|string
     */
    public function getReference1()
    {
        return $this->reference1;
    }

    /**
     * @param null|string $reference1
     */
    public function setReference1($reference1)
    {
        $this->reference1 = $reference1;
    }

    /**
     * @return null|string
     */
    public function getReference2()
    {
        return $this->reference2;
    }

    /**
     * @param null|string $reference2
     */
    public function setReference2($reference2)
    {
        $this->reference2 = $reference2;
    }

    /**
     * @return null|string
     */
    public function getReference3()
    {
        return $this->reference3;
    }

    /**
     * @param null|string $reference3
     */
    public function setReference3($reference3)
    {
        $this->reference3 = $reference3;
    }

    /**
     * @return null|string
     */
    public function getReference4()
    {
        return $this->reference4;
    }

    /**
     * @param null|string $reference4
     */
    public function setReference4($reference4)
    {
        $this->reference4 = $reference4;
    }

    /**
     * @return null|string
     */
    public function getReference5()
    {
        return $this->reference5;
    }

    /**
     * @param null|string $reference5
     */
    public function setReference5($reference5)
    {
        $this->reference5 = $reference5;
    }

}