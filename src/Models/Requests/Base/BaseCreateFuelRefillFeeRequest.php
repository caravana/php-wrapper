<?php

namespace Caravana\Core\Models\Requests\Base;


use Caravana\Core\Models\Requests\Contracts\CreateFuelRefillFeeRequest AS CreateFuelRefillFeeRequestContract;

abstract class BaseCreateFuelRefillFeeRequest implements CreateFuelRefillFeeRequestContract
{

    /**
     * @var float
     */
    protected $perGallonFee;

    /**
     * @return float
     */
    public function getPerGallonFee()
    {
        return $this->perGallonFee;
    }

    /**
     * @param float $perGallonFee
     */
    public function setPerGallonFee($perGallonFee)
    {
        $this->perGallonFee = $perGallonFee;
    }

}