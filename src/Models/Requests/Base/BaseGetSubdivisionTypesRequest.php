<?php

namespace Caravana\Core\Models\Requests\Base;



use Caravana\Core\Models\Requests\Contracts\GetSubdivisionTypesRequest AS GetSubdivisionTypesRequestContract;

abstract class BaseGetSubdivisionTypesRequest implements GetSubdivisionTypesRequestContract
{

    /**
     * String of comma separated integers or a single integer
     * @var string|int
     */
    protected $ids;

    /**
     * String of comma separated names
     * @var string
     */
    protected $names;

    /**
     * Defaults to 80
     * @var int
     */
    protected $limit;

    /**
     * Defaults to 1
     * @var int
     */
    protected $page;


    /**
     * @return int|string
     */
    public function getIds()
    {
        return $this->ids;
    }

    /**
     * @param int|string $ids
     */
    public function setIds($ids)
    {
        $this->ids = $ids;
    }

    /**
     * @return string
     */
    public function getNames()
    {
        return $this->names;
    }

    /**
     * @param string $names
     */
    public function setNames($names)
    {
        $this->names = $names;
    }

    /**
     * @return int
     */
    public function getLimit()
    {
        return $this->limit;
    }

    /**
     * @param int $limit
     */
    public function setLimit($limit)
    {
        $this->limit = $limit;
    }

    /**
     * @return int
     */
    public function getPage()
    {
        return $this->page;
    }

    /**
     * @param int $page
     */
    public function setPage($page)
    {
        $this->page = $page;
    }
    
}