<?php

namespace Caravana\Core\Models\Requests\Base;


use Caravana\Core\Models\Requests\Contracts\CreateVendorRequest AS CreateVendorRequestContract;

abstract class BaseCreateVendorRequest implements CreateVendorRequestContract
{

    /**
     * @var string
     */
    protected $name;

    /**
     * @var int|null
     */
    protected $ownerId;

    /**
     * @var int|null
     */
    protected $bookingPolicyId;

    /**
     * @var int|null
     */
    protected $cancellationPolicyId;

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return int|null
     */
    public function getOwnerId()
    {
        return $this->ownerId;
    }

    /**
     * @param int|null $ownerId
     */
    public function setOwnerId($ownerId)
    {
        $this->ownerId = $ownerId;
    }

    /**
     * @return int|null
     */
    public function getBookingPolicyId()
    {
        return $this->bookingPolicyId;
    }

    /**
     * @param int|null $bookingPolicyId
     */
    public function setBookingPolicyId($bookingPolicyId)
    {
        $this->bookingPolicyId = $bookingPolicyId;
    }

    /**
     * @return int|null
     */
    public function getCancellationPolicyId()
    {
        return $this->cancellationPolicyId;
    }

    /**
     * @param int|null $cancellationPolicyId
     */
    public function setCancellationPolicyId($cancellationPolicyId)
    {
        $this->cancellationPolicyId = $cancellationPolicyId;
    }
    
}