<?php

namespace Caravana\Core\Models\Requests\Base;


use Caravana\Core\Models\Requests\Contracts\CreateAddOnRequest AS CreateAddOnRequestContract;

abstract class BaseCreateAddOnRequest implements CreateAddOnRequestContract
{

    /**
     * @var string
     */
    protected $name;

    /**
     * @var string
     */
    protected $description;

    /**
     * @var int
     */
    protected $vendorId;

    /**
     * @var float
     */
    protected $rate;

    /**
     * @var int
     */
    protected $rateTypeId;

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return int
     */
    public function getVendorId()
    {
        return $this->vendorId;
    }

    /**
     * @param int $vendorId
     */
    public function setVendorId($vendorId)
    {
        $this->vendorId = $vendorId;
    }

    /**
     * @return float
     */
    public function getRate()
    {
        return $this->rate;
    }

    /**
     * @param float $rate
     */
    public function setRate($rate)
    {
        $this->rate = $rate;
    }

    /**
     * @return int
     */
    public function getRateTypeId()
    {
        return $this->rateTypeId;
    }

    /**
     * @param int $rateTypeId
     */
    public function setRateTypeId($rateTypeId)
    {
        $this->rateTypeId = $rateTypeId;
    }

}