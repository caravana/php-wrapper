<?php

namespace Caravana\Core\Models\Requests\Base;


use Caravana\Core\Models\Requests\Contracts\UpdatePasswordRequest AS UpdatePasswordRequestContract;

abstract class BaseUpdatePasswordRequest implements UpdatePasswordRequestContract
{

    /**
     * @var string
     */
    protected $currentPassword;

    /**
     * @var string
     */
    protected $newPassword;

    /**
     * @return string
     */
    public function getCurrentPassword()
    {
        return $this->currentPassword;
    }

    /**
     * @param string $currentPassword
     */
    public function setCurrentPassword($currentPassword)
    {
        $this->currentPassword = $currentPassword;
    }

    /**
     * @return string
     */
    public function getNewPassword()
    {
        return $this->newPassword;
    }

    /**
     * @param string $newPassword
     */
    public function setNewPassword($newPassword)
    {
        $this->newPassword = $newPassword;
    }
    
}