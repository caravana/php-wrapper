<?php

namespace Caravana\Core\Models\Requests\Base;


use Caravana\Core\Models\Requests\Contracts\GetVehiclesRequest AS GetVehiclesRequestContract;

abstract class BaseGetVehiclesRequest implements GetVehiclesRequestContract
{

    /**
     * @var int|string|null
     */
    protected $ids;

    /**
     * @var int|string|null
     */
    protected $vehicleTypeIds;

    /**
     * @var int|string|null
     */
    protected $vehicleClassIds;

    /**
     * @var int|string|null
     */
    protected $vehicleModelIds;

    /**
     * @var int|string|null
     */
    protected $vehicleMakeIds;

    /**
     * @var int|string|null
     */
    protected $vendorIds;

    /**
     * @var bool|null
     */
    protected $inRental;

    /**
     * @var int|string|null
     */
    protected $uniqueReferences;

    /**
     * @var int
     */
    protected $limit;

    /**
     * @var int
     */
    protected $page;

    /**
     * @return int|null|string
     */
    public function getIds()
    {
        return $this->ids;
    }

    /**
     * @param int|null|string $ids
     */
    public function setIds($ids)
    {
        $this->ids = $ids;
    }

    /**
     * @return int|null|string
     */
    public function getVehicleTypeIds()
    {
        return $this->vehicleTypeIds;
    }

    /**
     * @param int|null|string $vehicleTypeIds
     */
    public function setVehicleTypeIds($vehicleTypeIds)
    {
        $this->vehicleTypeIds = $vehicleTypeIds;
    }

    /**
     * @return int|null|string
     */
    public function getVehicleClassIds()
    {
        return $this->vehicleClassIds;
    }

    /**
     * @param int|null|string $vehicleClassIds
     */
    public function setVehicleClassIds($vehicleClassIds)
    {
        $this->vehicleClassIds = $vehicleClassIds;
    }

    /**
     * @return int|null|string
     */
    public function getVehicleModelIds()
    {
        return $this->vehicleModelIds;
    }

    /**
     * @param int|null|string $vehicleModelIds
     */
    public function setVehicleModelIds($vehicleModelIds)
    {
        $this->vehicleModelIds = $vehicleModelIds;
    }

    /**
     * @return int|null|string
     */
    public function getVehicleMakeIds()
    {
        return $this->vehicleMakeIds;
    }

    /**
     * @param int|null|string $vehicleMakeIds
     */
    public function setVehicleMakeIds($vehicleMakeIds)
    {
        $this->vehicleMakeIds = $vehicleMakeIds;
    }

    /**
     * @return int|null|string
     */
    public function getVendorIds()
    {
        return $this->vendorIds;
    }

    /**
     * @param int|null|string $vendorIds
     */
    public function setVendorIds($vendorIds)
    {
        $this->vendorIds = $vendorIds;
    }

    /**
     * @return bool|null
     */
    public function getInRental()
    {
        return $this->inRental;
    }

    /**
     * @param bool|null $inRental
     */
    public function setInRental($inRental)
    {
        $this->inRental = $inRental;
    }

    /**
     * @return int|null|string
     */
    public function getUniqueReferences()
    {
        return $this->uniqueReferences;
    }

    /**
     * @param int|null|string $uniqueReferences
     */
    public function setUniqueReferences($uniqueReferences)
    {
        $this->uniqueReferences = $uniqueReferences;
    }

    /**
     * @return int
     */
    public function getLimit()
    {
        return $this->limit;
    }

    /**
     * @param int $limit
     */
    public function setLimit($limit)
    {
        $this->limit = $limit;
    }

    /**
     * @return int
     */
    public function getPage()
    {
        return $this->page;
    }

    /**
     * @param int $page
     */
    public function setPage($page)
    {
        $this->page = $page;
    }
    
}