<?php

namespace Caravana\Core\Models\Requests\Base;


use Caravana\Core\Models\Requests\Contracts\CreateSeasonalRateRequest AS CreateSeasonalRateRequestContract;

abstract class BaseCreateSeasonalRateRequest implements CreateSeasonalRateRequestContract
{

    /**
     * @var int
     */
    protected $rentalId;

    /**
     * @var int
     */
    protected $seasonId;

    /**
     * @var float
     */
    protected $dailyRate;

    /**
     * @var bool
     */
    protected $applyDurationDiscount;

    /**
     * @return int
     */
    public function getRentalId()
    {
        return $this->rentalId;
    }

    /**
     * @param int $rentalId
     */
    public function setRentalId($rentalId)
    {
        $this->rentalId = $rentalId;
    }

    /**
     * @return int
     */
    public function getSeasonId()
    {
        return $this->seasonId;
    }

    /**
     * @param int $seasonId
     */
    public function setSeasonId($seasonId)
    {
        $this->seasonId = $seasonId;
    }

    /**
     * @return float
     */
    public function getDailyRate()
    {
        return $this->dailyRate;
    }

    /**
     * @param float $dailyRate
     */
    public function setDailyRate($dailyRate)
    {
        $this->dailyRate = $dailyRate;
    }

    /**
     * @return bool
     */
    public function getApplyDurationDiscount()
    {
        return $this->applyDurationDiscount;
    }

    /**
     * @param bool $applyDurationDiscount
     */
    public function setApplyDurationDiscount($applyDurationDiscount)
    {
        $this->applyDurationDiscount = $applyDurationDiscount;
    }

}