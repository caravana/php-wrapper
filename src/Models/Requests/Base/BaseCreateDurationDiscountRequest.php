<?php

namespace Caravana\Core\Models\Requests\Base;


use Caravana\Core\Models\Requests\Contracts\CreateDurationDiscountRequest AS CreateDurationDiscountRequestContract;

abstract class BaseCreateDurationDiscountRequest implements CreateDurationDiscountRequestContract
{

    /**
     * @var int
     */
    protected $minimumDays;

    /**
     * @var float
     */
    protected $percentDiscount;

    /**
     * @var int|null
     */
    protected $rentalId;

    /**
     * @return mixed
     */
    public function getMinimumDays()
    {
        return $this->minimumDays;
    }

    /**
     * @param mixed $minimumDays
     */
    public function setMinimumDays($minimumDays)
    {
        $this->minimumDays = $minimumDays;
    }

    /**
     * @return mixed
     */
    public function getPercentDiscount()
    {
        return $this->percentDiscount;
    }

    /**
     * @param mixed $percentDiscount
     */
    public function setPercentDiscount($percentDiscount)
    {
        $this->percentDiscount = $percentDiscount;
    }

    /**
     * @return int|null
     */
    public function getRentalId()
    {
        return $this->rentalId;
    }

    /**
     * @param int|null $rentalId
     */
    public function setRentalId($rentalId)
    {
        $this->rentalId = $rentalId;
    }

}