<?php

namespace Caravana\Core\Models\Requests\Base;


use Caravana\Core\Models\Requests\Contracts\GetRentalsRequest AS GetRentalsRequestContract;

abstract class BaseGetRentalsRequest implements GetRentalsRequestContract
{

    /**
     * @var string|int|null
     */
    protected $ids;

    /**
     * @var bool|null
     */
    protected $published;

    /**
     * @var string|int|null
     */
    protected $vendorIds;

    /**
     * Formatted as YYYY-MM-DD
     * @var string|null
     */
    protected $fromDate;

    /**
     * Formatted as YYYY-MM-DD
     * @var string|null
     */
    protected $toDate;

    /**
     * @var string|int|null
     */
    protected $vehicleIds;

    /**
     * @var string|int|null
     */
    protected $vehicleTypeIds;

    /**
     * @var string|int|null
     */
    protected $vehicleClassIds;

    /**
     * @var string|int|null
     */
    protected $vehicleMakeIds;

    /**
     * @var string|int|null
     */
    protected $vehicleModelIds;

    /**
     * @var string|int|null
     */
    protected $fuelTypeIds;

    /**
     * @var string|int|null
     */
    protected $transmissionIds;
    
    /**
     * @var int|null
     */
    protected $limit;

    /**
     * @var int|null
     */
    protected $page;

    /**
     * @return int|null|string
     */
    public function getIds()
    {
        return $this->ids;
    }

    /**
     * @param int|null|string $ids
     */
    public function setIds($ids)
    {
        $this->ids = $ids;
    }

    /**
     * @return bool|null
     */
    public function getPublished()
    {
        return $this->published;
    }

    /**
     * @param bool|null $published
     */
    public function setPublished($published)
    {
        $this->published = $published;
    }

    /**
     * @return int|null|string
     */
    public function getVendorIds()
    {
        return $this->vendorIds;
    }

    /**
     * @param int|null|string $vendorIds
     */
    public function setVendorIds($vendorIds)
    {
        $this->vendorIds = $vendorIds;
    }

    /**
     * @return null|string
     */
    public function getFromDate()
    {
        return $this->fromDate;
    }

    /**
     * @param null|string $fromDate
     */
    public function setFromDate($fromDate)
    {
        $this->fromDate = $fromDate;
    }

    /**
     * @return null|string
     */
    public function getToDate()
    {
        return $this->toDate;
    }

    /**
     * @param null|string $toDate
     */
    public function setToDate($toDate)
    {
        $this->toDate = $toDate;
    }

    /**
     * @return int|null|string
     */
    public function getVehicleIds()
    {
        return $this->vehicleIds;
    }

    /**
     * @param int|null|string $vehicleIds
     */
    public function setVehicleIds($vehicleIds)
    {
        $this->vehicleIds = $vehicleIds;
    }

    /**
     * @return int|null|string
     */
    public function getVehicleTypeIds()
    {
        return $this->vehicleTypeIds;
    }

    /**
     * @param int|null|string $vehicleTypeIds
     */
    public function setVehicleTypeIds($vehicleTypeIds)
    {
        $this->vehicleTypeIds = $vehicleTypeIds;
    }

    /**
     * @return int|null|string
     */
    public function getVehicleClassIds()
    {
        return $this->vehicleClassIds;
    }

    /**
     * @param int|null|string $vehicleClassIds
     */
    public function setVehicleClassIds($vehicleClassIds)
    {
        $this->vehicleClassIds = $vehicleClassIds;
    }

    /**
     * @return int|null|string
     */
    public function getVehicleMakeIds()
    {
        return $this->vehicleMakeIds;
    }

    /**
     * @param int|null|string $vehicleMakeIds
     */
    public function setVehicleMakeIds($vehicleMakeIds)
    {
        $this->vehicleMakeIds = $vehicleMakeIds;
    }

    /**
     * @return int|null|string
     */
    public function getVehicleModelIds()
    {
        return $this->vehicleModelIds;
    }

    /**
     * @param int|null|string $vehicleModelIds
     */
    public function setVehicleModelIds($vehicleModelIds)
    {
        $this->vehicleModelIds = $vehicleModelIds;
    }

    /**
     * @return int|null|string
     */
    public function getFuelTypeIds()
    {
        return $this->fuelTypeIds;
    }

    /**
     * @param int|null|string $fuelTypeIds
     */
    public function setFuelTypeIds($fuelTypeIds)
    {
        $this->fuelTypeIds = $fuelTypeIds;
    }

    /**
     * @return int|null|string
     */
    public function getTransmissionIds()
    {
        return $this->transmissionIds;
    }

    /**
     * @param int|null|string $transmissionIds
     */
    public function setTransmissionIds($transmissionIds)
    {
        $this->transmissionIds = $transmissionIds;
    }

    /**
     * @return int|null
     */
    public function getLimit()
    {
        return $this->limit;
    }

    /**
     * @param int|null $limit
     */
    public function setLimit($limit)
    {
        $this->limit = $limit;
    }

    /**
     * @return int|null
     */
    public function getPage()
    {
        return $this->page;
    }

    /**
     * @param int|null $page
     */
    public function setPage($page)
    {
        $this->page = $page;
    }
    
}