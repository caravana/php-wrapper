<?php

namespace Caravana\Core\Models\Requests\Base;


use Caravana\Core\Models\Requests\Contracts\CreateRentalRequest AS CreateRentalRequestContract;

abstract class BaseCreateRentalRequest implements CreateRentalRequestContract
{

    /**
     * @var int
     */
    protected $vehicleId;

    /**
     * @var int
     */
    protected $bookingPolicyId;

    /**
     * @var int
     */
    protected $cancellationPolicyId;

    /**
     * @var int|null
     */
    protected $fixedMileageFeeId;

    /**
     * @var int|null
     */
    protected $fuelRefillFeeId;

    /**
     * @var string|null
     */
    protected $name;

    /**
     * @var string|null
     */
    protected $description;

    /**
     * @var float|null
     */
    protected $rate;

    /**
     * @var float
     */
    protected $securityDeposit;

    /**
     * @var float
     */
    protected $taxPercent;

    /**
     * @var int
     */
    protected $minimumDays;

    /**
     * @return int
     */
    public function getVehicleId()
    {
        return $this->vehicleId;
    }

    /**
     * @param int $vehicleId
     */
    public function setVehicleId($vehicleId)
    {
        $this->vehicleId = $vehicleId;
    }

    /**
     * @return int
     */
    public function getBookingPolicyId()
    {
        return $this->bookingPolicyId;
    }

    /**
     * @param int $bookingPolicyId
     */
    public function setBookingPolicyId($bookingPolicyId)
    {
        $this->bookingPolicyId = $bookingPolicyId;
    }

    /**
     * @return int
     */
    public function getCancellationPolicyId()
    {
        return $this->cancellationPolicyId;
    }

    /**
     * @param int $cancellationPolicyId
     */
    public function setCancellationPolicyId($cancellationPolicyId)
    {
        $this->cancellationPolicyId = $cancellationPolicyId;
    }

    /**
     * @return int|null
     */
    public function getFixedMileageFeeId()
    {
        return $this->fixedMileageFeeId;
    }

    /**
     * @param int|null $fixedMileageFeeId
     */
    public function setFixedMileageFeeId($fixedMileageFeeId)
    {
        $this->fixedMileageFeeId = $fixedMileageFeeId;
    }

    /**
     * @return int|null
     */
    public function getFuelRefillFeeId()
    {
        return $this->fuelRefillFeeId;
    }

    /**
     * @param int|null $fuelRefillFeeId
     */
    public function setFuelRefillFeeId($fuelRefillFeeId)
    {
        $this->fuelRefillFeeId = $fuelRefillFeeId;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return float|null
     */
    public function getRate()
    {
        return $this->rate;
    }

    /**
     * @param float|null $rate
     */
    public function setRate($rate)
    {
        $this->rate = $rate;
    }

    /**
     * @return float
     */
    public function getSecurityDeposit()
    {
        return $this->securityDeposit;
    }

    /**
     * @param float $securityDeposit
     */
    public function setSecurityDeposit($securityDeposit)
    {
        $this->securityDeposit = $securityDeposit;
    }

    /**
     * @return float
     */
    public function getTaxPercent()
    {
        return $this->taxPercent;
    }

    /**
     * @param float $taxPercent
     */
    public function setTaxPercent($taxPercent)
    {
        $this->taxPercent = $taxPercent;
    }

    /**
     * @return int
     */
    public function getMinimumDays()
    {
        return $this->minimumDays;
    }

    /**
     * @param int $minimumDays
     */
    public function setMinimumDays($minimumDays)
    {
        $this->minimumDays = $minimumDays;
    }
    
}