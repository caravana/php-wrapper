<?php

namespace Caravana\Core\Models\Requests\Base;


use Caravana\Core\Models\Requests\Contracts\GetLocationsRequest AS GetLocationsRequestContract;

abstract class BaseGetLocationsRequest implements GetLocationsRequestContract
{

    /**
     * @var string|null
     */
    protected $ids;

    /**
     * @var string|int|null
     */
    protected $subdivisionIds;
    
    /**
     * @var int
     */
    protected $limit;

    /**
     * @var int
     */
    protected $page;
}