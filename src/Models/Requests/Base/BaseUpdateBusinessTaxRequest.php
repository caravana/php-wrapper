<?php

namespace Caravana\Core\Models\Requests\Base;


use Caravana\Core\Models\Requests\Contracts\UpdateBusinessTaxRequest AS UpdateBusinessTaxRequestContract;

abstract class BaseUpdateBusinessTaxRequest implements UpdateBusinessTaxRequestContract
{

    /**
     * @var string
     */
    protected $businessName;

    /**
     * @var string
     */
    protected $businessTaxId;

    /**
     * @var string
     */
    protected $firstName;

    /**
     * @var string
     */
    protected $lastName;

    /**
     * @var string
     */
    protected $ssnLastFour;

    /**
     * @var string
     */
    protected $formationDate;

    /**
     * @var BaseCreateLocationRequest
     */
    protected $location;

    /**
     * timestamp
     * @var string
     */
    protected $tosAcceptanceDate;

    /**
     * @var string
     */
    protected $tosAcceptanceIp;

    /**
     * @return string
     */
    public function getBusinessName()
    {
        return $this->businessName;
    }

    /**
     * @param string $businessName
     */
    public function setBusinessName($businessName)
    {
        $this->businessName = $businessName;
    }

    /**
     * @return string
     */
    public function getBusinessTaxId()
    {
        return $this->businessTaxId;
    }

    /**
     * @param string $businessTaxId
     */
    public function setBusinessTaxId($businessTaxId)
    {
        $this->businessTaxId = $businessTaxId;
    }

    /**
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * @param string $firstName
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;
    }

    /**
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * @param string $lastName
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;
    }

    /**
     * @return string
     */
    public function getSsnLastFour()
    {
        return $this->ssnLastFour;
    }

    /**
     * @param string $ssnLastFour
     */
    public function setSsnLastFour($ssnLastFour)
    {
        $this->ssnLastFour = $ssnLastFour;
    }

    /**
     * @return string
     */
    public function getFormationDate()
    {
        return $this->formationDate;
    }

    /**
     * @param string $formationDate
     */
    public function setFormationDate($formationDate)
    {
        $this->formationDate = $formationDate;
    }

    /**
     * @return BaseCreateLocationRequest
     */
    public function getLocation()
    {
        return $this->location;
    }

    /**
     * @param BaseCreateLocationRequest $location
     */
    public function setLocation($location)
    {
        $this->location = $location;
    }

    /**
     * @return string
     */
    public function getTosAcceptanceDate()
    {
        return $this->tosAcceptanceDate;
    }

    /**
     * @param string $tosAcceptanceDate
     */
    public function setTosAcceptanceDate($tosAcceptanceDate)
    {
        $this->tosAcceptanceDate = $tosAcceptanceDate;
    }

    /**
     * @return string
     */
    public function getTosAcceptanceIp()
    {
        return $this->tosAcceptanceIp;
    }

    /**
     * @param string $tosAcceptanceIp
     */
    public function setTosAcceptanceIp($tosAcceptanceIp)
    {
        $this->tosAcceptanceIp = $tosAcceptanceIp;
    }
    
}