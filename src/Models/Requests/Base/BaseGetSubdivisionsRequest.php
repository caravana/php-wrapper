<?php

namespace Caravana\Core\Models\Requests\Base;



use Caravana\Core\Models\Requests\Contracts\GetSubdivisionsRequest AS GetSubdivisionsRequestContract;

abstract class BaseGetSubdivisionsRequest implements GetSubdivisionsRequestContract
{

    /**
     * String of comma separated integers or a single integer
     * @var string|null
     */
    protected $ids;

    /**
     * String of comma separated integers or a single integer
     * @var string|null
     */
    protected $countryIds;

    /**
     * String of comma separated integers or a single integer
     * @var string|null
     */
    protected $subdivisionTypeIds;

    /**
     * String of comma separated names
     * @var string|null
     */
    protected $names;

    /**
     * String of comma separated symbols
     * @var string
     */
    protected $symbols;

    /**
     * String of comma separated localSymbols
     * @var string
     */
    protected $localSymbols;

    /**
     * Defaults to 80
     * @var int
     */
    protected $limit;

    /**
     * Defaults to 1
     * @var int
     */
    protected $page;

    /**
     * @return null|string
     */
    public function getIds()
    {
        return $this->ids;
    }

    /**
     * @param null|string $ids
     */
    public function setIds($ids)
    {
        $this->ids = $ids;
    }

    /**
     * @return null|string
     */
    public function getCountryIds()
    {
        return $this->countryIds;
    }

    /**
     * @param null|string $countryIds
     */
    public function setCountryIds($countryIds)
    {
        $this->countryIds = $countryIds;
    }

    /**
     * @return null|string
     */
    public function getSubdivisionTypeIds()
    {
        return $this->subdivisionTypeIds;
    }

    /**
     * @param null|string $subdivisionTypeIds
     */
    public function setSubdivisionTypeIds($subdivisionTypeIds)
    {
        $this->subdivisionTypeIds = $subdivisionTypeIds;
    }

    /**
     * @return null|string
     */
    public function getNames()
    {
        return $this->names;
    }

    /**
     * @param null|string $names
     */
    public function setNames($names)
    {
        $this->names = $names;
    }

    /**
     * @return string
     */
    public function getSymbols()
    {
        return $this->symbols;
    }

    /**
     * @param string $symbols
     */
    public function setSymbols($symbols)
    {
        $this->symbols = $symbols;
    }

    /**
     * @return string
     */
    public function getLocalSymbols()
    {
        return $this->localSymbols;
    }

    /**
     * @param string $localSymbols
     */
    public function setLocalSymbols($localSymbols)
    {
        $this->localSymbols = $localSymbols;
    }

    /**
     * @return int
     */
    public function getLimit()
    {
        return $this->limit;
    }

    /**
     * @param int $limit
     */
    public function setLimit($limit)
    {
        $this->limit = $limit;
    }

    /**
     * @return int
     */
    public function getPage()
    {
        return $this->page;
    }

    /**
     * @param int $page
     */
    public function setPage($page)
    {
        $this->page = $page;
    }
    
}