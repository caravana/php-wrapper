<?php

namespace Caravana\Core\Models\Requests\Base;


use Caravana\Core\Models\Requests\Contracts\GetVendorInvitesRequest AS GetVendorInvitesRequestContract;

abstract class BaseGetVendorInvitesRequest implements GetVendorInvitesRequestContract
{

    /**
     * @var int|string|null
     */
    protected $ids;

    /**
     * @var int|string|null
     */
    protected $vendorIds;

    /**
     * @var int|string|null
     */
    protected $userIds;

    /**
     * @var int|string|null
     */
    protected $vendorInviteStatusIds;

    /**
     * @var int|string|null
     */
    protected $createdByIds;

    /**
     * @var int
     */
    protected $limit;

    /**
     * @var int
     */
    protected $page;

    /**
     * @return int|null|string
     */
    public function getIds()
    {
        return $this->ids;
    }

    /**
     * @param int|null|string $ids
     */
    public function setIds($ids)
    {
        $this->ids = $ids;
    }

    /**
     * @return int|null|string
     */
    public function getVendorIds()
    {
        return $this->vendorIds;
    }

    /**
     * @param int|null|string $vendorIds
     */
    public function setVendorIds($vendorIds)
    {
        $this->vendorIds = $vendorIds;
    }

    /**
     * @return int|null|string
     */
    public function getUserIds()
    {
        return $this->userIds;
    }

    /**
     * @param int|null|string $userIds
     */
    public function setUserIds($userIds)
    {
        $this->userIds = $userIds;
    }

    /**
     * @return int|null|string
     */
    public function getVendorInviteStatusIds()
    {
        return $this->vendorInviteStatusIds;
    }

    /**
     * @param int|null|string $vendorInviteStatusIds
     */
    public function setVendorInviteStatusIds($vendorInviteStatusIds)
    {
        $this->vendorInviteStatusIds = $vendorInviteStatusIds;
    }

    /**
     * @return int|null|string
     */
    public function getCreatedByIds()
    {
        return $this->createdByIds;
    }

    /**
     * @param int|null|string $createdByIds
     */
    public function setCreatedByIds($createdByIds)
    {
        $this->createdByIds = $createdByIds;
    }

    /**
     * @return int
     */
    public function getLimit()
    {
        return $this->limit;
    }

    /**
     * @param int $limit
     */
    public function setLimit($limit)
    {
        $this->limit = $limit;
    }

    /**
     * @return int
     */
    public function getPage()
    {
        return $this->page;
    }

    /**
     * @param int $page
     */
    public function setPage($page)
    {
        $this->page = $page;
    }
    
}