<?php

namespace Caravana\Core\Models\Requests\Base;


use Caravana\Core\Models\Requests\Contracts\CreateRegistrationRequest AS CreateRegistrationRequestContract;

abstract class BaseCreateRegistrationRequest implements CreateRegistrationRequestContract
{

    /**
     * @var string
     */
    protected $licensePlate;

    /**
     * @var string|null
     */
    protected $issuedDate;

    /**
     * @var string|null
     */
    protected $expirationDate;

    /**
     * @var int
     */
    protected $subdivisionId;

    /**
     * @return string
     */
    public function getLicensePlate()
    {
        return $this->licensePlate;
    }

    /**
     * @param string $licensePlate
     */
    public function setLicensePlate($licensePlate)
    {
        $this->licensePlate = $licensePlate;
    }

    /**
     * @return null|string
     */
    public function getIssuedDate()
    {
        return $this->issuedDate;
    }

    /**
     * @param null|string $issuedDate
     */
    public function setIssuedDate($issuedDate)
    {
        $this->issuedDate = $issuedDate;
    }

    /**
     * @return null|string
     */
    public function getExpirationDate()
    {
        return $this->expirationDate;
    }

    /**
     * @param null|string $expirationDate
     */
    public function setExpirationDate($expirationDate)
    {
        $this->expirationDate = $expirationDate;
    }

    /**
     * @return int
     */
    public function getSubdivisionId()
    {
        return $this->subdivisionId;
    }

    /**
     * @param int $subdivisionId
     */
    public function setSubdivisionId($subdivisionId)
    {
        $this->subdivisionId = $subdivisionId;
    }

}