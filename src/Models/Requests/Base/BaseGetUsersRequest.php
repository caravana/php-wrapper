<?php

namespace Caravana\Core\Models\Requests\Base;



use Caravana\Core\Models\Requests\Contracts\GetUsersRequest AS GetUsersRequestContract;

abstract class BaseGetUsersRequest implements GetUsersRequestContract
{

    /**
     * String of comma separated integers or a single integer
     * @var string|null
     */
    protected $ids;

    /**
     * String of comma separated email addresses
     * @var string|null         
     */
    protected $emails;

    /**
     * String of comma separated first names
     * @var string|null
     */
    protected $firstNames;

    /**
     * String of comma separated last names
     * @var string|null
     */
    protected $lastNames;

    /**
     * Defaults to 80
     * @var int
     */
    protected $limit;

    /**
     * Defaults to 1
     * @var int
     */
    protected $page;


    /**
     * @return int|string
     */
    public function getIds()
    {
        return $this->ids;
    }

    /**
     * @param int|string $ids
     */
    public function setIds($ids)
    {
        $this->ids = $ids;
    }

    /**
     * @return null|string
     */
    public function getEmails()
    {
        return $this->emails;
    }

    /**
     * @param null|string $emails
     */
    public function setEmails($emails)
    {
        $this->emails = $emails;
    }

    /**
     * @return null|string
     */
    public function getFirstNames()
    {
        return $this->firstNames;
    }

    /**
     * @param null|string $firstNames
     */
    public function setFirstNames($firstNames)
    {
        $this->firstNames = $firstNames;
    }

    /**
     * @return null|string
     */
    public function getLastNames()
    {
        return $this->lastNames;
    }

    /**
     * @param null|string $lastNames
     */
    public function setLastNames($lastNames)
    {
        $this->lastNames = $lastNames;
    }

    /**
     * @return int
     */
    public function getLimit()
    {
        return $this->limit;
    }

    /**
     * @param int $limit
     */
    public function setLimit($limit)
    {
        $this->limit = $limit;
    }

    /**
     * @return int
     */
    public function getPage()
    {
        return $this->page;
    }

    /**
     * @param int $page
     */
    public function setPage($page)
    {
        $this->page = $page;
    }
    
}