<?php

namespace Caravana\Core\Models\Requests\Base;


use Caravana\Core\Models\Requests\Contracts\GetUserInvitesRequest AS GetUserInvitesRequestContract;

abstract class BaseGetUserInvitesRequest implements GetUserInvitesRequestContract
{

    /**
     * String of comma separated UserInvite ids
     * @var string|null
     */
    protected $ids;

    /**
     * String of comma separated UserInviteStatusType ids
     * @var string|null
     */
    protected $userInviteStatusIds;

    /**
     * String of comma separated User ids
     * @var string|null
     */
    protected $createdByIds;

    /**
     * String of comma separated emails
     * @var string|null
     */
    protected $emails;

    /**
     * String of comma separated firstNames
     * @var string|null
     */
    protected $firstNames;

    /**
     * String of comma separated lastNames
     * @var string|null
     */
    protected $lastNames;

    /**
     * Defaults to 80
     * @var int
     */
    protected $limit;

    /**
     * Defaults to 1
     * @var int
     */
    protected $page;

    
    /**
     * @return null|string
     */
    public function getIds()
    {
        return $this->ids;
    }

    /**
     * @param null|string $ids
     */
    public function setIds($ids)
    {
        $this->ids = $ids;
    }

    /**
     * @return null|string
     */
    public function getUserInviteStatusIds()
    {
        return $this->userInviteStatusIds;
    }

    /**
     * @param null|string $userInviteStatusIds
     */
    public function setUserInviteStatusIds($userInviteStatusIds)
    {
        $this->userInviteStatusIds = $userInviteStatusIds;
    }

    /**
     * @return null|string
     */
    public function getCreatedByIds()
    {
        return $this->createdByIds;
    }

    /**
     * @param null|string $createdByIds
     */
    public function setCreatedByIds($createdByIds)
    {
        $this->createdByIds = $createdByIds;
    }

    /**
     * @return null|string
     */
    public function getEmails()
    {
        return $this->emails;
    }

    /**
     * @param null|string $emails
     */
    public function setEmails($emails)
    {
        $this->emails = $emails;
    }

    /**
     * @return null|string
     */
    public function getFirstNames()
    {
        return $this->firstNames;
    }

    /**
     * @param null|string $firstNames
     */
    public function setFirstNames($firstNames)
    {
        $this->firstNames = $firstNames;
    }

    /**
     * @return null|string
     */
    public function getLastNames()
    {
        return $this->lastNames;
    }

    /**
     * @param null|string $lastNames
     */
    public function setLastNames($lastNames)
    {
        $this->lastNames = $lastNames;
    }

    /**
     * @return int
     */
    public function getLimit()
    {
        return $this->limit;
    }

    /**
     * @param int $limit
     */
    public function setLimit($limit)
    {
        $this->limit = $limit;
    }

    /**
     * @return int
     */
    public function getPage()
    {
        return $this->page;
    }

    /**
     * @param int $page
     */
    public function setPage($page)
    {
        $this->page = $page;
    }
    
}