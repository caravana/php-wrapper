<?php

namespace Caravana\Core\Models\Requests;


use Caravana\Core\Models\Requests\Base\BaseCreateUserRequest;
use Caravana\Core\Models\Requests\Contracts\Validatable;
use jamesvweston\Utilities\ArrayUtil AS AU;

class CreateUserRequest extends BaseCreateUserRequest implements Validatable
{

    /**
     * @param   array|null $data
     */
    public function __construct($data = null)
    {
        if (is_array($data))
        {
            $this->firstName                = AU::get($data['firstName']);
            $this->lastName                 = AU::get($data['lastName']);
            $this->email                    = AU::get($data['email']);
            $this->password                 = AU::get($data['password']);
        }
    }

    /**
     * @return array
     */
    public function jsonSerialize()
    {
        $object['firstName']                = $this->firstName;
        $object['lastName']                 = $this->lastName;
        $object['email']                    = $this->email;
        $object['password']                 = $this->password;
        
        return $object;
    }
    
    
    public function validate()
    {
        // TODO: Implement validate() method.
    }

}