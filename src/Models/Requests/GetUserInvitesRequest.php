<?php

namespace Caravana\Core\Models\Requests;


use Caravana\Core\Models\Requests\Base\BaseGetUserInvitesRequest;
use Caravana\Core\Models\Requests\Contracts\Validatable;
use jamesvweston\Utilities\ArrayUtil AS AU;

class GetUserInvitesRequest extends BaseGetUserInvitesRequest implements Validatable
{

    /**
     * @param   array|null $data
     */
    public function __construct($data = null)
    {
        if (is_array($data))
        {
            $this->ids                      = AU::get($data['ids']);
            $this->userInviteStatusIds      = AU::get($data['userInviteStatusIds']);
            $this->createdByIds             = AU::get($data['createdByIds']);
            $this->emails                   = AU::get($data['emails']);
            $this->firstNames               = AU::get($data['firstNames']);
            $this->lastNames                = AU::get($data['lastNames']);
            $this->limit                    = AU::get($data['limit'], 80);
            $this->page                     = AU::get($data['page'], 1);
        }
    }

    /**
     * @return array
     */
    public function jsonSerialize()
    {
        $object['ids']                      = $this->ids;
        $object['userInviteStatusIds']      = $this->userInviteStatusIds;
        $object['createdByIds']             = $this->createdByIds;
        $object['emails']                   = $this->emails;
        $object['firstNames']               = $this->firstNames;
        $object['lastNames']                = $this->lastNames;
        $object['limit']                    = $this->limit;
        $object['page']                     = $this->page;

        return $object;
    }


    public function validate()
    {
        // TODO: Implement validate() method.
    }
    
}