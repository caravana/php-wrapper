<?php

namespace Caravana\Core\Models\Requests;



use Caravana\Core\Models\Requests\Base\BaseCreateVendorLocationRequest;
use Caravana\Core\Models\Requests\Contracts\Validatable;
use jamesvweston\Utilities\ArrayUtil AS AU;

class CreateVendorLocationRequest extends BaseCreateVendorLocationRequest implements Validatable
{

    /**
     * @param   array|null $data
     */
    public function __construct($data = null)
    {
        if (is_array($data))
        {
            $this->reference1               = AU::get($data['reference1']);
            $this->street1                  = AU::get($data['street1']);
            $this->street2                  = AU::get($data['street2']);
            $this->city                     = AU::get($data['city']);
            $this->postalCode               = AU::get($data['postalCode']);
            $this->subdivisionId            = AU::get($data['subdivisionId']);
        }
    }

    /**
     * @return array
     */
    public function jsonSerialize()
    {
        $object['reference1']               = $this->reference1;
        $object['street1']                  = $this->street1;
        $object['street2']                  = $this->street2;
        $object['city']                     = $this->city;
        $object['postalCode']               = $this->postalCode;
        $object['subdivisionId']            = $this->subdivisionId;

        return $object;
    }


    public function validate()
    {
        // TODO: Implement validate() method.
    }
    
}