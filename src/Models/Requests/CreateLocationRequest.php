<?php

namespace Caravana\Core\Models\Requests;


use Caravana\API\Exceptions\EntityValidationException;
use Caravana\API\Exceptions\RequiredFieldMissingException;
use Caravana\Core\Models\Requests\Base\BaseCreateLocationRequest;
use Caravana\Core\Models\Requests\Contracts\Validatable;
use jamesvweston\Utilities\ArrayUtil AS AU;
use jamesvweston\Utilities\InputUtil;

class CreateLocationRequest extends BaseCreateLocationRequest implements Validatable
{

    /**
     * @param   array|null $data
     */
    public function __construct($data = null)
    {
        if (is_array($data))
        {
            $this->street1                  = AU::get($data['street1']);
            $this->street2                  = AU::get($data['street2']);
            $this->city                     = AU::get($data['city']);
            $this->postalCode               = AU::get($data['postalCode']);
            $this->subdivisionId            = AU::get($data['subdivisionId']);
        }
    }

    /**
     * @return array
     */
    public function jsonSerialize()
    {
        $object['street1']                  = $this->street1;
        $object['street2']                  = $this->street2;
        $object['city']                     = $this->city;
        $object['postalCode']               = $this->postalCode;
        $object['subdivisionId']            = $this->subdivisionId;

        return $object;
    }


    public function validate()
    {
        /**
         * Validate required fields
         */
        if (is_null($this->street1) || empty(trim($this->street1)))
            throw new RequiredFieldMissingException('CreateLocationRequest', 'street1');

        if (is_null($this->city) || empty(trim($this->city)))
            throw new RequiredFieldMissingException('CreateLocationRequest', 'city');

        if (is_null($this->postalCode) || empty(trim($this->postalCode)))
            throw new RequiredFieldMissingException('CreateLocationRequest', 'postalCode');

        if (is_null($this->subdivisionId) || empty(trim($this->subdivisionId)))
            throw new RequiredFieldMissingException('CreateLocationRequest', 'subdivisionId');

        /**
         * Validate expected data types
         */
        if (is_null(InputUtil::getInt($this->subdivisionId)))
            throw new EntityValidationException('CreateLocationRequest', 'subdivisionId', 'Expected integer value', $this->subdivisionId);
    }

    public function clean ()
    {
        $this->subdivisionId                = InputUtil::getInt($this->subdivisionId);
    }
    
}