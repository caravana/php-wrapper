<?php

namespace Caravana\Core\Models\Requests;


use Caravana\API\Exceptions\EntityValidationException;
use Caravana\Core\Models\Requests\Base\BaseGetRentalsRequest;
use Caravana\Core\Models\Requests\Contracts\Validatable;
use jamesvweston\Utilities\ArrayUtil AS AU;
use jamesvweston\Utilities\BooleanUtil AS BU;
use jamesvweston\Utilities\DateUtil AS DU;
use jamesvweston\Utilities\InputUtil AS IU;

class GetRentalsRequest extends BaseGetRentalsRequest implements Validatable
{

    /**
     * @param   array|null $data
     */
    public function __construct($data = null)
    {
        if (is_array($data))
        {
            $this->ids                      = AU::get($data['ids']);
            $this->published                = AU::get($data['published']);
            $this->vendorIds                = AU::get($data['vendorIds']);
            $this->fromDate                 = AU::get($data['fromDate']);
            $this->toDate                   = AU::get($data['toDate']);
            $this->vehicleIds               = AU::get($data['vehicleIds']);
            $this->vehicleTypeIds           = AU::get($data['vehicleTypeIds']);
            $this->vehicleClassIds          = AU::get($data['vehicleClassIds']);
            $this->vehicleMakeIds           = AU::get($data['vehicleMakeIds']);
            $this->vehicleModelIds          = AU::get($data['vehicleModelIds']);
            $this->fuelTypeIds              = AU::get($data['fuelTypeIds']);
            $this->transmissionIds          = AU::get($data['transmissionIds']);
            $this->limit                    = AU::get($data['limit'], 80);
            $this->page                     = AU::get($data['page'], 1);
        }
    }

    /**
     * @return array
     */
    public function jsonSerialize()
    {
        $object['ids']                      = $this->ids;
        $object['published']                = $this->published;
        $object['vendorIds']                = $this->vendorIds;
        $object['fromDate']                 = $this->fromDate;
        $object['toDate']                   = $this->toDate;
        $object['vehicleIds']               = $this->vehicleIds;
        $object['vehicleTypeIds']           = $this->vehicleTypeIds;
        $object['vehicleClassIds']          = $this->vehicleClassIds;
        $object['vehicleMakeIds']           = $this->vehicleMakeIds;
        $object['vehicleModelIds']          = $this->vehicleModelIds;
        $object['fuelTypeIds']              = $this->fuelTypeIds;
        $object['transmissionIds']          = $this->transmissionIds;
        $object['limit']                    = $this->limit;
        $object['page']                     = $this->page;

        return $object;
    }

    public function validate()
    {
        if (!is_null($this->fromDate))
        {
            if (!DU::validate($this->fromDate, 'YYYY-MM-DD'))
                throw new EntityValidationException('GetRentalsRequest', 'fromDate', 'YYYY-MM-DD required', $this->fromDate);
        }

        if (!is_null($this->toDate))
        {
            if (!DU::validate($this->toDate, 'YYYY-MM-DD'))
                throw new EntityValidationException('GetRentalsRequest', 'toDate', 'YYYY-MM-DD required', $this->toDate);
        }

        if (!is_null($this->published))
        {
            if (is_null(BU::getBooleanValue($this->published)))
                throw new EntityValidationException('GetRentalsRequest', 'published', 'boolean value expected', $this->published);
        }
    }

    public function clean()
    {
        $this->ids                          = IU::getIdsString($this->ids);
        $this->published                    = BU::getBooleanValue($this->published);
        $this->vendorIds                    = IU::getIdsString($this->vendorIds);
        $this->fromDate                     = DU::validate($this->fromDate, 'YYYY-MM-DD') ? $this->fromDate : null;
        $this->toDate                       = DU::validate($this->toDate, 'YYYY-MM-DD') ? $this->toDate : null;
        $this->vehicleIds                   = IU::getIdsString($this->vehicleIds);
        $this->vehicleTypeIds               = IU::getIdsString($this->vehicleTypeIds);
        $this->vehicleClassIds              = IU::getIdsString($this->vehicleClassIds);
        $this->vehicleMakeIds               = IU::getIdsString($this->vehicleMakeIds);
        $this->vehicleModelIds              = IU::getIdsString($this->vehicleModelIds);
        $this->fuelTypeIds                  = IU::getIdsString($this->fuelTypeIds);
        $this->transmissionIds              = IU::getIdsString($this->transmissionIds);
        $this->limit                        = IU::getInt($this->limit);
        $this->page                         = IU::getInt($this->page);
    }
    
}