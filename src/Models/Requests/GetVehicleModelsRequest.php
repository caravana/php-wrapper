<?php

namespace Caravana\Core\Models\Requests;


use Caravana\Core\Models\Requests\Base\BaseGetVehicleModelsRequest;
use Caravana\Core\Models\Requests\Contracts\Validatable;
use jamesvweston\Utilities\ArrayUtil AS AU;

class GetVehicleModelsRequest extends BaseGetVehicleModelsRequest implements Validatable
{

    /**
     * @param   array|null $data
     */
    public function __construct($data = null)
    {
        if (is_array($data))
        {
            $this->ids                      = AU::get($data['ids']);
            $this->names                    = AU::get($data['names']);
            $this->vehicleMakeIds           = AU::get($data['vehicleMakeIds']);
            $this->limit                    = AU::get($data['limit'], 80);
            $this->page                     = AU::get($data['page'], 1);
        }
    }

    /**
     * @return array
     */
    public function jsonSerialize()
    {
        $object['ids']                      = $this->ids;
        $object['names']                    = $this->names;
        $object['vehicleMakeIds']           = $this->vehicleMakeIds;
        $object['limit']                    = $this->limit;
        $object['page']                     = $this->page;

        return $object;
    }


    public function validate()
    {
        // TODO: Implement validate() method.
    }
    
}