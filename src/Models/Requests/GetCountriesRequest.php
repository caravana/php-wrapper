<?php

namespace Caravana\Core\Models\Requests;


use Caravana\Core\Models\Requests\Base\BaseGetCountriesRequest;
use Caravana\Core\Models\Requests\Contracts\Validatable;
use jamesvweston\Utilities\ArrayUtil AS AU;

class GetCountriesRequest extends BaseGetCountriesRequest implements Validatable
{
    
    /**
     * @param   array|null $data
     */
    public function __construct($data = null)
    {
        if (is_array($data))
        {
            $this->ids                      = AU::get($data['ids']);
            $this->names                    = AU::get($data['names']);
            $this->iso2s                    = AU::get($data['iso2s']);
            $this->iso3s                    = AU::get($data['iso3s']);
            $this->limit                    = AU::get($data['limit'], 80);
            $this->page                     = AU::get($data['page'], 1);
        }
    }

    /**
     * @return array
     */
    public function jsonSerialize()
    {
        $object['ids']                      = $this->ids;
        $object['names']                    = $this->names;
        $object['iso2s']                    = $this->iso2s;
        $object['iso3s']                    = $this->iso3s;
        $object['limit']                    = $this->limit;
        $object['page']                     = $this->page;

        return $object;
    }
    
    public function validate()
    {
        // TODO: Implement validate() method.
    }
    
}