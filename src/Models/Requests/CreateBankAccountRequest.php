<?php

namespace Caravana\Core\Models\Requests;


use Caravana\API\Exceptions\EntityValidationException;
use Caravana\API\Exceptions\RequiredFieldMissingException;
use Caravana\Core\Models\Requests\Base\BaseCreateBankAccountRequest;
use Caravana\Core\Models\Requests\Contracts\Validatable;
use jamesvweston\Utilities\ArrayUtil AS AU;
use jamesvweston\Utilities\InputUtil AS IU;
use jamesvweston\Utilities\InputUtil;

class CreateBankAccountRequest extends BaseCreateBankAccountRequest implements Validatable
{

    /**
     * @param   array|null $data
     */
    public function __construct($data = null)
    {
        if (is_array($data))
        {
            $this->accountNumber            = AU::get($data['accountNumber']);
            $this->accountHolderName        = AU::get($data['accountHolderName']);
            $this->accountHolderType        = AU::get($data['accountHolderType']);
            $this->routingNumber            = AU::get($data['routingNumber']);
            $this->countryId                = AU::get($data['countryId']);
        }
    }

    public function validate()
    {
        /**
         * Validate required fields
         */
        if (is_null($this->accountNumber))
            throw new RequiredFieldMissingException('CreateBankAccountRequest', 'accountNumber');

        if (is_null($this->accountHolderName))
            throw new RequiredFieldMissingException('CreateBankAccountRequest', 'accountHolderName');

        if (is_null($this->accountHolderType))
            throw new RequiredFieldMissingException('CreateBankAccountRequest', 'accountHolderType');

        if (is_null($this->routingNumber))
            throw new RequiredFieldMissingException('CreateBankAccountRequest', 'routingNumber');

        if (is_null($this->countryId))
            throw new RequiredFieldMissingException('CreateBankAccountRequest', 'countryId');

        /**
         * Validate field settings
         */
        if (!in_array($this->accountHolderType, ['individual', 'company']))
            throw new EntityValidationException('CreateBankAccountRequest', 'accountHolderType', 'Must be either individual or company', $this->accountHolderType);

        /**
         * Validate expected data types
         */
        if (is_null(InputUtil::getInt($this->countryId)))
            throw new EntityValidationException('CreateDurationDiscountRequest', 'countryId', 'Expected integer value', $this->countryId);
    }

    /**
     * @return array
     */
    public function jsonSerialize()
    {
        $object['accountNumber']            = $this->accountNumber;
        $object['accountHolderName']        = $this->accountHolderName;
        $object['accountHolderType']        = $this->accountHolderType;
        $object['routingNumber']            = $this->routingNumber;
        $object['countryId']                = $this->countryId;

        return $object;
    }

    public function clean ()
    {
        $this->countryId                    = IU::getInt($this->countryId);
    }
    
}