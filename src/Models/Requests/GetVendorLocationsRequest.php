<?php

namespace Caravana\Core\Models\Requests;


use Caravana\Core\Models\Requests\Base\BaseGetVendorLocationsRequest;
use Caravana\Core\Models\Requests\Contracts\Validatable;
use jamesvweston\Utilities\ArrayUtil AS AU;

class GetVendorLocationsRequest extends BaseGetVendorLocationsRequest implements Validatable
{

    /**
     * @param   array|null $data
     */
    public function __construct($data = null)
    {
        if (is_array($data))
        {
            $this->ids                      = AU::get($data['ids']);
            $this->labels                   = AU::get($data['labels']);
            $this->vendorIds                = AU::get($data['vendorIds']);
            $this->limit                    = AU::get($data['limit']);
            $this->page                     = AU::get($data['page']);
        }
    }

    /**
     * @return array
     */
    public function jsonSerialize()
    {
        $object['ids']                      = $this->ids;
        $object['labels']                   = $this->labels;
        $object['vendorIds']                = $this->vendorIds;
        $object['limit']                    = $this->limit;
        $object['page']                     = $this->page;

        return $object;
    }

    public function validate()
    {
        // TODO: Implement validate() method.
    }
    
}