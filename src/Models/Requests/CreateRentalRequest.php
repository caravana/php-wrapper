<?php

namespace Caravana\Core\Models\Requests;


use Caravana\Core\Models\Requests\Base\BaseCreateRentalRequest;
use Caravana\Core\Models\Requests\Contracts\Validatable;
use jamesvweston\Utilities\ArrayUtil AS AU;

class CreateRentalRequest extends BaseCreateRentalRequest implements Validatable
{

    /**
     * @param   array|null $data
     */
    public function __construct($data = null)
    {
        if (is_array($data))
        {
            $this->bookingPolicyId          = AU::get($data['bookingPolicyId']);
            $this->cancellationPolicyId     = AU::get($data['cancellationPolicyId']);
            $this->fixedMileageFeeId        = AU::get($data['fixedMileageFeeId']);
            $this->fuelRefillFeeId          = AU::get($data['fuelRefillFeeId']);
            $this->vehicleId                = AU::get($data['vehicleId']);
            $this->name                     = AU::get($data['name']);
            $this->description              = AU::get($data['description']);
            $this->rate                     = AU::get($data['rate']);
            $this->securityDeposit          = AU::get($data['securityDeposit']);
            $this->taxPercent               = AU::get($data['taxPercent']);
            $this->minimumDays              = AU::get($data['minimumDays']);
        }
    }

    /**
     * @return array
     */
    public function jsonSerialize()
    {
        $object['bookingPolicyId']          = $this->bookingPolicyId;
        $object['cancellationPolicyId']     = $this->cancellationPolicyId;
        $object['fixedMileageFeeId']        = $this->fixedMileageFeeId;
        $object['fuelRefillFeeId']          = $this->fuelRefillFeeId;
        $object['vehicleId']                = $this->vehicleId;
        $object['name']                     = $this->name;
        $object['description']              = $this->description;
        $object['rate']                     = $this->rate;
        $object['securityDeposit']          = $this->securityDeposit;
        $object['taxPercent']               = $this->taxPercent;
        $object['minimumDays']              = $this->minimumDays;

        return $object;
    }


    public function validate()
    {
        // TODO: Implement validate() method.
    }

}