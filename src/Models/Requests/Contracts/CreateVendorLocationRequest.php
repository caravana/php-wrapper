<?php

namespace Caravana\Core\Models\Requests\Contracts;


interface CreateVendorLocationRequest extends CreateLocationRequest
{
    function getReference1();
    function setReference1($reference1);
}