<?php

namespace Caravana\Core\Models\Requests\Contracts;


interface GetUsersRequest extends \JsonSerializable
{
    function getIds();
    function setIds($ids);
    function getEmails();
    function setEmails($emails);
    function getFirstNames();
    function setFirstNames($firstNames);
    function getLastNames();
    function setLastNames($lastNames);
    function getLimit();
    function setLimit($limit);
    function getPage();
    function setPage($page);
}