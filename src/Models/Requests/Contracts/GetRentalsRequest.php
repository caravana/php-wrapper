<?php

namespace Caravana\Core\Models\Requests\Contracts;


interface GetRentalsRequest extends \JsonSerializable
{
    function getIds();
    function setIds($ids);
    function getPublished();
    function setPublished($published);
    function getVendorIds();
    function setVendorIds($vendorIds);
    function getFromDate();
    function setFromDate($fromDate);
    function getToDate();
    function setToDate($toDate);
    function getVehicleIds();
    function setVehicleIds($vehicleIds);
    function getVehicleTypeIds();
    function setVehicleTypeIds($vehicleTypeIds);
    function getVehicleClassIds();
    function setVehicleClassIds($vehicleClassIds);
    function getVehicleMakeIds();
    function setVehicleMakeIds($vehicleMakeIds);
    function getVehicleModelIds();
    function setVehicleModelIds($vehicleModelIds);
    function getFuelTypeIds();
    function setFuelTypeIds($fuelTypeIds);
    function getTransmissionIds();
    function setTransmissionIds($transmissionIds);
    function getLimit();
    function setLimit($limit);
    function getPage();
    function setPage($page);
}