<?php

namespace Caravana\Core\Models\Requests\Contracts;


interface CreateUserInviteRequest extends \JsonSerializable
{
    function getFirstName();
    function setFirstName($firstName);
    function getLastName();
    function setLastName($lastName);
    function getEmail();
    function setEmail($email);
}