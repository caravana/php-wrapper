<?php

namespace Caravana\Core\Models\Requests\Contracts;


interface CreateVendorRequest extends \JsonSerializable
{
    function getName();
    function setName($name);
    function getOwnerId();
    function setOwnerId($ownerId);
    function getBookingPolicyId();
    function setBookingPolicyId($bookingPolicyId);
    function getCancellationPolicyId();
    function setCancellationPolicyId($cancellationPolicyId);
}