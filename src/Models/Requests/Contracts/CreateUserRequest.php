<?php

namespace Caravana\Core\Models\Requests\Contracts;


interface CreateUserRequest extends \JsonSerializable
{
    function getFirstName();
    function setFirstName($firstName);
    function getLastName();
    function setLastName($lastName);
    function getEmail();
    function setEmail($email);
    function getPassword();
    function setPassword($password);
}