<?php

namespace Caravana\Core\Models\Requests\Contracts;

interface CreateAddOnRequest extends \JsonSerializable
{
    public function getName();

    public function setName($name);

    public function getDescription();

    public function setDescription($description);

    public function getVendorId();

    public function setVendorId($vendorId);

    public function getRate();

    public function setRate($rate);

    public function getRateTypeId();

    public function setRateTypeId($rateTypeId);

}