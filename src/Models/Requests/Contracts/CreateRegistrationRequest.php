<?php

namespace Caravana\Core\Models\Requests\Contracts;

interface CreateRegistrationRequest extends \JsonSerializable
{
    public function getLicensePlate();

    public function setLicensePlate($licensePlate);

    public function getIssuedDate();

    public function setIssuedDate($issuedDate);

    public function getExpirationDate();

    public function setExpirationDate($expirationDate);

    public function getSubdivisionId();

    public function setSubdivisionId($subdivisionId);
}