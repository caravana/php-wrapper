<?php

namespace Caravana\Core\Models\Requests\Contracts;


interface CreateBankAccountRequest extends \JsonSerializable
{
    function getAccountNumber();
    function setAccountNumber($accountNumber);
    function getAccountHolderName();
    function setAccountHolderName($accountHolderName);
    function getAccountHolderType();
    function setAccountHolderType($accountHolderType);
    function getRoutingNumber();
    function setRoutingNumber($routingNumber);
    function getCountryId();
    function setCountryId($countryId);
}