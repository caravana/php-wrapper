<?php

namespace Caravana\Core\Models\Requests\Contracts;


interface CreateFuelRefillFeeRequest extends \JsonSerializable
{

    function getPerGallonFee();

    function setPerGallonFee($perGallonFee);

}