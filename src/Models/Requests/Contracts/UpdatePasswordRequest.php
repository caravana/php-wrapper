<?php

namespace Caravana\Core\Models\Requests\Contracts;


interface UpdatePasswordRequest extends \JsonSerializable
{
    function getCurrentPassword();
    function setCurrentPassword($currentPassword);
    function getNewPassword();
    function setNewPassword($newPassword);
}