<?php

namespace Caravana\Core\Models\Requests\Contracts;

interface CreateSeasonRequest extends \JsonSerializable
{
    public function getVendorId();

    public function setVendorId($vendorId);

    public function getStartDate();

    public function setStartDate($startDate);

    public function getEndDate();

    public function setEndDate($endDate);

    public function getName();

    public function setName($name);
}