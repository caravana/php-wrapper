<?php

namespace Caravana\Core\Models\Requests\Contracts;


interface GetVehicleModelsRequest extends \JsonSerializable
{
    function getIds();
    function setIds($ids);
    function getNames();
    function setNames($names);
    function getVehicleMakeIds();
    function setVehicleMakeIds($vehicleMakeIds);
    function getLimit();
    function setLimit($limit);
    function getPage();
    function setPage($page);
}