<?php

namespace Caravana\Core\Models\Requests\Contracts;

interface CreateSeasonalRateRequest extends \JsonSerializable
{
    public function getRentalId();

    public function setRentalId($rentalId);

    public function getSeasonId();

    public function setSeasonId($seasonId);

    public function getDailyRate();

    public function setDailyRate($dailyRate);

    public function getApplyDurationDiscount();

    function setApplyDurationDiscount($applyDurationDiscount);
}