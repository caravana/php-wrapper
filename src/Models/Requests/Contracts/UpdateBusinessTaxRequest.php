<?php

namespace Caravana\Core\Models\Requests\Contracts;

interface UpdateBusinessTaxRequest extends \JsonSerializable
{
    public function getBusinessName();

    public function setBusinessName($businessName);

    public function getBusinessTaxId();

    public function setBusinessTaxId($businessTaxId);

    public function getFirstName();

    public function setFirstName($firstName);

    public function getLastName();

    public function setLastName($lastName);

    public function getSsnLastFour();

    public function setSsnLastFour($ssnLastFour);

    public function getFormationDate();

    public function setFormationDate($formationDate);

    public function getLocation();

    public function setLocation($location);

    public function getTosAcceptanceDate();

    public function setTosAcceptanceDate($tosAcceptanceDate);

    public function getTosAcceptanceIp();

    public function setTosAcceptanceIp($tosAcceptanceIp);
}