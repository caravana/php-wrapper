<?php

namespace Caravana\Core\Models\Requests\Contracts;


interface CreateVehicleRequest extends \JsonSerializable
{
    function getVendorId();
    function setVendorId($vendorId);
    function getVehicleTypeId();
    function setVehicleTypeId($vehicleTypeId);
    function getVendorLocationId();
    function setVendorLocationId($vendorLocationId);
    function getVehicleModelId();
    function setVehicleModelId($vehicleModelId);
    function getVehicleClassId();
    function setVehicleClassId($vehicleClassId);
    function getTransmissionId();
    function setTransmissionId($transmissionId);
    function getFuelTypeId();
    function setFuelTypeId($fuelTypeId);
    function getAccommodates();
    function setAccommodates($accommodates);
    function getYearCreated();
    function setYearCreated($yearCreated);
    function getFeetLong();
    function setFeetLong($feetLong);
    function getVin();
    function setVin($vin);
    function getUniqueReference();
    function setUniqueReference($uniqueReference);
    function getReference1();
    function setReference1($reference1);
    function getReference2();
    function setReference2($reference2);
    function getReference3();
    function setReference3($reference3);
    function getReference4();
    function setReference4($reference4);
    function getReference5();
    function setReference5($reference5);
    
}