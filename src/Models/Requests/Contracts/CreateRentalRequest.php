<?php

namespace Caravana\Core\Models\Requests\Contracts;


interface CreateRentalRequest extends \JsonSerializable
{
    function getVehicleId();
    function setVehicleId($vehicleId);
    function getBookingPolicyId();
    function setBookingPolicyId($bookingPolicyId);
    function getCancellationPolicyId();
    function setCancellationPolicyId($cancellationPolicyId);
    function getFixedMileageFeeId();
    function setFixedMileageFeeId($fixedMileageFeeId);
    function getFuelRefillFeeId();
    function setFuelRefillFeeId($fuelRefillFeeId);
    function getName();
    function setName($name);
    function getDescription();
    function setDescription($description);
    function getRate();
    function setRate($rate);
    function getSecurityDeposit();
    function setSecurityDeposit($securityDeposit);
    function getTaxPercent();
    function setTaxPercent($taxPercent);
    function getMinimumDays();
    function setMinimumDays($minimumDays);
}