<?php

namespace Caravana\Core\Models\Requests\Contracts;


interface GetVehiclesRequest extends \JsonSerializable
{
    function getIds();
    function setIds($ids);
    function getVehicleTypeIds();
    function setVehicleTypeIds($vehicleTypeIds);
    function getVehicleClassIds();
    function setVehicleClassIds($vehicleClassIds);
    function getVehicleModelIds();
    function setVehicleModelIds($vehicleModelIds);
    function getVehicleMakeIds();
    function setVehicleMakeIds($vehicleMakeIds);
    function getVendorIds();
    function setVendorIds($vendorIds);
    function getInRental();
    function setInRental($inRental);
    function getUniqueReferences();
    function setUniqueReferences($uniqueReferences);
    function getLimit();
    function setLimit($limit);
    function getPage();
    function setPage($page);
}