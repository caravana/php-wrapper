<?php

namespace Caravana\Core\Models\Requests\Contracts;


interface Validatable
{
    function validate();
}