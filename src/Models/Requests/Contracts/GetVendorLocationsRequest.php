<?php

namespace Caravana\Core\Models\Requests\Contracts;


interface GetVendorLocationsRequest extends \JsonSerializable
{
    function getIds();
    function setIds($ids);
    function getLabels();
    function setLabels($labels);
    function getVendorIds();
    function setVendorIds($vendorIds);
    function getLimit();
    function setLimit($limit);
    function getPage();
    function setPage($page);
}