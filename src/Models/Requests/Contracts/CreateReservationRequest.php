<?php

namespace Caravana\Core\Models\Requests\Contracts;

interface CreateReservationRequest
{
    public function getUserId();

    public function setUserId($userId);

    public function getVendorId();

    public function setVendorId($vendorId);

    public function getServiceIds();

    public function setServiceIds($serviceIds);

    public function getStartDate();

    public function setStartDate($startDate);

    public function getEndDate();

    public function setEndDate($endDate);
}