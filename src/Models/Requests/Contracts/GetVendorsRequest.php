<?php

namespace Caravana\Core\Models\Requests\Contracts;


interface GetVendorsRequest extends \JsonSerializable
{
    function getIds();
    function setIds($ids);
    function getNames();
    function setNames($names);
    function getOwnerIds();
    function setOwnerIds($ownerIds);
    function getLimit();
    function setLimit($limit);
    function getPage();
    function setPage($page);
}