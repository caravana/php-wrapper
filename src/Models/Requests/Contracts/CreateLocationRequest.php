<?php

namespace Caravana\Core\Models\Requests\Contracts;


interface CreateLocationRequest extends \JsonSerializable
{
    function getStreet1();
    function setStreet1($street1);
    function getStreet2();
    function setStreet2($street2);
    function getCity();
    function setCity($city);
    function getPostalCode();
    function setPostalCode($postalCode);
    function getSubdivisionId();
    function setSubdivisionId($subdivisionId);
}