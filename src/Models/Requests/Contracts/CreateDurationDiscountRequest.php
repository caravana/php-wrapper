<?php

namespace Caravana\Core\Models\Requests\Contracts;

interface CreateDurationDiscountRequest extends \JsonSerializable
{
    public function getMinimumDays();

    public function setMinimumDays($minimumDays);

    public function getPercentDiscount();

    public function setPercentDiscount($percentDiscount);

    public function getRentalId();

    public function setRentalId($rentalId);
}