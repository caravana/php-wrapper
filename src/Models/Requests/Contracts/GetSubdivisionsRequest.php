<?php

namespace Caravana\Core\Models\Requests\Contracts;


interface GetSubdivisionsRequest extends \JsonSerializable
{
    function getIds();
    function setIds($ids);
    function getNames();
    function setNames($names);
    function getSymbols();
    function setSymbols($symbols);
    function getLocalSymbols();
    function setLocalSymbols($localSymbols);
    function getCountryIds();
    function setCountryIds($countryIds);
    function getSubdivisionTypeIds();
    function setSubdivisionTypeIds($subdivisionTypeIds);
    function getLimit();
    function setLimit($limit);
    function getPage();
    function setPage($page);
}