<?php

namespace Caravana\Core\Models\Requests\Contracts;

interface CreateConsignedVehicleRequest extends \JsonSerializable
{
    public function getVendorId();

    public function setVendorId($vendorId);

    public function getBookingPolicyId();

    public function setBookingPolicyId($bookingPolicyId);

    public function getCancellationPolicyId();

    public function setCancellationPolicyId($cancellationPolicyId);

    public function getFixedMileageFeeId();

    public function setFixedMileageFeeId($fixedMileageFeeId);

    public function getVehicleTypeId();

    public function setVehicleTypeId($vehicleTypeId);

    public function getVehicleClassId();

    public function setVehicleClassId($vehicleClassId);

    public function getRate();

    public function setRate($rate);

    public function getName();

    public function setName($name);

    public function getDescription();

    public function setDescription($description);
}