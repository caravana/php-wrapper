<?php

namespace Caravana\Core\Models\Requests\Contracts;


interface CreateVehicleFeatureRequest extends \JsonSerializable
{
    function getFeatureId();
    function setFeatureId($featureId);
    function getValue();
    function setValue($value);
}