<?php

namespace Caravana\Core\Models\Requests\Contracts;


interface CreateVendorInviteRequest extends \JsonSerializable
{
    function getVendorId();
    function setVendorId($vendorId);
    function getUserId();
    function setUserId($userId);
}