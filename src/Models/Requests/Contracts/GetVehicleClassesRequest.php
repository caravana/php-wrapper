<?php

namespace Caravana\Core\Models\Requests\Contracts;


interface GetVehicleClassesRequest extends \JsonSerializable
{
    function getIds();
    function setIds($ids);
    function getNames();
    function setNames($names);
    function getLimit();
    function setLimit($limit);
    function getPage();
    function setPage($page);
}