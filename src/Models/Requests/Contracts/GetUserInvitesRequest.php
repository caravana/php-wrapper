<?php

namespace Caravana\Core\Models\Requests\Contracts;


interface GetUserInvitesRequest extends \JsonSerializable
{
    function getIds();
    function setIds($ids);
    function getUserInviteStatusIds();
    function setUserInviteStatusIds($userInviteStatusIds);
    function getCreatedByIds();
    function setCreatedByIds($createdByIds);
    function getEmails();
    function setEmails($emails);
    function getFirstNames();
    function setFirstNames($firstNames);
    function getLastNames();
    function setLastNames($lastNames);
    function getLimit();
    function setLimit($limit);
    function getPage();
    function setPage($page);
}