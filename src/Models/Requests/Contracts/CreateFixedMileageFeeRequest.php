<?php

namespace Caravana\Core\Models\Requests\Contracts;

interface CreateFixedMileageFeeRequest extends \JsonSerializable
{
    public function getFreeMiles();

    public function setFreeMiles($freeMiles);

    public function getMileageFee();

    public function setMileageFee($mileageFee);
}