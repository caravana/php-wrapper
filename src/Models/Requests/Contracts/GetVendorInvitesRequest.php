<?php

namespace Caravana\Core\Models\Requests\Contracts;


interface GetVendorInvitesRequest extends \JsonSerializable
{
    function getIds();
    function setIds($ids);
    function getVendorIds();
    function setVendorIds($vendorIds);
    function getUserIds();
    function setUserIds($userIds);
    function getVendorInviteStatusIds();
    function setVendorInviteStatusIds($vendorInviteStatusIds);
    function getCreatedByIds();
    function setCreatedByIds($createdByIds);
    function getLimit();
    function setLimit($limit);
    function getPage();
    function setPage($page);
}