<?php

namespace Caravana\Core\Models\Requests\Contracts;


interface GetCountriesRequest extends \JsonSerializable
{
    function getIds();
    function setIds($ids);
    function getNames();
    function setNames($names);
    function getIso2s();
    function setIso2s($iso2s);
    function getIso3s();
    function setIso3s($iso3s);
    function getLimit();
    function setLimit($limit);
    function getPage();
    function setPage($page);
}