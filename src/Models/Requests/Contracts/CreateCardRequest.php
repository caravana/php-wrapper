<?php

namespace Caravana\Core\Models\Requests\Contracts;


interface CreateCardRequest extends \JsonSerializable
{
    function getName();
    function setName($name);
    function getNumber();
    function setNumber($number);
    function getMonth();
    function setMonth($month);
    function getYear();
    function setYear($year);
    function getCvc();
    function setCvc($cvc);
    function getPostalCode();
    function setPostalCode($postalCode);
}