<?php

namespace Caravana\Core\Models\Requests;


use Caravana\Core\Models\Requests\Base\BaseCreateVendorInviteRequest;
use Caravana\Core\Models\Requests\Contracts\Validatable;
use jamesvweston\Utilities\ArrayUtil AS AU;

class CreateVendorInviteRequest extends BaseCreateVendorInviteRequest implements Validatable
{

    /**
     * @param   array|null $data
     */
    public function __construct($data = null)
    {
        if (is_array($data))
        {
            $this->vendorId                 = AU::get($data['vendorId']);
            $this->userId                   = AU::get($data['userId']);
        }
    }

    /**
     * @return array
     */
    public function jsonSerialize()
    {
        $object['vendorId']                 = $this->vendorId;
        $object['userId']                   = $this->userId;

        return $object;
    }


    public function validate()
    {
        // TODO: Implement validate() method.
    }

}