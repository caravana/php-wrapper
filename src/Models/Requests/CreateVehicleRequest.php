<?php

namespace Caravana\Core\Models\Requests;


use Caravana\Core\Models\Requests\Base\BaseCreateVehicleRequest;
use Caravana\Core\Models\Requests\Contracts\Validatable;
use jamesvweston\Utilities\ArrayUtil AS AU;
use jamesvweston\Utilities\InputUtil AS IU;

class CreateVehicleRequest extends BaseCreateVehicleRequest implements Validatable
{

    /**
     * @param   array|null $data
     */
    public function __construct($data = null)
    {
        if (is_array($data))
        {
            $this->vendorId                 = AU::get($data['vendorId']);
            $this->vehicleTypeId            = AU::get($data['vehicleTypeId']);
            $this->vendorLocationId         = AU::get($data['vendorLocationId']);
            $this->vehicleModelId           = AU::get($data['vehicleModelId']);
            $this->vehicleClassId           = AU::get($data['vehicleClassId']);
            $this->transmissionId           = AU::get($data['transmissionId']);
            $this->fuelTypeId               = AU::get($data['fuelTypeId']);
            $this->accommodates             = AU::get($data['accommodates']);
            $this->yearCreated              = AU::get($data['yearCreated']);
            $this->feetLong                 = AU::get($data['feetLong']);
            $this->vin                      = AU::get($data['vin']);
            $this->uniqueReference          = AU::get($data['uniqueReference']);
            $this->reference1               = AU::get($data['reference1']);
            $this->reference2               = AU::get($data['reference2']);
            $this->reference3               = AU::get($data['reference3']);
            $this->reference4               = AU::get($data['reference4']);
            $this->reference5               = AU::get($data['reference5']);
        }
    }

    public function clean()
    {
        $this->vendorId                     = IU::getInt($this->vendorId);
        $this->vehicleTypeId                = IU::getInt($this->vehicleTypeId);
        $this->vendorLocationId             = IU::getInt($this->vendorLocationId);
        $this->vehicleModelId               = IU::getInt($this->vehicleModelId);
        $this->vehicleClassId               = IU::getInt($this->vehicleClassId);
        $this->transmissionId               = IU::getInt($this->transmissionId);
        $this->fuelTypeId                   = IU::getInt($this->fuelTypeId);
        $this->accommodates                 = IU::getInt($this->accommodates);
        $this->yearCreated                  = IU::getInt($this->yearCreated);
    }

    /**
     * @return array
     */
    public function jsonSerialize()
    {
        $object['vendorId']                 = $this->vendorId;
        $object['vehicleTypeId']            = $this->vehicleTypeId;
        $object['vendorLocationId']         = $this->vendorLocationId;
        $object['vehicleModelId']           = $this->vehicleModelId;
        $object['vehicleClassId']           = $this->vehicleClassId;
        $object['transmissionId']           = $this->transmissionId;
        $object['fuelTypeId']               = $this->fuelTypeId;
        $object['accommodates']             = $this->accommodates;
        $object['yearCreated']              = $this->yearCreated;
        $object['feetLong']                 = $this->feetLong;
        $object['uniqueReference']          = $this->uniqueReference;
        $object['reference1']               = $this->reference1;
        $object['reference2']               = $this->reference2;
        $object['reference3']               = $this->reference3;
        $object['reference4']               = $this->reference4;
        $object['reference5']               = $this->reference5;

        return $object;
    }

    public function validate()
    {
        // TODO: Implement validate() method.
    }

}