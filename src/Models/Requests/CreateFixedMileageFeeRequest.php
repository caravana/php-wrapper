<?php

namespace Caravana\Core\Models\Requests;


use Caravana\API\Exceptions\EntityValidationException;
use Caravana\API\Exceptions\RequiredFieldMissingException;
use Caravana\Core\Models\Requests\Base\BaseCreateFixedMileageFeeRequest;
use Caravana\Core\Models\Requests\Contracts\Validatable;
use jamesvweston\Utilities\ArrayUtil AS AU;
use jamesvweston\Utilities\InputUtil;

class CreateFixedMileageFeeRequest extends BaseCreateFixedMileageFeeRequest implements Validatable
{

    /**
     * @param   array|null $data
     */
    public function __construct($data = null)
    {
        if (is_array($data))
        {
            $this->freeMiles                = AU::get($data['freeMiles']);
            $this->mileageFee               = AU::get($data['mileageFee']);
        }
    }

    /**
     * @return array
     */
    public function jsonSerialize()
    {
        $object['freeMiles']                = $this->freeMiles;
        $object['mileageFee']               = $this->mileageFee;

        return $object;
    }

    public function validate()
    {
        /**
         * Validate required fields
         */
        if (is_null($this->freeMiles))
            throw new RequiredFieldMissingException('CreateFixedMileageFeeRequest', 'freeMiles');

        if (is_null($this->mileageFee))
            throw new RequiredFieldMissingException('CreateFixedMileageFeeRequest', 'mileageFee');

        /**
         * Validate expected data types
         */
        if (is_null(InputUtil::getInt($this->freeMiles)))
            throw new EntityValidationException('CreateFixedMileageFeeRequest', 'freeMiles', 'integer expected', $this->freeMiles);

        /**
         * Allow integer or float
         */
        if (is_null(InputUtil::getFloat($this->mileageFee)) && is_null(InputUtil::getInt($this->mileageFee)))
            throw new EntityValidationException('CreateFixedMileageFeeRequest', 'mileageFee', 'float expected', $this->mileageFee);
    }

    public function clean ()
    {
        $this->freeMiles                    = InputUtil::getInt($this->freeMiles);
        $this->mileageFee                   = !is_null(InputUtil::getFloat($this->mileageFee)) ? InputUtil::getFloat($this->mileageFee) : InputUtil::getInt($this->mileageFee);
    }
}