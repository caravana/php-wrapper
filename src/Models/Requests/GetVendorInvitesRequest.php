<?php

namespace Caravana\Core\Models\Requests;


use Caravana\Core\Models\Requests\Base\BaseGetVendorInvitesRequest;
use Caravana\Core\Models\Requests\Contracts\Validatable;
use jamesvweston\Utilities\ArrayUtil AS AU;

class GetVendorInvitesRequest extends BaseGetVendorInvitesRequest implements Validatable
{

    /**
     * @param   array|null $data
     */
    public function __construct($data = null)
    {
        if (is_array($data))
        {
            $this->ids                      = AU::get($data['ids']);
            $this->vendorIds                = AU::get($data['vendorIds']);
            $this->userIds                  = AU::get($data['userIds']);
            $this->vendorInviteStatusIds    = AU::get($data['vendorInviteStatusIds']);
            $this->createdByIds             = AU::get($data['createdByIds']);
            $this->limit                    = AU::get($data['limit'], 80);
            $this->page                     = AU::get($data['page'], 1);
        }
    }

    /**
     * @return array
     */
    public function jsonSerialize()
    {
        $object['ids']                      = $this->ids;
        $object['vendorIds']                = $this->vendorIds;
        $object['userIds']                  = $this->userIds;
        $object['vendorInviteStatusIds']    = $this->vendorInviteStatusIds;
        $object['createdByIds']             = $this->createdByIds;
        $object['limit']                    = $this->limit;
        $object['page']                     = $this->page;

        return $object;
    }


    public function validate()
    {
        // TODO: Implement validate() method.
    }

}