<?php

namespace Caravana\Core\Models\Requests;


use Caravana\Core\Models\Requests\Base\BaseUpdatePasswordRequest;
use Caravana\Core\Models\Requests\Contracts\Validatable;
use jamesvweston\Utilities\ArrayUtil AS AU;

class UpdatePasswordRequest extends BaseUpdatePasswordRequest implements Validatable
{

    /**
     * @param   array|null $data
     */
    public function __construct($data = null)
    {
        if (is_array($data))
        {
            $this->currentPassword          = AU::get($data['currentPassword']);
            $this->newPassword              = AU::get($data['newPassword']);
        }
    }

    /**
     * @return array
     */
    public function jsonSerialize()
    {
        $object['currentPassword']          = $this->currentPassword;
        $object['newPassword']              = $this->newPassword;

        return $object;
    }


    public function validate()
    {
        // TODO: Implement validate() method.
    }

}