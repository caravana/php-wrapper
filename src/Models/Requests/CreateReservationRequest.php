<?php

namespace Caravana\Core\Models\Requests;


use Caravana\API\Exceptions\EntityValidationException;
use Caravana\API\Exceptions\RequiredFieldMissingException;
use Caravana\Core\Models\Requests\Base\BaseCreateReservationRequest;
use Caravana\Core\Models\Requests\Contracts\Validatable;
use jamesvweston\Utilities\ArrayUtil AS AU;
use jamesvweston\Utilities\DateUtil AS DU;
use jamesvweston\Utilities\InputUtil;

class CreateReservationRequest extends BaseCreateReservationRequest implements Validatable
{

    /**
     * @param   array|null $data
     */
    public function __construct($data = null)
    {
        if (is_array($data))
        {
            $this->userId                   = AU::get($data['userId']);
            $this->vendorId                 = AU::get($data['vendorId']);
            $this->serviceIds               = AU::get($data['serviceIds']);
            $this->startDate                = AU::get($data['startDate']);
            $this->endDate                  = AU::get($data['endDate']);
        }
    }

    /**
     * @return array
     */
    public function jsonSerialize()
    {
        $object['userId']                   = $this->userId;
        $object['vendorId']                 = $this->vendorId;
        $object['serviceIds']               = $this->serviceIds;
        $object['startDate']                = $this->startDate;
        $object['endDate']                  = $this->endDate;

        return $object;
    }

    public function validate()
    {
        /**
         * Validate required fields
         */
        if (is_null($this->userId))
            throw new RequiredFieldMissingException('CreateReservationRequest', 'userId');

        if (is_null($this->vendorId))
            throw new RequiredFieldMissingException('CreateReservationRequest', 'vendorId');

        if (is_null($this->serviceIds))
            throw new RequiredFieldMissingException('CreateReservationRequest', 'serviceIds');

        if (is_null($this->startDate))
            throw new RequiredFieldMissingException('CreateReservationRequest', 'startDate');

        if (is_null($this->endDate))
            throw new RequiredFieldMissingException('CreateReservationRequest', 'endDate');
        /**
         * Validate expected data types
         */
        if (is_null(InputUtil::getInt($this->userId)))
            throw new EntityValidationException('CreateReservationRequest', 'userId', 'Expected integer value', $this->userId);

        if (is_null(InputUtil::getInt($this->vendorId)))
            throw new EntityValidationException('CreateReservationRequest', 'vendorId', 'Expected numeric value', $this->vendorId);

        if (DU::validate($this->startDate, 'YYYY-MM-DD') === false)
            throw new EntityValidationException('CreateReservationRequest', 'startDate', 'Expected YYYY-MM-DD format', $this->startDate);

        if (DU::validate($this->endDate, 'YYYY-MM-DD') === false)
            throw new EntityValidationException('CreateReservationRequest', 'endDate', 'Expected YYYY-MM-DD format', $this->endDate);

        $serviceIds                         = explode(',', $this->serviceIds);
        $newItems                           = [];
        foreach ($serviceIds AS $item)
        {
            $item                           = trim($item);
            $tempItem                       = InputUtil::getInt($item);
            if (is_null($tempItem))
                throw new EntityValidationException('CreateReservationRequest', 'serviceIds', 'Expected integer value for item', $tempItem);

            $newItems[]                     = $tempItem;
        }
        $this->serviceIds                   = implode(',', $newItems);
    }

    public function clean ()
    {
        $this->userId                       = InputUtil::getInt($this->userId);
        $this->vendorId                     = InputUtil::getInt($this->vendorId);
    }

}