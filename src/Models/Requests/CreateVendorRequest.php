<?php

namespace Caravana\Core\Models\Requests;


use Caravana\Core\Models\Requests\Base\BaseCreateVendorRequest;
use Caravana\Core\Models\Requests\Contracts\Validatable;
use jamesvweston\Utilities\ArrayUtil AS AU;

class CreateVendorRequest extends BaseCreateVendorRequest implements Validatable
{

    /**
     * @param   array|null $data
     */
    public function __construct($data = null)
    {
        if (is_array($data))
        {
            $this->name                     = AU::get($data['name']);
            $this->ownerId                  = AU::get($data['ownerId']);
            $this->bookingPolicyId          = AU::get($data['bookingPolicyId']);
            $this->cancellationPolicyId     = AU::get($data['cancellationPolicyId']);
        }
    }

    /**
     * @return array
     */
    public function jsonSerialize()
    {
        $object['name']                     = $this->name;
        $object['ownerId']                  = $this->ownerId;
        $object['bookingPolicyId']          = $this->bookingPolicyId;
        $object['cancellationPolicyId']     = $this->cancellationPolicyId;

        return $object;
    }


    public function validate()
    {
        // TODO: Implement validate() method.
    }
    
}