<?php

namespace Caravana\Core\Models\Requests;


use Caravana\API\Exceptions\EntityValidationException;
use Caravana\API\Exceptions\RequiredFieldMissingException;
use Caravana\Core\Models\Requests\Base\BaseCreateSeasonRequest;
use Caravana\Core\Models\Requests\Contracts\Validatable;
use jamesvweston\Utilities\ArrayUtil AS AU;
use jamesvweston\Utilities\DateUtil;
use jamesvweston\Utilities\InputUtil;

class CreateSeasonRequest extends BaseCreateSeasonRequest implements Validatable
{

    /**
     * @param   array|null $data
     */
    public function __construct($data = null)
    {
        if (is_array($data))
        {
            $this->vendorId                 = AU::get($data['vendorId']);
            $this->startDate                = AU::get($data['startDate']);
            $this->endDate                  = AU::get($data['endDate']);
            $this->name                     = AU::get($data['name']);
        }
    }

    /**
     * @return array
     */
    public function jsonSerialize()
    {
        $object['vendorId']                 = $this->vendorId;
        $object['startDate']                = $this->startDate;
        $object['endDate']                  = $this->endDate;
        $object['name']                     = $this->name;

        return $object;
    }

    public function validate()
    {
        /**
         * Validate required fields
         */
        if (is_null($this->startDate))
            throw new RequiredFieldMissingException('CreateSeasonRequest', 'startDate');

        if (is_null($this->endDate))
            throw new RequiredFieldMissingException('CreateSeasonRequest', 'endDate');

        if (is_null($this->name))
            throw new RequiredFieldMissingException('CreateSeasonRequest', 'name');

        /**
         * Validate expected data types
         */
        if (!DateUtil::validate($this->startDate, 'YYYY-MM-DD'))
            throw new EntityValidationException('CreateSeasonRequest', 'startDate', 'YYYY-MM-DD required', $this->startDate);

        if (!DateUtil::validate($this->endDate, 'YYYY-MM-DD'))
            throw new EntityValidationException('CreateSeasonRequest', 'endDate', 'YYYY-MM-DD required', $this->endDate);

        if (!is_string($this->name))
            throw new EntityValidationException('CreateSeasonRequest', 'name', 'string expected', $this->name);

    }

    public function clean ()
    {
        $this->vendorId                     = InputUtil::getInt($this->vendorId);
        $this->name                         = trim($this->name);
    }

}