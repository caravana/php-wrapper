<?php

namespace Caravana\Core\Models\Requests;


use Caravana\API\Exceptions\EntityValidationException;
use Caravana\API\Exceptions\RequiredFieldMissingException;
use Caravana\Core\Models\Requests\Base\BaseCreateConsignedVehicleRequest;
use Caravana\Core\Models\Requests\Contracts\Validatable;
use jamesvweston\Utilities\ArrayUtil AS AU;
use jamesvweston\Utilities\InputUtil;

class CreateConsignedVehicleRequest extends BaseCreateConsignedVehicleRequest implements Validatable
{

    /**
     * @param   array|null $data
     */
    public function __construct($data = null)
    {
        if (is_array($data))
        {
            $this->name                     = AU::get($data['name']);
            $this->description              = AU::get($data['description']);
            $this->rate                     = AU::get($data['rate']);
            $this->bookingPolicyId          = AU::get($data['bookingPolicyId']);
            $this->cancellationPolicyId     = AU::get($data['cancellationPolicyId']);
            $this->fixedMileageFeeId        = AU::get($data['fixedMileageFeeId']);
            $this->vendorId                 = AU::get($data['vendorId']);
            $this->vehicleTypeId            = AU::get($data['vehicleTypeId']);
            $this->vehicleClassId           = AU::get($data['vehicleClassId']);
        }
    }

    public function validate()
    {
        if (is_null($this->name))
            throw new RequiredFieldMissingException('CreateConsignedVehicleRequest', 'name');

        if (is_null($this->description))
            throw new RequiredFieldMissingException('CreateConsignedVehicleRequest', 'description');

        if (is_null($this->rate))
            throw new RequiredFieldMissingException('CreateConsignedVehicleRequest', 'rate');

        if (is_null($this->bookingPolicyId))
            throw new RequiredFieldMissingException('CreateConsignedVehicleRequest', 'bookingPolicyId');

        if (is_null($this->cancellationPolicyId))
            throw new RequiredFieldMissingException('CreateConsignedVehicleRequest', 'cancellationPolicyId');

        if (is_null($this->vendorId))
            throw new RequiredFieldMissingException('CreateConsignedVehicleRequest', 'vendorId');

        if (is_null($this->vehicleTypeId))
            throw new RequiredFieldMissingException('CreateConsignedVehicleRequest', 'vehicleTypeId');

        if (is_null($this->vehicleClassId))
            throw new RequiredFieldMissingException('CreateConsignedVehicleRequest', 'vehicleClassId');

        /**
         * Validate expected data types
         */
        if (!is_string($this->name))
            throw new EntityValidationException('CreateConsignedVehicleRequest', 'name', 'string expected', $this->name);

        if (!is_string($this->description))
            throw new EntityValidationException('CreateConsignedVehicleRequest', 'description', 'string expected', $this->name);

        if (is_null(InputUtil::getFloat($this->rate)))
            throw new EntityValidationException('CreateConsignedVehicleRequest', 'rate', 'Expected float value', $this->rate);

        if (is_null(InputUtil::getInt($this->bookingPolicyId)))
            throw new EntityValidationException('CreateConsignedVehicleRequest', 'bookingPolicyId', 'Expected integer value', $this->bookingPolicyId);

        if (is_null(InputUtil::getInt($this->cancellationPolicyId)))
            throw new EntityValidationException('CreateConsignedVehicleRequest', 'cancellationPolicyId', 'Expected integer value', $this->cancellationPolicyId);

        if (!is_null($this->fixedMileageFeeId))
        {
            if (is_null(InputUtil::getInt($this->fixedMileageFeeId)))
                throw new EntityValidationException('CreateConsignedVehicleRequest', 'fixedMileageFeeId', 'Expected integer value', $this->fixedMileageFeeId);
        }

        if (is_null(InputUtil::getInt($this->vendorId)))
            throw new EntityValidationException('CreateConsignedVehicleRequest', 'vendorId', 'Expected integer value', $this->vendorId);

        if (is_null(InputUtil::getInt($this->vehicleTypeId)))
            throw new EntityValidationException('CreateConsignedVehicleRequest', 'vehicleTypeId', 'Expected integer value', $this->vehicleTypeId);

        if (is_null(InputUtil::getInt($this->vehicleClassId)))
            throw new EntityValidationException('CreateConsignedVehicleRequest', 'vehicleClassId', 'Expected integer value', $this->vehicleClassId);
    }

    /**
     * @return array
     */
    public function jsonSerialize()
    {
        $object['name']                     = $this->name;
        $object['description']              = $this->description;
        $object['bookingPolicyId']          = $this->bookingPolicyId;
        $object['cancellationPolicyId']     = $this->cancellationPolicyId;
        $object['fixedMileageFeeId']        = $this->fixedMileageFeeId;
        $object['vendorId']                 = $this->vendorId;
        $object['vehicleTypeId']            = $this->vehicleTypeId;
        $object['vehicleClassId']           = $this->vehicleClassId;

        return $object;
    }

    public function clean ()
    {
        $this->rate                         = InputUtil::getFloat($this->rate);
        $this->bookingPolicyId              = InputUtil::getInt($this->bookingPolicyId);
        $this->cancellationPolicyId         = InputUtil::getInt($this->cancellationPolicyId);
        $this->fixedMileageFeeId            = InputUtil::getInt($this->fixedMileageFeeId);
        $this->vendorId                     = InputUtil::getInt($this->vendorId);
        $this->vehicleTypeId                = InputUtil::getInt($this->vehicleTypeId);
        $this->vehicleClassId               = InputUtil::getInt($this->vehicleClassId);
    }
}