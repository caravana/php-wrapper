<?php

namespace Caravana\Core\Models\Requests;


use Caravana\API\Exceptions\RequiredFieldMissingException;
use Caravana\Core\Models\Requests\Base\BaseCreateVehicleFeatureRequest;
use Caravana\Core\Models\Requests\Contracts\Validatable;
use jamesvweston\Utilities\ArrayUtil AS AU;

class CreateVehicleFeatureRequest extends BaseCreateVehicleFeatureRequest implements Validatable
{

    /**
     * @param   array|null $data
     */
    public function __construct($data = null)
    {
        if (is_array($data))
        {
            $this->featureId                = AU::get($data['featureId']);
            $this->value                    = AU::get($data['value']);
        }
    }

    public function validate()
    {
        /**
         * Validate required fields
         */
        if (is_null($this->featureId) || empty(trim($this->featureId)))
            throw new RequiredFieldMissingException('CreateVehicleFeatureRequest', 'featureId');

        if (is_null($this->value) || empty(trim($this->value)))
            throw new RequiredFieldMissingException('CreateVehicleFeatureRequest', 'value');
    }

    /**
     * @return  array
     */
    public function jsonSerialize()
    {
        $object['featureId']                = $this->featureId;
        $object['value']                    = $this->value;

        return $object;
    }
}