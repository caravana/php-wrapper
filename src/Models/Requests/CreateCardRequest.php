<?php

namespace Caravana\Core\Models\Requests;


use Caravana\API\Exceptions\RequiredFieldMissingException;
use Caravana\Core\Models\Requests\Base\BaseCreateCardRequest;
use Caravana\Core\Models\Requests\Contracts\Validatable;
use jamesvweston\Utilities\ArrayUtil AS AU;
use jamesvweston\Utilities\InputUtil;

class CreateCardRequest extends BaseCreateCardRequest implements Validatable
{

    /**
     * @param   array|null $data
     */
    public function __construct($data = null)
    {
        if (is_array($data))
        {
            $this->name                     = AU::get($data['name']);
            $this->number                   = AU::get($data['number']);
            $this->month                    = AU::get($data['month']);
            $this->year                     = AU::get($data['year']);
            $this->cvc                      = AU::get($data['cvc']);
            $this->postalCode               = AU::get($data['postalCode']);
        }
    }
    
    public function validate()
    {
        if (is_null($this->name))
            throw new RequiredFieldMissingException('CreateCardRequest', 'name');

        if (is_null($this->number))
            throw new RequiredFieldMissingException('CreateCardRequest', 'number');

        if (is_null($this->month))
            throw new RequiredFieldMissingException('CreateCardRequest', 'month');

        if (is_null($this->year))
            throw new RequiredFieldMissingException('CreateCardRequest', 'year');

        if (is_null($this->cvc))
            throw new RequiredFieldMissingException('CreateCardRequest', 'cvc');

        if (is_null($this->postalCode))
            throw new RequiredFieldMissingException('CreateCardRequest', 'postalCode');
    }

    /**
     * @return array
     */
    public function jsonSerialize()
    {
        $object['name']                     = $this->name;
        $object['number']                   = $this->number;
        $object['month']                    = $this->month;
        $object['year']                     = $this->year;
        $object['cvc']                      = $this->cvc;
        $object['postalCode']               = $this->postalCode;

        return $object;
    }

    public function clean ()
    {
        $this->year                         = InputUtil::getInt($this->year);
    }
}