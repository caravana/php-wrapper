<?php

namespace Caravana\Core\Models\Requests;


use Caravana\Core\Models\Requests\Base\BaseCreateUserInviteRequest;
use Caravana\Core\Models\Requests\Contracts\Validatable;
use jamesvweston\Utilities\ArrayUtil AS AU;

class CreateUserInviteRequest extends BaseCreateUserInviteRequest implements Validatable
{

    /**
     * CreateUserInvite constructor.
     * @param   array|null $data
     */
    public function __construct($data = null)
    {
        if (is_array($data))
        {
            $this->firstName                = AU::get($data['firstName']);
            $this->lastName                 = AU::get($data['lastName']);
            $this->email                    = AU::get($data['email']);
        }
    }


    /**
     * @return array
     */
    public function jsonSerialize()
    {
        $object['firstName']                = $this->firstName;
        $object['lastName']                 = $this->lastName;
        $object['email']                    = $this->email;

        return $object;
    }


    public function validate()
    {
        // TODO: Implement validate() method.
    }
    
}