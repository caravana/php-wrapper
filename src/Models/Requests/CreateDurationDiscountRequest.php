<?php

namespace Caravana\Core\Models\Requests;


use Caravana\API\Exceptions\EntityValidationException;
use Caravana\API\Exceptions\RequiredFieldMissingException;
use Caravana\Core\Models\Requests\Base\BaseCreateDurationDiscountRequest;
use Caravana\Core\Models\Requests\Contracts\Validatable;
use jamesvweston\Utilities\ArrayUtil AS AU;
use jamesvweston\Utilities\InputUtil;

class CreateDurationDiscountRequest extends BaseCreateDurationDiscountRequest implements Validatable
{

    public function __construct($data = null)
    {
        if (is_array($data))
        {
            $this->minimumDays              = AU::get($data['minimumDays']);
            $this->percentDiscount          = AU::get($data['percentDiscount']);
            $this->rentalId                 = AU::get($data['rentalId']);
        }
    }


    /**
     * @return array
     */
    public function jsonSerialize()
    {
        $object['minimumDays']              = $this->minimumDays;
        $object['percentDiscount']          = $this->percentDiscount;
        $object['rentalId']                 = $this->rentalId;

        return $object;
    }


    public function validate()
    {
        /**
         * Validate required fields
         */
        if (is_null($this->minimumDays))
            throw new RequiredFieldMissingException('CreateDurationDiscountRequest', 'minimumDays');

        if (is_null($this->percentDiscount))
            throw new RequiredFieldMissingException('CreateDurationDiscountRequest', 'percentDiscount');

        /**
         * Validate expected data types
         */
        if (is_null(InputUtil::getInt($this->minimumDays)))
            throw new EntityValidationException('CreateDurationDiscountRequest', 'minimumDays', 'Expected integer value', $this->minimumDays);

        if (is_null(InputUtil::getFloat($this->percentDiscount)))
            throw new EntityValidationException('CreateDurationDiscountRequest', 'percentDiscount', 'Expected float value', $this->percentDiscount);
    }

    public function clean ()
    {
        $this->minimumDays                  = InputUtil::getInt($this->minimumDays);
        $this->percentDiscount              = InputUtil::getFloat($this->percentDiscount);
        $this->rentalId                     = InputUtil::getInt($this->rentalId);
    }

}