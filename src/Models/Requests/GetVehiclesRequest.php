<?php

namespace Caravana\Core\Models\Requests;


use Caravana\Core\Models\Requests\Base\BaseGetVehiclesRequest;
use Caravana\Core\Models\Requests\Contracts\Validatable;
use jamesvweston\Utilities\ArrayUtil AS AU;

class GetVehiclesRequest extends BaseGetVehiclesRequest implements Validatable
{

    /**
     * @param   array|null $data
     */
    public function __construct($data = null)
    {
        if (is_array($data))
        {
            $this->ids                      = AU::get($data['ids']);

            $this->vehicleTypeIds           = AU::get($data['vehicleTypeIds']);
            $this->vehicleClassIds          = AU::get($data['vehicleClassIds']);
            $this->vehicleModelIds          = AU::get($data['vehicleModelIds']);
            $this->vehicleMakeIds           = AU::get($data['vehicleMakeIds']);
            $this->vendorIds                = AU::get($data['vendorIds']);
            $this->inRental                 = AU::get($data['inRental']);
            $this->uniqueReferences         = AU::get($data['uniqueReferences']);
            
            
            
            $this->limit                    = AU::get($data['limit'], 80);
            $this->page                     = AU::get($data['page'], 1);
        }
    }

    /**
     * @return array
     */
    public function jsonSerialize()
    {
        $object['ids']                      = $this->ids;
        $object['vehicleTypeIds']           = $this->vehicleTypeIds;
        $object['vehicleClassIds']          = $this->vehicleClassIds;
        $object['vehicleModelIds']          = $this->vehicleModelIds;
        $object['vehicleMakeIds']           = $this->vehicleMakeIds;
        $object['vendorIds']                = $this->vendorIds;
        $object['inRental']                 = $this->inRental;
        $object['uniqueReferences']         = $this->uniqueReferences;
        $object['limit']                    = $this->limit;
        $object['page']                     = $this->page;

        return $object;
    }


    public function validate()
    {
        // TODO: Implement validate() method.
    }
}