<?php

namespace Caravana\Core\Models\Requests;


use Caravana\API\Exceptions\EntityValidationException;
use Caravana\API\Exceptions\RequiredFieldMissingException;
use Caravana\Core\Models\Requests\Base\BaseCreateRegistrationRequest;
use Caravana\Core\Models\Requests\Contracts\Validatable;
use jamesvweston\Utilities\ArrayUtil AS AU;
use jamesvweston\Utilities\DateUtil;
use jamesvweston\Utilities\InputUtil;

class CreateRegistrationRequest extends BaseCreateRegistrationRequest implements Validatable
{

    /**
     * @param   array|null $data
     */
    public function __construct($data = null)
    {
        if (is_array($data))
        {
            $this->licensePlate             = AU::get($data['licensePlate']);
            $this->issuedDate               = AU::get($data['issuedDate']);
            $this->expirationDate           = AU::get($data['expirationDate']);
            $this->subdivisionId            = AU::get($data['subdivisionId']);
        }
    }

    public function validate()
    {
        if (is_null($this->licensePlate))
            throw new RequiredFieldMissingException('CreateRegistrationRequest', 'licensePlate');

        if (is_null($this->subdivisionId))
            throw new RequiredFieldMissingException('CreateRegistrationRequest', 'subdivisionId');


        /**
         * Validate expected data types
         */
        if (!is_null($this->issuedDate))
        {
            if (!DateUtil::validate($this->issuedDate, 'YYYY-MM-DD'))
                throw new EntityValidationException('CreateRegistrationRequest', 'issuedDate', 'YYYY-MM-DD required', $this->issuedDate);
        }

        if (!is_null($this->expirationDate))
        {
            if (!DateUtil::validate($this->expirationDate, 'YYYY-MM-DD'))
                throw new EntityValidationException('CreateRegistrationRequest', 'expirationDate', 'YYYY-MM-DD required', $this->expirationDate);
        }

        if (is_null(InputUtil::getInt($this->subdivisionId)))
            throw new EntityValidationException('CreateRegistrationRequest', 'subdivisionId', 'Expected integer value', $this->subdivisionId);
    }

    /**
     * @return array
     */
    public function jsonSerialize()
    {
        $object['licensePlate']             = $this->licensePlate;
        $object['issuedDate']               = $this->issuedDate;
        $object['expirationDate']           = $this->expirationDate;
        $object['subdivisionId']            = $this->subdivisionId;

        return $object;
    }

    public function clean ()
    {
        $this->subdivisionId                = InputUtil::getInt($this->subdivisionId);
    }

}