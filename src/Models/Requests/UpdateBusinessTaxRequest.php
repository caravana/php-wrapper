<?php

namespace Caravana\Core\Models\Requests;


use Caravana\API\Exceptions\RequiredFieldMissingException;
use Caravana\Core\Models\Requests\Base\BaseUpdateBusinessTaxRequest;
use Caravana\Core\Models\Requests\Contracts\Validatable;
use jamesvweston\Utilities\ArrayUtil AS AU;

class UpdateBusinessTaxRequest extends BaseUpdateBusinessTaxRequest implements Validatable
{

    /**
     * @param   array|null $data
     */
    public function __construct($data = null)
    {
        if (is_array($data))
        {
            $this->businessName             = AU::get($data['businessName']);
            $this->businessTaxId            = AU::get($data['businessTaxId']);
            $this->firstName                = AU::get($data['firstName']);
            $this->lastName                 = AU::get($data['lastName']);
            $this->ssnLastFour              = AU::get($data['ssnLastFour']);
            $this->formationDate            = AU::get($data['formationDate']);
            $this->location                 = !is_null(AU::get($data['location'])) ? new CreateLocationRequest(AU::get($data['location'])) : AU::get($data['location']);
            $this->tosAcceptanceDate        = AU::get($data['tosAcceptanceDate']);
            $this->tosAcceptanceIp          = AU::get($data['tosAcceptanceIp']);
        }
    }
    
    public function validate()
    {
        /**
         * Validate required fields
         */
        if (is_null($this->businessName) || empty(trim($this->businessName)))
            throw new RequiredFieldMissingException('UpdateBusinessTaxRequest', 'businessName');

        if (is_null($this->businessTaxId) || empty(trim($this->businessTaxId)))
            throw new RequiredFieldMissingException('UpdateBusinessTaxRequest', 'businessTaxId');

        if (is_null($this->firstName) || empty(trim($this->firstName)))
            throw new RequiredFieldMissingException('UpdateBusinessTaxRequest', 'firstName');

        if (is_null($this->lastName) || empty(trim($this->lastName)))
            throw new RequiredFieldMissingException('UpdateBusinessTaxRequest', 'lastName');

        if (is_null($this->ssnLastFour) || empty(trim($this->ssnLastFour)))
            throw new RequiredFieldMissingException('UpdateBusinessTaxRequest', 'ssnLastFour');

        if (is_null($this->formationDate) || empty(trim($this->formationDate)))
            throw new RequiredFieldMissingException('UpdateBusinessTaxRequest', 'formationDate');

        if (!($this->location instanceof CreateLocationRequest))
            throw new RequiredFieldMissingException('UpdateBusinessTaxRequest', 'location');

        if (is_null($this->tosAcceptanceDate) || empty(trim($this->tosAcceptanceDate)))
            throw new RequiredFieldMissingException('UpdateBusinessTaxRequest', 'tosAcceptanceDate');

        if (is_null($this->tosAcceptanceIp) || empty(trim($this->tosAcceptanceIp)))
            throw new RequiredFieldMissingException('UpdateBusinessTaxRequest', 'tosAcceptanceIp');
        
        $this->location->validate();
    }

    /**
     * @return array
     */
    public function jsonSerialize()
    {
        $object['businessName']             = $this->businessName;
        $object['businessTaxId']            = $this->businessTaxId;
        $object['firstName']                = $this->firstName;
        $object['lastName']                 = $this->lastName;
        $object['ssnLastFour']              = $this->ssnLastFour;
        $object['formationDate']            = $this->formationDate;
        $object['location']                 = ($this->location instanceof \JsonSerializable) ? $this->location->jsonSerialize() : $this->location;
        $object['tosAcceptanceDate']        = $this->tosAcceptanceDate;
        $object['tosAcceptanceIp']          = $this->tosAcceptanceIp;

        return $object;
    }
}