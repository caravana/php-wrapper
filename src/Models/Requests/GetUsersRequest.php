<?php

namespace Caravana\Core\Models\Requests;


use Caravana\Core\Models\Requests\Base\BaseGetUsersRequest;
use Caravana\Core\Models\Requests\Contracts\Validatable;
use jamesvweston\Utilities\ArrayUtil AS AU;

class GetUsersRequest extends BaseGetUsersRequest implements Validatable
{

    /**
     * @param   array|null $data
     */
    public function __construct($data = null)
    {
        if (is_array($data))
        {
            $this->ids                      = AU::get($data['ids']);
            $this->emails                   = AU::get($data['emails']);
            $this->firstNames               = AU::get($data['firstNames']);
            $this->lastNames                = AU::get($data['lastNames']);
            $this->limit                    = AU::get($data['limit'], 80);
            $this->page                     = AU::get($data['page'], 1);
        }
    }

    /**
     * @return array
     */
    public function jsonSerialize()
    {
        $object['ids']                      = $this->ids;
        $object['emails']                   = $this->emails;
        $object['firstNames']               = $this->firstNames;
        $object['lastNames']                = $this->lastNames;
        $object['limit']                    = $this->limit;
        $object['page']                     = $this->page;
        
        return $object;
    }
    
    
    public function validate()
    {
        // TODO: Implement validate() method.
    }

}