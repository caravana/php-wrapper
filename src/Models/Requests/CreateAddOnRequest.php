<?php

namespace Caravana\Core\Models\Requests;


use Caravana\API\Exceptions\EntityValidationException;
use Caravana\API\Exceptions\RequiredFieldMissingException;
use Caravana\Core\Models\Requests\Base\BaseCreateAddOnRequest;
use Caravana\Core\Models\Requests\Contracts\Validatable;
use jamesvweston\Utilities\ArrayUtil AS AU;
use jamesvweston\Utilities\InputUtil;

class CreateAddOnRequest extends BaseCreateAddOnRequest implements Validatable
{

    /**
     * @param   array|null $data
     */
    public function __construct($data = null)
    {
        if (is_array($data))
        {
            $this->name                     = AU::get($data['name']);
            $this->description              = AU::get($data['description']);
            $this->vendorId                 = AU::get($data['vendorId']);
            $this->rate                     = AU::get($data['rate']);
            $this->rateTypeId               = AU::get($data['rateTypeId']);
        }
    }

    public function validate()
    {
        if (is_null($this->name))
            throw new RequiredFieldMissingException('CreateAddOnRequest', 'name');

        if (is_null($this->description))
            throw new RequiredFieldMissingException('CreateAddOnRequest', 'description');

        if (is_null($this->vendorId))
            throw new RequiredFieldMissingException('CreateAddOnRequest', 'vendorId');

        if (is_null($this->rate))
            throw new RequiredFieldMissingException('CreateAddOnRequest', 'rate');

        if (is_null($this->rateTypeId))
            throw new RequiredFieldMissingException('CreateAddOnRequest', 'rateTypeId');

        /**
         * Validate expected data types
         */
        if (!is_string($this->name))
            throw new EntityValidationException('CreateAddOnRequest', 'name', 'string expected', $this->name);

        if (!is_string($this->description))
            throw new EntityValidationException('CreateAddOnRequest', 'description', 'string expected', $this->name);

        if (is_null(InputUtil::getInt($this->vendorId)))
            throw new EntityValidationException('CreateAddOnRequest', 'vendorId', 'Expected float value', $this->vendorId);

        if (is_null(InputUtil::getFloat($this->rate)))
            throw new EntityValidationException('CreateAddOnRequest', 'rate', 'Expected float value', $this->rate);

        if (is_null(InputUtil::getInt($this->rateTypeId)))
            throw new EntityValidationException('CreateAddOnRequest', 'rateTypeId', 'Expected float value', $this->rateTypeId);
    }

    /**
     * @return array
     */
    public function jsonSerialize()
    {
        $object['name']                     = $this->name;
        $object['description']              = $this->description;
        $object['vendorId']                 = $this->vendorId;
        $object['rate']                     = $this->rate;
        $object['rateTypeId']               = $this->rateTypeId;

        return $object;
    }

    public function clean ()
    {
        $this->vendorId                     = InputUtil::getInt($this->vendorId);
        $this->rate                         = InputUtil::getFloat($this->rate);
        $this->rateTypeId                   = InputUtil::getInt($this->rateTypeId);
    }

}