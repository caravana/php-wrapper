<?php

namespace Caravana\Core\Models\Requests;


use Caravana\API\Exceptions\EntityValidationException;
use Caravana\API\Exceptions\RequiredFieldMissingException;
use Caravana\Core\Models\Requests\Base\BaseCreateSeasonalRateRequest;
use Caravana\Core\Models\Requests\Contracts\Validatable;
use jamesvweston\Utilities\ArrayUtil AS AU;
use jamesvweston\Utilities\BooleanUtil;
use jamesvweston\Utilities\InputUtil;

class CreateSeasonalRateRequest extends BaseCreateSeasonalRateRequest implements Validatable
{

    /**
     * @param   array|null $data
     */
    public function __construct($data = null)
    {
        if (is_array($data))
        {
            $this->rentalId                 = AU::get($data['rentalId']);
            $this->seasonId                 = AU::get($data['seasonId']);
            $this->dailyRate                = AU::get($data['dailyRate']);
            $this->applyDurationDiscount    = AU::get($data['applyDurationDiscount']);
        }
    }

    /**
     * @return array
     */
    public function jsonSerialize()
    {
        $object['rentalId']                 = $this->rentalId;
        $object['seasonId']                 = $this->seasonId;
        $object['dailyRate']                = $this->dailyRate;
        $object['applyDurationDiscount']    = $this->applyDurationDiscount;

        return $object;
    }


    public function validate()
    {
        /**
         * Validate required fields
         */
        if (is_null($this->seasonId))
            throw new RequiredFieldMissingException('CreateSeasonalRateRequest', 'seasonId');

        if (is_null($this->dailyRate))
            throw new RequiredFieldMissingException('CreateSeasonalRateRequest', 'dailyRate');

        if (is_null($this->applyDurationDiscount))
            throw new RequiredFieldMissingException('CreateSeasonalRateRequest', 'applyDurationDiscount');

        /**
         * Validate expected data types
         */
        if (is_null(InputUtil::getInt($this->seasonId)))
            throw new EntityValidationException('CreateSeasonalRateRequest', 'seasonId', 'Expected integer value', $this->seasonId);

        if (!is_numeric($this->dailyRate))
            throw new EntityValidationException('CreateSeasonalRateRequest', 'dailyRate', 'Expected numeric value', $this->dailyRate);

        if (is_null(BooleanUtil::getBooleanValue($this->applyDurationDiscount)))
            throw new EntityValidationException('CreateSeasonalRateRequest', 'applyDurationDiscount', 'Expected boolean value', $this->applyDurationDiscount);
    }

    public function clean ()
    {
        $this->rentalId                     = InputUtil::getInt($this->seasonId);
        $this->seasonId                     = InputUtil::getInt($this->seasonId);
        $this->dailyRate                    = floatval($this->dailyRate);
        $this->applyDurationDiscount        = BooleanUtil::getBooleanValue($this->applyDurationDiscount);
    }

}