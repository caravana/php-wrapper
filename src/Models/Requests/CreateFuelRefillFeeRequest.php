<?php

namespace Caravana\Core\Models\Requests;


use Caravana\API\Exceptions\EntityValidationException;
use Caravana\API\Exceptions\RequiredFieldMissingException;
use Caravana\Core\Models\Requests\Base\BaseCreateFuelRefillFeeRequest;
use Caravana\Core\Models\Requests\Contracts\Validatable;
use jamesvweston\Utilities\ArrayUtil AS AU;
use jamesvweston\Utilities\InputUtil;

class CreateFuelRefillFeeRequest extends BaseCreateFuelRefillFeeRequest implements Validatable
{

    /**
     * @param   array|null $data
     */
    public function __construct($data = null)
    {
        if (is_array($data))
        {
            $this->perGallonFee             = AU::get($data['perGallonFee']);
        }
    }

    /**
     * @return array
     */
    public function jsonSerialize()
    {
        $object['perGallonFee']             = $this->perGallonFee;

        return $object;
    }

    public function validate()
    {
        /**
         * Validate required fields
         */
        if (is_null($this->perGallonFee))
            throw new RequiredFieldMissingException('CreateFuelRefillFeeRequest', 'perGallonFee');

        /**
         * Validate expected data types
         */

        /**
         * Allow integer or float
         */
        if (is_null(InputUtil::getFloat($this->perGallonFee)) && is_null(InputUtil::getInt($this->perGallonFee)))
            throw new EntityValidationException('CreateFuelRefillFeeRequest', 'perGallonFee', 'float expected', $this->perGallonFee);
    }

    public function clean ()
    {
        $this->perGallonFee                   = !is_null(InputUtil::getFloat($this->perGallonFee)) ? InputUtil::getFloat($this->perGallonFee) : InputUtil::getInt($this->perGallonFee);
    }

}