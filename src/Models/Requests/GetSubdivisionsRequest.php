<?php

namespace Caravana\Core\Models\Requests;


use Caravana\Core\Models\Requests\Base\BaseGetSubdivisionsRequest;
use Caravana\Core\Models\Requests\Contracts\Validatable;
use jamesvweston\Utilities\ArrayUtil AS AU;

class GetSubdivisionsRequest extends BaseGetSubdivisionsRequest implements Validatable
{

    /**
     * @param   array|null $data
     */
    public function __construct($data = null)
    {
        if (is_array($data))
        {
            $this->ids                      = AU::get($data['ids']);
            $this->countryIds               = AU::get($data['countryIds']);
            $this->subdivisionTypeIds       = AU::get($data['subdivisionTypeIds']);
            $this->names                    = AU::get($data['names']);
            $this->symbols                  = AU::get($data['symbols']);
            $this->localSymbols             = AU::get($data['localSymbols']);
            $this->limit                    = AU::get($data['limit'], 80);
            $this->page                     = AU::get($data['page'], 1);
        }
    }

    /**
     * @return array
     */
    public function jsonSerialize()
    {
        $object['ids']                      = $this->ids;
        $object['countryIds']               = $this->countryIds;
        $object['subdivisionTypeIds']       = $this->subdivisionTypeIds;
        $object['names']                    = $this->names;
        $object['symbols']                  = $this->symbols;
        $object['localSymbols']             = $this->localSymbols;
        $object['limit']                    = $this->limit;
        $object['page']                     = $this->page;
        
        return $object;
    }
    
    public function validate()
    {
        // TODO: Implement validate() method.
    }

}