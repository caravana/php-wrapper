<?php

namespace Caravana\Core\Models\Responses;


use Caravana\Core\Models\Responses\Base\BaseCard;
use jamesvweston\Utilities\ArrayUtil AS AU;

class Card extends BaseCard
{

    /**
     * @param   array|null $data
     */
    public function __construct($data = null)
    {
        if (is_array($data))
        {
            $this->id                       = AU::get($data['id']);
            $this->name                     = AU::get($data['name']);
            $this->postalCode               = AU::get($data['postalCode']);
            $this->expMonth                 = AU::get($data['expMonth']);
            $this->expYear                  = AU::get($data['expYear']);
            $this->lastFour                 = AU::get($data['lastFour']);
        }
    }

    /**
     * @return array
     */
    public function jsonSerialize()
    {
        $object['id']                       = $this->id;
        $object['name']                     = $this->name;
        $object['postalCode']               = $this->postalCode;
        $object['expMonth']                 = $this->expMonth;
        $object['expYear']                  = $this->expYear;
        $object['lastFour']                 = $this->lastFour;

        return $object;
    }
    
}