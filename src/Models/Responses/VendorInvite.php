<?php

namespace Caravana\Core\Models\Responses;


use Caravana\Core\Models\Responses\Base\BaseVendorInvite;
use jamesvweston\Utilities\ArrayUtil AS AU;

class VendorInvite extends BaseVendorInvite
{

    /**
     * @param   array|null $data
     */
    public function __construct($data = null)
    {
        if (is_array($data))
        {
            $this->id                       = AU::get($data['id']);
            $this->vendor                   = new Vendor(AU::get($data['vendor']));
            $this->user                     = new User(AU::get($data['user']));
            $this->status                   = new VendorInviteStatus(AU::get($data['status']));
            $this->createdBy                = new User(AU::get($data['createdBy']));
            $this->createdAt                = new DateTime(AU::get($data['createdAt']));
        }
    }

    /**
     * @return array
     */
    public function jsonSerialize()
    {
        $object['id']                       = $this->id;
        $object['vendor']                   = $this->vendor->jsonSerialize();
        $object['user']                     = $this->user->jsonSerialize();
        $object['status']                   = $this->status->jsonSerialize();
        $object['createdBy']                = $this->createdBy->jsonSerialize();
        $object['createdAt']                = $this->createdAt->jsonSerialize();

        return $object;
    }

}