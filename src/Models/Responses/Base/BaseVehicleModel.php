<?php

namespace Caravana\Core\Models\Responses\Base;


use Caravana\Core\Models\Responses\Contracts\VehicleModel AS VehicleModelContract;

abstract class BaseVehicleModel implements VehicleModelContract
{

    /**
     * @var int
     */
    protected $id;

    /**
     * @var string
     */
    protected $name;

    /**
     * @var BaseVehicleMake
     */
    protected $vehicleMake;

    /**
     * @var BaseVehicleType
     */
    protected $vehicleType;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return BaseVehicleMake
     */
    public function getVehicleMake()
    {
        return $this->vehicleMake;
    }

    /**
     * @param BaseVehicleMake $vehicleMake
     */
    public function setVehicleMake($vehicleMake)
    {
        $this->vehicleMake = $vehicleMake;
    }

    /**
     * @return BaseVehicleType
     */
    public function getVehicleType()
    {
        return $this->vehicleType;
    }

    /**
     * @param BaseVehicleType $vehicleType
     */
    public function setVehicleType($vehicleType)
    {
        $this->vehicleType = $vehicleType;
    }
    
}