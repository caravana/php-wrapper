<?php

namespace Caravana\Core\Models\Responses\Base;


use Caravana\Core\Models\Responses\Contracts\VendorInvite AS VendorInviteContract;

abstract class BaseVendorInvite implements VendorInviteContract
{

    /**
     * @var int
     */
    protected $id;

    /**
     * @var BaseVendor
     */
    protected $vendor;

    /**
     * @var BaseUser
     */
    protected $user;

    /**
     * @var BaseVendorInviteStatus
     */
    protected $status;

    /**
     * @var BaseUser
     */
    protected $createdBy;

    /**
     * @var BaseDateTime
     */
    protected $createdAt;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return BaseVendor
     */
    public function getVendor()
    {
        return $this->vendor;
    }

    /**
     * @param BaseVendor $vendor
     */
    public function setVendor($vendor)
    {
        $this->vendor = $vendor;
    }

    /**
     * @return BaseUser
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param BaseUser $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }

    /**
     * @return BaseVendorInviteStatus
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param BaseVendorInviteStatus $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return BaseUser
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * @param BaseUser $createdBy
     */
    public function setCreatedBy($createdBy)
    {
        $this->createdBy = $createdBy;
    }

    /**
     * @return BaseDateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param BaseDateTime $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }
    
}