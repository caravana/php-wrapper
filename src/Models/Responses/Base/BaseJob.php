<?php

namespace Caravana\Core\Models\Responses\Base;


use Caravana\Core\Models\Responses\Contracts\Job AS JobContract;

abstract class BaseJob implements JobContract
{

    /**
     * @var int
     */
    protected $id;

    /**
     * @var BaseVendor|null
     */
    protected $vendor;

    /**
     * @var BaseJobType
     */
    protected $jobType;

    /**
     * @var BaseJobStatus
     */
    protected $jobStatus;

    /**
     * @var BaseJobStatusHistory[]
     */
    protected $jobStatusHistory;

    /**
     * @var BaseJobItem[]
     */
    protected $jobItems;

    /**
     * @var int
     */
    protected $totalItems;

    /**
     * @var int
     */
    protected $totalSuccessful;

    /**
     * @var int
     */
    protected $totalErrors;

    /**
     * @var string|null
     */
    protected $originalFileName;

    /**
     * @var string|null
     */
    protected $convertedFileName;

    /**
     * @var string|null
     */
    protected $fileKey;

    /**
     * @var string|null
     */
    protected $filePath;

    /**
     * @var BaseUser
     */
    protected $createdBy;

    /**
     * @var BaseDateTime
     */
    protected $createdAt;

    /**
     * @var BaseDateTime|null
     */
    protected $startedAt;

    /**
     * @var BaseDateTime|null
     */
    protected $finishedAt;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return BaseVendor|null
     */
    public function getVendor()
    {
        return $this->vendor;
    }

    /**
     * @param BaseVendor|null $vendor
     */
    public function setVendor($vendor)
    {
        $this->vendor = $vendor;
    }

    /**
     * @return BaseJobType
     */
    public function getJobType()
    {
        return $this->jobType;
    }

    /**
     * @param BaseJobType $jobType
     */
    public function setJobType($jobType)
    {
        $this->jobType = $jobType;
    }

    /**
     * @return BaseJobStatus
     */
    public function getJobStatus()
    {
        return $this->jobStatus;
    }

    /**
     * @param BaseJobStatus $jobStatus
     */
    public function setJobStatus($jobStatus)
    {
        $this->jobStatus = $jobStatus;
    }

    /**
     * @return BaseJobStatusHistory[]
     */
    public function getJobStatusHistory()
    {
        return $this->jobStatusHistory;
    }

    /**
     * @param BaseJobStatusHistory $jobStatusHistory
     */
    public function addJobStatusHistory($jobStatusHistory)
    {
        $this->jobStatusHistory[] = $jobStatusHistory;
    }

    /**
     * @return BaseJobItem[]
     */
    public function getJobItems()
    {
        return $this->jobItems;
    }

    /**
     * @param BaseJobItem $jobItem
     */
    public function addJobItem($jobItem)
    {
        $this->jobItems[] = $jobItem;
    }

    /**
     * @return int
     */
    public function getTotalItems()
    {
        return $this->totalItems;
    }

    /**
     * @param int $totalItems
     */
    public function setTotalItems($totalItems)
    {
        $this->totalItems = $totalItems;
    }

    /**
     * @return int
     */
    public function getTotalSuccessful()
    {
        return $this->totalSuccessful;
    }

    /**
     * @param int $totalSuccessful
     */
    public function setTotalSuccessful($totalSuccessful)
    {
        $this->totalSuccessful = $totalSuccessful;
    }

    /**
     * @return int
     */
    public function getTotalErrors()
    {
        return $this->totalErrors;
    }

    /**
     * @param int $totalErrors
     */
    public function setTotalErrors($totalErrors)
    {
        $this->totalErrors = $totalErrors;
    }

    /**
     * @return null|string
     */
    public function getOriginalFileName()
    {
        return $this->originalFileName;
    }

    /**
     * @param null|string $originalFileName
     */
    public function setOriginalFileName($originalFileName)
    {
        $this->originalFileName = $originalFileName;
    }

    /**
     * @return null|string
     */
    public function getConvertedFileName()
    {
        return $this->convertedFileName;
    }

    /**
     * @param null|string $convertedFileName
     */
    public function setConvertedFileName($convertedFileName)
    {
        $this->convertedFileName = $convertedFileName;
    }

    /**
     * @return null|string
     */
    public function getFileKey()
    {
        return $this->fileKey;
    }

    /**
     * @param null|string $fileKey
     */
    public function setFileKey($fileKey)
    {
        $this->fileKey = $fileKey;
    }

    /**
     * @return null|string
     */
    public function getFilePath()
    {
        return $this->filePath;
    }

    /**
     * @param null|string $filePath
     */
    public function setFilePath($filePath)
    {
        $this->filePath = $filePath;
    }

    /**
     * @return BaseUser
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * @param BaseUser $createdBy
     */
    public function setCreatedBy($createdBy)
    {
        $this->createdBy = $createdBy;
    }

    /**
     * @return BaseDateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param BaseDateTime $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return BaseDateTime|null
     */
    public function getStartedAt()
    {
        return $this->startedAt;
    }

    /**
     * @param BaseDateTime|null $startedAt
     */
    public function setStartedAt($startedAt)
    {
        $this->startedAt = $startedAt;
    }

    /**
     * @return BaseDateTime|null
     */
    public function getFinishedAt()
    {
        return $this->finishedAt;
    }

    /**
     * @param BaseDateTime|null $finishedAt
     */
    public function setFinishedAt($finishedAt)
    {
        $this->finishedAt = $finishedAt;
    }
    
}