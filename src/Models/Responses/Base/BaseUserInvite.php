<?php

namespace Caravana\Core\Models\Responses\Base;


use Caravana\Core\Models\Responses\Contracts\UserInvite AS UserInviteContract;

abstract class BaseUserInvite implements UserInviteContract
{

    /**
     * @var     int
     */
    protected $id;

    /**
     * @var     string
     */
    protected $firstName;

    /**
     * @var     string
     */
    protected $lastName;

    /**
     * @var     string
     */
    protected $email;

    /**
     * @var     BaseUserInviteStatus
     */
    protected $status;

    /**
     * @var     BaseUser
     */
    protected $createdBy;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * @param string $firstName
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;
    }

    /**
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * @param string $lastName
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return BaseUserInviteStatus
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param BaseUserInviteStatus $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return BaseUser
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * @param BaseUser $createdBy
     */
    public function setCreatedBy($createdBy)
    {
        $this->createdBy = $createdBy;
    }
    
}