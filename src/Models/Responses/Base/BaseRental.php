<?php

namespace Caravana\Core\Models\Responses\Base;


use Caravana\Core\Models\Responses\Contracts\Rental AS RentalContract;

abstract class BaseRental implements RentalContract
{

    /**
     * @var int
     */
    protected $id;

    /**
     * @var string
     */
    protected $name;

    /**
     * @var string
     */
    protected $description;

    /**
     * @var float
     */
    protected $securityDeposit;

    /**
     * @var float
     */
    protected $taxPercent;

    /**
     * @var int
     */
    protected $minimumDays;

    /**
     * @var BaseRentalStatus
     */
    protected $rentalStatus;

    /**
     * @var BaseVehicle
     */
    protected $vehicle;

    /**
     * @var string[]
     */
    protected $unavailableDates;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return float
     */
    public function getSecurityDeposit()
    {
        return $this->securityDeposit;
    }

    /**
     * @param float $securityDeposit
     */
    public function setSecurityDeposit($securityDeposit)
    {
        $this->securityDeposit = $securityDeposit;
    }

    /**
     * @return float
     */
    public function getTaxPercent()
    {
        return $this->taxPercent;
    }

    /**
     * @param float $taxPercent
     */
    public function setTaxPercent($taxPercent)
    {
        $this->taxPercent = $taxPercent;
    }

    /**
     * @return int
     */
    public function getMinimumDays()
    {
        return $this->minimumDays;
    }

    /**
     * @param int $minimumDays
     */
    public function setMinimumDays($minimumDays)
    {
        $this->minimumDays = $minimumDays;
    }

    /**
     * @return BaseRentalStatus
     */
    public function getRentalStatus()
    {
        return $this->rentalStatus;
    }

    /**
     * @param BaseRentalStatus $rentalStatus
     */
    public function setRentalStatus($rentalStatus)
    {
        $this->rentalStatus = $rentalStatus;
    }
    
    /**
     * @return BaseVehicle
     */
    public function getVehicle()
    {
        return $this->vehicle;
    }

    /**
     * @param BaseVehicle $vehicle
     */
    public function setVehicle($vehicle)
    {
        $this->vehicle = $vehicle;
    }

    /**
     * @return string[]
     */
    public function getUnavailableDates()
    {
        return $this->unavailableDates;
    }

    /**
     * @param string[] $unavailableDates
     */
    public function setUnavailableDates($unavailableDates)
    {
        $this->unavailableDates = $unavailableDates;
    }
    
}