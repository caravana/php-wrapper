<?php

namespace Caravana\Core\Models\Responses\Base;


use Caravana\Core\Models\Responses\Contracts\JobItem AS JobItemContract;

abstract class BaseJobItem implements JobItemContract
{

    /**
     * @var int
     */
    protected $id;

    /**
     * @var string|null
     */
    protected $reference;

    /**
     * @var string
     */
    protected $status;

    /**
     * @var string|null
     */
    protected $newItemLocation;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return null|string
     */
    public function getReference()
    {
        return $this->reference;
    }

    /**
     * @param null|string $reference
     */
    public function setReference($reference)
    {
        $this->reference = $reference;
    }

    /**
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param string $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return null|string
     */
    public function getNewItemLocation()
    {
        return $this->newItemLocation;
    }

    /**
     * @param null|string $newItemLocation
     */
    public function setNewItemLocation($newItemLocation)
    {
        $this->newItemLocation = $newItemLocation;
    }
    
}