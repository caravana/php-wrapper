<?php

namespace Caravana\Core\Models\Responses\Base;


use Caravana\Core\Models\Responses\Contracts\Vehicle AS VehicleContract;

abstract class BaseVehicle implements VehicleContract
{

    /**
     * @var int
     */
    protected $id;

    /**
     * @var BaseVehicleModel
     */
    protected $vehicleModel;

    /**
     * @var BaseVehicleType
     */
    protected $vehicleType;

    /**
     * @var BaseVehicleClass
     */
    protected $vehicleClass;

    /**
     * @var BaseTransmission|null
     */
    protected $transmission;

    /**
     * @var BaseFuelType|null
     */
    protected $fuelType;

    /**
     * @var BaseVehicleImage|null
     */
    protected $defaultVehicleImage;

    /**
     * @var BaseVehicleImage[]
     */
    protected $vehicleImages        = [];

    /**
     * @var BaseUser
     */
    protected $createdBy;
    
    /**
     * @var int
     */
    protected $yearCreated;

    /**
     * @var int
     */
    protected $feetLong;

    /**
     * @var string|null
     */
    protected $uniqueReference;

    /**
     * @var int
     */
    protected $statusId;

    /**
     * @var BaseDateTime
     */
    protected $createdAt;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return BaseVehicleModel
     */
    public function getVehicleModel()
    {
        return $this->vehicleModel;
    }

    /**
     * @param BaseVehicleModel $vehicleModel
     */
    public function setVehicleModel($vehicleModel)
    {
        $this->vehicleModel = $vehicleModel;
    }

    /**
     * @return BaseVehicleType
     */
    public function getVehicleType()
    {
        return $this->vehicleType;
    }

    /**
     * @param BaseVehicleType $vehicleType
     */
    public function setVehicleType($vehicleType)
    {
        $this->vehicleType = $vehicleType;
    }

    /**
     * @return BaseVehicleClass
     */
    public function getVehicleClass()
    {
        return $this->vehicleClass;
    }

    /**
     * @param BaseVehicleClass $vehicleClass
     */
    public function setVehicleClass($vehicleClass)
    {
        $this->vehicleClass = $vehicleClass;
    }

    /**
     * @return BaseTransmission|null
     */
    public function getTransmission()
    {
        return $this->transmission;
    }

    /**
     * @param BaseTransmission|null $transmission
     */
    public function setTransmission($transmission)
    {
        $this->transmission = $transmission;
    }

    /**
     * @return BaseFuelType|null
     */
    public function getFuelType()
    {
        return $this->fuelType;
    }

    /**
     * @param BaseFuelType|null     $fuelType
     */
    public function setFuelType($fuelType)
    {
        $this->fuelType = $fuelType;
    }

    /**
     * @return BaseVehicleImage|null
     */
    public function getDefaultVehicleImage()
    {
        return $this->defaultVehicleImage;
    }

    /**
     * @param BaseVehicleImage|null $defaultVehicleImage
     */
    public function setDefaultVehicleImage($defaultVehicleImage)
    {
        $this->defaultVehicleImage = $defaultVehicleImage;
    }

    /**
     * @return BaseVehicleImage[]
     */
    public function getVehicleImages()
    {
        return $this->vehicleImages;
    }

    /**
     * @param BaseVehicleImage  $vehicleImage
     */
    public function addVehicleImage($vehicleImage)
    {
        $this->vehicleImages[]  = $vehicleImage;
    }

    /**
     * @return BaseUser
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * @param BaseUser $createdBy
     */
    public function setCreatedBy($createdBy)
    {
        $this->createdBy = $createdBy;
    }

    /**
     * @return int
     */
    public function getYearCreated()
    {
        return $this->yearCreated;
    }

    /**
     * @param int $yearCreated
     */
    public function setYearCreated($yearCreated)
    {
        $this->yearCreated = $yearCreated;
    }

    /**
     * @return int
     */
    public function getFeetLong()
    {
        return $this->feetLong;
    }

    /**
     * @param int $feetLong
     */
    public function setFeetLong($feetLong)
    {
        $this->feetLong = $feetLong;
    }

    /**
     * @return int
     */
    public function getStatusId()
    {
        return $this->statusId;
    }

    /**
     * @param int $statusId
     */
    public function setStatusId($statusId)
    {
        $this->statusId = $statusId;
    }

    /**
     * @return null|string
     */
    public function getUniqueReference()
    {
        return $this->uniqueReference;
    }

    /**
     * @param null|string $uniqueReference
     */
    public function setUniqueReference($uniqueReference)
    {
        $this->uniqueReference = $uniqueReference;
    }
    
    /**
     * @return BaseDateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param BaseDateTime $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }
    
}