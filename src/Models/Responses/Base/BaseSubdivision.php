<?php

namespace Caravana\Core\Models\Responses\Base;


use Caravana\Core\Models\Responses\Contracts\Subdivision AS SubdivisionContract;

abstract class BaseSubdivision implements SubdivisionContract
{
    
    /**
     * @var int
     */
    protected $id;

    /**
     * @var string
     */
    protected $name;

    /**
     * @var string
     */
    protected $symbol;

    /**
     * @var string
     */
    protected $localSymbol;

    /**
     * @var BaseSubdivisionType
     */
    protected $subdivisionType;

    /**
     * @var BaseCountry
     */
    protected $country;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getSymbol()
    {
        return $this->symbol;
    }

    /**
     * @param string $symbol
     */
    public function setSymbol($symbol)
    {
        $this->symbol = $symbol;
    }

    /**
     * @return string
     */
    public function getLocalSymbol()
    {
        return $this->localSymbol;
    }

    /**
     * @param string $localSymbol
     */
    public function setLocalSymbol($localSymbol)
    {
        $this->localSymbol = $localSymbol;
    }

    /**
     * @return BaseSubdivisionType
     */
    public function getSubdivisionType()
    {
        return $this->subdivisionType;
    }

    /**
     * @param BaseSubdivisionType $subdivisionType
     */
    public function setSubdivisionType($subdivisionType)
    {
        $this->subdivisionType = $subdivisionType;
    }

    /**
     * @return BaseCountry
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param BaseCountry $country
     */
    public function setCountry($country)
    {
        $this->country = $country;
    }
    
}