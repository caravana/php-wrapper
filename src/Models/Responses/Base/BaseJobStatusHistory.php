<?php

namespace Caravana\Core\Models\Responses\Base;


use Caravana\Core\Models\Responses\Contracts\JobStatusHistory AS JobStatusHistoryContract;

abstract class BaseJobStatusHistory implements JobStatusHistoryContract
{

    /**
     * @var int
     */
    protected $id;

    /**
     * @var BaseJobStatus
     */
    protected $jobStatus;

    /**
     * @var BaseUser
     */
    protected $createdBy;

    /**
     * @var BaseDateTime
     */
    protected $createdAt;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return BaseJobStatus
     */
    public function getJobStatus()
    {
        return $this->jobStatus;
    }

    /**
     * @param BaseJobStatus $jobStatus
     */
    public function setJobStatus($jobStatus)
    {
        $this->jobStatus = $jobStatus;
    }

    /**
     * @return BaseUser
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * @param BaseUser $createdBy
     */
    public function setCreatedBy($createdBy)
    {
        $this->createdBy = $createdBy;
    }

    /**
     * @return BaseDateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param BaseDateTime $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }
    
}