<?php

namespace Caravana\Core\Models\Responses\Base;


use Caravana\Core\Models\Responses\Contracts\DateTime AS DateTimeContract;

abstract class BaseDateTime implements DateTimeContract
{

    /**
     * @var string
     */
    protected $date;

    /**
     * @var int
     */
    protected $timeZoneType;

    /**
     * @var string
     */
    protected $timeZone;


    /**
     * @return string
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param string $date
     */
    public function setDate($date)
    {
        $this->date = $date;
    }

    /**
     * @return int
     */
    public function getTimezoneType()
    {
        return $this->timeZoneType;
    }

    /**
     * @param int $timeZoneType
     */
    public function setTimezoneType($timeZoneType)
    {
        $this->timeZoneType = $timeZoneType;
    }

    /**
     * @return string
     */
    public function getTimezone()
    {
        return $this->timeZone;
    }

    /**
     * @param string $timeZone
     */
    public function setTimezone($timeZone)
    {
        $this->timeZone = $timeZone;
    }
    
}