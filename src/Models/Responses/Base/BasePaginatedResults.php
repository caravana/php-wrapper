<?php

namespace Caravana\Core\Models\Responses\Base;


use Caravana\Core\Models\Responses\Contracts\PaginatedResults AS PaginatedResultsContract;

abstract class BasePaginatedResults implements PaginatedResultsContract
{

    /**
     * @var int
     */
    protected $total;

    /**
     * @var int
     */
    protected $perPage;

    /**
     * @var int
     */
    protected $currentPage;

    /**
     * @var int
     */
    protected $lastPage;

    /**
     * @var string|null
     */
    protected $nextPageUrl;

    /**
     * @var string|null
     */
    protected $prevPageUrl;

    /**
     * @var int|null
     */
    protected $from;

    /**
     * @var int|null
     */
    protected $to;

    /**
     * @var array
     */
    protected $data;

    /**
     * @return int
     */
    public function getTotal()
    {
        return $this->total;
    }

    /**
     * @param int $total
     */
    public function setTotal($total)
    {
        $this->total = $total;
    }

    /**
     * @return int
     */
    public function getPerPage()
    {
        return $this->perPage;
    }

    /**
     * @param int $perPage
     */
    public function setPerPage($perPage)
    {
        $this->perPage = $perPage;
    }

    /**
     * @return int
     */
    public function getCurrentPage()
    {
        return $this->currentPage;
    }

    /**
     * @param int $currentPage
     */
    public function setCurrentPage($currentPage)
    {
        $this->currentPage = $currentPage;
    }

    /**
     * @return int
     */
    public function getLastPage()
    {
        return $this->lastPage;
    }

    /**
     * @param int $lastPage
     */
    public function setLastPage($lastPage)
    {
        $this->lastPage = $lastPage;
    }

    /**
     * @return null|string
     */
    public function getNextPageUrl()
    {
        return $this->nextPageUrl;
    }

    /**
     * @param null|string $nextPageUrl
     */
    public function setNextPageUrl($nextPageUrl)
    {
        $this->nextPageUrl = $nextPageUrl;
    }

    /**
     * @return null|string
     */
    public function getPrevPageUrl()
    {
        return $this->prevPageUrl;
    }

    /**
     * @param null|string $prevPageUrl
     */
    public function setPrevPageUrl($prevPageUrl)
    {
        $this->prevPageUrl = $prevPageUrl;
    }

    /**
     * @return int|null
     */
    public function getFrom()
    {
        return $this->from;
    }

    /**
     * @param int|null $from
     */
    public function setFrom($from)
    {
        $this->from = $from;
    }

    /**
     * @return int|null
     */
    public function getTo()
    {
        return $this->to;
    }

    /**
     * @param int|null $to
     */
    public function setTo($to)
    {
        $this->to = $to;
    }

    abstract function getData();

    /**
     * @param array $data
     */
    public function setData($data)
    {
        $this->data = $data;
    }
    
}