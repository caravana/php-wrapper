<?php

namespace Caravana\Core\Models\Responses;


use Caravana\Core\Models\Responses\Base\BaseCountry;
use jamesvweston\Utilities\ArrayUtil AS AU;

class Country extends BaseCountry
{

    /**
     * @param   array|null $data
     */
    public function __construct($data = null)
    {
        if (is_array($data))
        {
            $this->id                       = AU::get($data['id']);
            $this->name                     = AU::get($data['name']);
            $this->iso2                     = AU::get($data['iso2']);
            $this->iso3                     = AU::get($data['iso3']);
        }
    }

    /**
     * @return array
     */
    public function jsonSerialize()
    {
        $object['id']                       = $this->id;
        $object['name']                     = $this->name;
        $object['iso2']                     = $this->iso2;
        $object['iso3']                     = $this->iso3;

        return $object;
    }

}