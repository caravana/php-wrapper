<?php

namespace Caravana\Core\Models\Responses;


use Caravana\Core\Models\Responses\Base\BaseVehicleImage;
use jamesvweston\Utilities\ArrayUtil AS AU;

class VehicleImage extends BaseVehicleImage
{

    /**
     * @param   array|null $data
     */
    public function __construct($data = null)
    {
        if (is_array($data))
        {
            $this->id                       = AU::get($data['id']);
            $this->path                     = AU::get($data['path']);
        }
    }

    /**
     * @return array
     */
    public function jsonSerialize()
    {
        $object['id']                       = $this->id;
        $object['path']                     = $this->path;

        return $object;
    }

}