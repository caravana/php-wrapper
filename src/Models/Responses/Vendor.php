<?php

namespace Caravana\Core\Models\Responses;


use Caravana\Core\Models\Responses\Base\BaseVendor;
use jamesvweston\Utilities\ArrayUtil AS AU;

class Vendor extends BaseVendor
{

    /**
     * @param   array|null $data
     */
    public function __construct($data = null)
    {
        if (is_array($data))
        {
            $this->id                       = AU::get($data['id']);
            $this->name                     = AU::get($data['name']);

            if (!is_null(AU::get($data['owner'])))
                $this->owner                = new User(AU::get($data['owner']));
            
            if (!is_null(AU::get($data['createdAt'])))
                $this->createdAt            = new DateTime(AU::get($data['createdAt']));
        }
    }

    /**
     * @return array
     */
    public function jsonSerialize()
    {
        $object['id']                       = $this->id;
        $object['name']                     = $this->name;
        $object['owner']                    = ($this->owner instanceof \JsonSerializable) ? $this->owner->jsonSerialize() : null;
        $object['createdAt']                = ($this->createdAt instanceof \JsonSerializable) ? $this->createdAt->jsonSerialize() : null;

        return $object;
    }

}