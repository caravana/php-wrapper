<?php

namespace Caravana\Core\Models\Responses;


use Caravana\Core\Models\Responses\Base\BaseUserInvite;
use jamesvweston\Utilities\ArrayUtil AS AU;

class UserInvite extends BaseUserInvite
{
    
    /**
     * @param   array|null $data
     */
    public function __construct($data = null)
    {
        if (is_array($data))
        {
            $this->id                       = AU::get($data['id']);
            $this->firstName                = AU::get($data['firstName']);
            $this->lastName                 = AU::get($data['lastName']);
            $this->email                    = AU::get($data['email']);
            
            if (!is_null(AU::get($data['status'])))
                $this->status               = new UserInviteStatus(AU::get($data['status']));
            
            if (!is_null(AU::get($data['createdBy'])))
                $this->createdBy            = new User(AU::get($data['createdBy']));
        }
    }

    /**
     * @return array
     */
    public function jsonSerialize()
    {
        $object['id']                       = $this->id;
        $object['firstName']                = $this->firstName;
        $object['lastName']                 = $this->lastName;
        $object['email']                    = $this->email;
        $object['status']                   = ($this->status instanceof \JsonSerializable) ? $this->status->jsonSerialize() : null;
        $object['createdBy']                = ($this->createdBy instanceof \JsonSerializable) ? $this->createdBy->jsonSerialize() : null;
        
        return $object;
    }
}