<?php

namespace Caravana\Core\Models\Responses\Collections;


use Caravana\Core\Models\Responses\Base\BasePaginatedResults;
use Caravana\Core\Models\Responses\FuelType;
use jamesvweston\Utilities\ArrayUtil AS AU;

class FuelTypeCollection extends BasePaginatedResults
{

    /**
     * @param   array|null $data
     */
    public function __construct($data = null)
    {
        if (is_array($data))
        {
            $this->total                    = AU::get($data['total']);
            $this->perPage                  = AU::get($data['per_page'], AU::get($data['perPage']));
            $this->currentPage              = AU::get($data['current_page'], AU::get($data['currentPage']));
            $this->lastPage                 = AU::get($data['last_page'], AU::get($data['lastPage']));
            $this->nextPageUrl              = AU::get($data['next_page_url'], AU::get($data['nextPageUrl']));
            $this->prevPageUrl              = AU::get($data['prev_page_url'], AU::get($data['prevPageUrl']));
            $this->from                     = AU::get($data['from']);
            $this->to                       = AU::get($data['to']);

            $this->data                     = [];

            if (is_array(AU::get($data['data'])))
            {
                foreach ($data['data'] AS $item)
                {
                    $this->data[]           = new FuelType($item);
                }
            }
        }
    }

    /**
     * @return FuelType[]
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @return array
     */
    public function jsonSerialize()
    {
        $object['total']                    = $this->total;
        $object['perPage']                  = $this->perPage;
        $object['currentPage']              = $this->currentPage;
        $object['lastPage']                 = $this->lastPage;
        $object['nextPageUrl']              = $this->nextPageUrl;
        $object['prevPageUrl']              = $this->prevPageUrl;
        $object['from']                     = $this->from;
        $object['to']                       = $this->to;

        $data                               = $this->getData();
        foreach ($data AS $item)
        {
            $object['data'][]               = $item->jsonSerialize();
        }


        return $object;
    }
}