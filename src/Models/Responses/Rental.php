<?php

namespace Caravana\Core\Models\Responses;


use Caravana\Core\Models\Responses\Base\BaseRental;
use jamesvweston\Utilities\ArrayUtil AS AU;

class Rental extends BaseRental
{

    /**
     * @param   array|null $data
     */
    public function __construct($data = null)
    {
        if (is_array($data))
        {
            $this->id                       = AU::get($data['id']);
            $this->name                     = AU::get($data['name']);
            $this->description              = AU::get($data['description']);
            $this->securityDeposit          = AU::get($data['securityDeposit']);
            $this->taxPercent               = AU::get($data['taxPercent']);
            $this->minimumDays              = AU::get($data['minimumDays']);

            if (!is_null(AU::get($data['rentalStatus'])))
                $this->rentalStatus         = new RentalStatus(AU::get($data['rentalStatus']));

            if (!is_null(AU::get($data['vehicle'])))
                $this->vehicle              = new Vehicle(AU::get($data['vehicle']));
            
            $this->setUnavailableDates(AU::get($data['unavailableDates']));
        }
    }

    /**
     * @return array
     */
    public function jsonSerialize()
    {
        $object['id']                       = $this->id;
        $object['name']                     = $this->name;
        $object['description']              = $this->description;
        $object['securityDeposit']          = $this->securityDeposit;
        $object['taxPercent']               = $this->taxPercent;
        $object['minimumDays']              = $this->minimumDays;
        $object['rentalStatus']             = ($this->rentalStatus instanceof \JsonSerializable) ? $this->rentalStatus->jsonSerialize() : null;
        $object['vehicle']                  = ($this->vehicle instanceof \JsonSerializable) ? $this->vehicle->jsonSerialize() : null;
        $object['unavailableDates']         = $this->unavailableDates;
        
        return $object;
    }
    
}