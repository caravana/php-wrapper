<?php

namespace Caravana\Core\Models\Responses;


use Caravana\Core\Models\Responses\Base\BaseVehicleModel;
use jamesvweston\Utilities\ArrayUtil AS AU;

class VehicleModel extends BaseVehicleModel
{

    /**
     * @param   array|null $data
     */
    public function __construct($data = null)
    {
        if (is_array($data))
        {
            $this->id                       = AU::get($data['id']);
            $this->name                     = AU::get($data['name']);
            
            if (!is_null(AU::get($data['vehicleMake'])))
                $this->vehicleMake          = new VehicleMake(AU::get($data['vehicleMake']));

            if (!is_null(AU::get($data['vehicleType'])))
                $this->vehicleType          = new VehicleType(AU::get($data['vehicleType']));
        }
    }

    /**
     * @return array
     */
    public function jsonSerialize()
    {
        $object['id']                       = $this->id;
        $object['name']                     = $this->name;
        $object['vehicleMake']              = ($this->vehicleMake instanceof \JsonSerializable) ? $this->vehicleMake->jsonSerialize() : null;
        $object['vehicleType']              = ($this->vehicleType instanceof \JsonSerializable) ? $this->vehicleType->jsonSerialize() : null;
        
        return $object;
    }
    
}