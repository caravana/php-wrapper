<?php

namespace Caravana\Core\Models\Responses;


use Caravana\Core\Models\Responses\Base\BaseJobStatusHistory;
use jamesvweston\Utilities\ArrayUtil AS AU;

class JobStatusHistory extends BaseJobStatusHistory
{

    /**
     * @param   array|null $data
     */
    public function __construct($data = null)
    {
        if (is_array($data))
        {
            $this->id                       = AU::get($data['id']);
            $this->jobStatus                = new JobStatus(AU::get($data['jobStatus']));
            
            if (!is_null(AU::get($data['createdBy'])))
                $this->createdBy                = new User(AU::get($data['createdBy']));
            
            $this->createdAt                = new DateTime(AU::get($data['createdAt']));
        }
    }

    /**
     * @return array
     */
    public function jsonSerialize()
    {
        $object['id']                       = $this->id;
        $object['jobStatus']                = $this->jobStatus->jsonSerialize();
        $object['createdBy']                = ($this->createdBy instanceof \JsonSerializable) ? $this->createdBy->jsonSerialize() : null;
        $object['createdAt']                = $this->createdAt->jsonSerialize();
        return $object;
    }
    
}