<?php

namespace Caravana\Core\Models\Responses;


use Caravana\Core\Models\Responses\Base\BaseJobItem;
use jamesvweston\Utilities\ArrayUtil AS AU;

class JobItem extends BaseJobItem
{

    /**
     * @param   array|null $data
     */
    public function __construct($data = null)
    {
        if (is_array($data))
        {
            $this->id                       = AU::get($data['id']);
            $this->reference                = AU::get($data['reference']);
            $this->status                   = AU::get($data['status']);
            $this->newItemLocation          = AU::get($data['newItemLocation']);
        }
    }

    /**
     * @return array
     */
    public function jsonSerialize()
    {
        $object['id']                       = $this->id;
        $object['reference']                = $this->reference;
        $object['status']                   = $this->status;
        $object['newItemLocation']          = $this->newItemLocation;
        return $object;
    }
    
}