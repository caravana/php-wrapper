<?php

namespace Caravana\Core\Models\Responses;


use Caravana\Core\Models\Responses\Base\BaseVehicle;
use jamesvweston\Utilities\ArrayUtil AS AU;

class Vehicle extends BaseVehicle
{

    /**
     * @param   array|null $data
     */
    public function __construct($data = null)
    {
        if (is_array($data))
        {
            $this->id                       = AU::get($data['id']);
            $this->vehicleModel             = new VehicleModel(AU::get($data['vehicleModel']));
            $this->vehicleType              = new VehicleType(AU::get($data['vehicleType']));
            $this->vehicleClass             = new VehicleClass(AU::get($data['vehicleClass']));
            $this->transmission             = !is_null(AU::get($data['transmission'])) ? new Transmission(AU::get($data['transmission'])) : null;
            $this->fuelType                 = !is_null(AU::get($data['fuelType'])) ? new FuelType(AU::get($data['fuelType'])) : null;
            $this->defaultVehicleImage      = !is_null(AU::get($data['defaultVehicleImage'])) ? new VehicleImage(AU::get($data['defaultVehicleImage'])) : null;

            $this->vehicleImages            = [];
            if (!is_null(AU::isArrays($data['vehicleImages'])))
            {
                foreach ($data['vehicleImages'] AS $vehicleImage)
                {
                    $this->vehicleImages[]  = new VehicleImage($vehicleImage);
                }
            }

            $this->createdBy                = new User(AU::get($data['createdBy']));
            $this->yearCreated              = AU::get($data['yearCreated']);
            $this->feetLong                 = AU::get($data['feetLong']);
            $this->uniqueReference          = AU::get($data['uniqueReference']);
            $this->statusId                 = AU::get($data['statusId']);
            $this->createdAt                = AU::get($data['createdAt']);
        }
    }

    /**
     * @return array
     */
    public function jsonSerialize()
    {
        $object['id']                       = $this->id;
        $object['vehicleModel']             = ($this->vehicleModel instanceof \JsonSerializable) ? $this->vehicleModel->jsonSerialize() : null;
        $object['vehicleType']              = ($this->vehicleType instanceof \JsonSerializable) ? $this->vehicleType->jsonSerialize() : null;
        $object['vehicleClass']             = ($this->vehicleClass instanceof \JsonSerializable) ? $this->vehicleClass->jsonSerialize() : null;
        $object['transmission']             = ($this->transmission instanceof \JsonSerializable) ? $this->transmission->jsonSerialize() : null;
        $object['fuelType']                 = ($this->fuelType instanceof \JsonSerializable) ? $this->fuelType->jsonSerialize() : null;
        $object['defaultVehicleImage']      = ($this->defaultVehicleImage instanceof \JsonSerializable) ? $this->defaultVehicleImage->jsonSerialize() : null;

        $object['vehicleImages']            = [];
        foreach ($this->vehicleImages AS $vehicleImage)
        {
            $object['vehicleImages'][]      = $vehicleImage->jsonSerialze();
        }

        $object['createdBy']                = ($this->createdBy instanceof \JsonSerializable) ? $this->createdBy->jsonSerialize() : null;
        $object['yearCreated']              = $this->yearCreated;
        $object['feetLong']                 = $this->feetLong;
        $object['uniqueReference']          = $this->uniqueReference;
        $object['statusId']                 = $this->statusId;
        $object['createdAt']                = ($this->vehicleModel instanceof \JsonSerializable) ? $this->vehicleModel->jsonSerialize() : null;

        return $object;
    }
    
}