<?php

namespace Caravana\Core\Models\Responses;


use Caravana\Core\Models\Responses\Base\BaseSubdivision;
use jamesvweston\Utilities\ArrayUtil AS AU;

class Subdivision extends BaseSubdivision
{
    
    /**
     * @param   array|null $data
     */
    public function __construct($data = null)
    {
        if (is_array($data))
        {
            $this->id                       = AU::get($data['id']);
            $this->name                     = AU::get($data['name']);
            $this->symbol                   = AU::get($data['symbol']);
            $this->localSymbol              = AU::get($data['localSymbol']);
            
            if (!is_null(AU::get($data['subdivisionType'])))
                $this->subdivisionType      = new SubdivisionType(AU::get($data['subdivisionType']));
            
            if (!is_null(AU::get($data['country'])))
                $this->country              = new Country(AU::get($data['country']));
        }
    }

    /**
     * @return array
     */
    public function jsonSerialize()
    {
        $object['id']                       = $this->id;
        $object['name']                     = $this->name;
        $object['symbol']                   = $this->symbol;
        $object['localSymbol']              = $this->localSymbol;
        $object['subdivisionType']          = ($this->subdivisionType instanceof \JsonSerializable) ? $this->subdivisionType->jsonSerialize() : null;
        $object['country']                  = ($this->country instanceof \JsonSerializable) ? $this->country->jsonSerialize() : null;

        return $object;
    }

}