<?php

namespace Caravana\Core\Models\Responses;


use Caravana\Core\Models\Responses\Base\BaseJob;
use jamesvweston\Utilities\ArrayUtil AS AU;

class Job extends BaseJob
{

    /**
     * @param   array|null $data
     */
    public function __construct($data = null)
    {
        if (is_array($data))
        {
            $this->id                       = AU::get($data['id']);

            if (!is_null(AU::get($data['vendor'])))
                $this->vendor               = new Vendor(AU::get($data['vendor']));

            $this->jobType                  = new JobType(AU::get($data['jobType']));
            $this->jobStatus                = new JobStatus(AU::get($data['jobStatus']));

            $this->jobStatusHistory         = [];
            $jobStatusHistory               = AU::get($data['jobStatusHistory']);
            if (AU::isArrays($jobStatusHistory))
            {
                foreach ($jobStatusHistory AS $item)
                {
                    $this->jobStatusHistory[]= new JobStatusHistory($item);
                }
            }

            $this->jobItems                 = [];
            $jobItems                       = AU::get($data['jobItems']);
            if (AU::isArrays($jobItems))
            {
                foreach ($jobItems AS $item)
                {
                    $this->jobItems[]       = new JobItem($item);
                }
            }

            $this->totalItems               = AU::get($data['totalItems']);
            $this->totalSuccessful          = AU::get($data['totalSuccessful']);
            $this->totalErrors              = AU::get($data['totalErrors']);
            $this->originalFileName         = AU::get($data['originalFileName']);
            $this->convertedFileName        = AU::get($data['convertedFileName']);
            $this->fileKey                  = AU::get($data['fileKey']);
            $this->filePath                 = AU::get($data['filePath']);

            if (!is_null(AU::get($data['createdBy'])))
                $this->createdBy            = new User(AU::get($data['createdBy']));

            if (!is_null(AU::get($data['createdAt'])))
                $this->createdAt            = new DateTime(AU::get($data['createdAt']));

            if (!is_null(AU::get($data['startedAt'])))
                $this->startedAt            = new DateTime(AU::get($data['startedAt']));

            if (!is_null(AU::get($data['finishedAt'])))
                $this->finishedAt           = new DateTime(AU::get($data['finishedAt']));

        }
    }

    /**
     * @return array
     */
    public function jsonSerialize()
    {
        $object['id']                       = $this->id;
        $object['vendor']                   = ($this->vendor instanceof \JsonSerializable) ? $this->vendor->jsonSerialize() : null;
        $object['jobType']                  = $this->jobType->jsonSerialize();
        $object['jobStatus']                = $this->jobStatus->jsonSerialize();

        $object['jobStatusHistory']         = [];
        foreach ($this->jobStatusHistory AS $item)
        {
            $object['jobStatusHistory'][]   = $item->jsonSerialize();
        }

        $object['jobItems']                 = [];
        foreach ($this->jobItems AS $item)
        {
            $object['jobItems'][]           = $item->jsonSerialize();
        }

        $object['totalItems']               = $this->totalItems;
        $object['totalSuccessful']          = $this->totalSuccessful;
        $object['totalErrors']              = $this->totalErrors;
        $object['originalFileName']         = $this->originalFileName;
        $object['convertedFileName']        = $this->convertedFileName;
        $object['fileKey']                  = $this->fileKey;
        $object['filePath']                 = $this->filePath;
        $object['createdBy']                = ($this->createdBy instanceof \JsonSerializable) ? $this->createdBy->jsonSerialize() : null;
        $object['createdAt']                = ($this->createdAt instanceof \JsonSerializable) ? $this->createdAt->jsonSerialize() : null;
        $object['startedAt']                = ($this->startedAt instanceof \JsonSerializable) ? $this->startedAt->jsonSerialize() : null;
        $object['finishedAt']               = ($this->finishedAt instanceof \JsonSerializable) ? $this->finishedAt->jsonSerialize() : null;

        return $object;
    }

}