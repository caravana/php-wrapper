<?php

namespace Caravana\Core\Models\Responses;


use Caravana\Core\Models\Responses\Base\BaseDateTime;
use jamesvweston\Utilities\ArrayUtil AS AU;

class DateTime extends BaseDateTime
{

    /**
     * @param   array|null      $data
     */
    public function __construct($data = null)
    {
        if (is_array($data))
        {
            $this->date                     = AU::get($data['date']);
            $this->timeZoneType             = AU::get($data['timezone_type'], AU::get($data['timeZoneType']));
            $this->timeZone                 = AU::get($data['timezone'], AU::get($data['timeZone']));
        }
    }

    /**
     * @return array
     */
    public function jsonSerialize()
    {
        $object['date']                     = $this->date;
        $object['timeZoneType']             = $this->timeZoneType;
        $object['timeZone']                 = $this->timeZone;
        
        return $object;
    }

}