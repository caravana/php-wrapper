<?php

namespace Caravana\Core\Models\Responses;


use Caravana\Core\Models\Responses\Base\BaseAddress;
use jamesvweston\Utilities\ArrayUtil AS AU;

class Address extends BaseAddress
{

    /**
     * @param   array|null $data
     */
    public function __construct($data = null)
    {
        if (is_array($data))
        {
            $this->id                       = AU::get($data['id']);
            $this->name                     = AU::get($data['name']);
            $this->companyName              = AU::get($data['companyName']);
            $this->phone                    = AU::get($data['phone']);
            $this->email                    = AU::get($data['email']);
            $this->street1                  = AU::get($data['street1']);
            $this->street2                  = AU::get($data['street2']);
            $this->city                     = AU::get($data['city']);
            $this->postalCode               = AU::get($data['postalCode']);
            
            if (!is_null(AU::get($data['subdivision'])))
                $this->subdivision          = new Subdivision(AU::get($data['subdivision']));
        }
    }

    /**
     * @return array
     */
    public function jsonSerialize()
    {
        $object['id']                       = $this->id;
        $object['name']                     = $this->name;
        $object['companyName']              = $this->companyName;
        $object['phone']                    = $this->phone;
        $object['email']                    = $this->email;
        $object['street1']                  = $this->street1;
        $object['street2']                  = $this->street2;
        $object['city']                     = $this->city;
        $object['postalCode']               = $this->postalCode;
        $object['subdivision']              = ($this->subdivision instanceof \JsonSerializable) ? $this->subdivision->jsonSerialize() : null;

        return $object;
    }

}