<?php

namespace Caravana\Core\Models\Responses;


use Caravana\Core\Models\Responses\Base\BaseVehicleMake;
use jamesvweston\Utilities\ArrayUtil AS AU;

class VehicleMake extends BaseVehicleMake
{

    /**
     * @param   array|null $data
     */
    public function __construct($data = null)
    {
        if (is_array($data))
        {
            $this->id                       = AU::get($data['id']);
            $this->name                     = AU::get($data['name']);
        }
    }

    /**
     * @return array
     */
    public function jsonSerialize()
    {
        $object['id']                       = $this->id;
        $object['name']                     = $this->name;

        return $object;
    }
    
}