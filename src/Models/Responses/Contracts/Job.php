<?php

namespace Caravana\Core\Models\Responses\Contracts;


interface Job extends \JsonSerializable
{
    function getId();
    function setId($id);
    function getVendor();
    function setVendor($vendor);
    function getJobType();
    function setJobType($jobType);
    function getJobStatus();
    function setJobStatus($jobStatus);
    function getJobStatusHistory();
    function addJobStatusHistory($jobStatusHistory);
    function getJobItems();
    function addJobItem($jobItem);
    function getTotalItems();
    function setTotalItems($totalItems);
    function getTotalSuccessful();
    function setTotalSuccessful($totalSuccessful);
    function getTotalErrors();
    function setTotalErrors($totalErrors);
    function getOriginalFileName();
    function setOriginalFileName($originalFileName);
    function getConvertedFileName();
    function setConvertedFileName($convertedFileName);
    function getFileKey();
    function setFileKey($fileKey);
    function getFilePath();
    function setFilePath($filePath);
    function getCreatedBy();
    function setCreatedBy($createdBy);
    function getCreatedAt();
    function setCreatedAt($createdAt);
    function getStartedAt();
    function setStartedAt($startedAt);
    function getFinishedAt();
    function setFinishedAt($finishedAt);
}