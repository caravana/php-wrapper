<?php

namespace Caravana\Core\Models\Responses\Contracts;


interface Subdivision extends \JsonSerializable
{
    function getId();
    function setId($id);
    function getName();
    function setName($name);
    function getSymbol();
    function setSymbol($symbol);
    function getLocalSymbol();
    function setLocalSymbol($localSymbol);
    function getSubdivisionType();
    function setSubdivisionType($subdivisionType);
    function getCountry();
    function setCountry($country);
}