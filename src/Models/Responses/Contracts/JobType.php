<?php

namespace Caravana\Core\Models\Responses\Contracts;


interface JobType extends \JsonSerializable
{
    function getId();
    function setId($id);
    function getName();
    function setName($name);
}