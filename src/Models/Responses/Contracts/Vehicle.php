<?php

namespace Caravana\Core\Models\Responses\Contracts;


interface Vehicle extends \JsonSerializable
{
    function getId();
    function setId($id);
    function getVehicleModel();
    function setVehicleModel($vehicleModel);
    function getVehicleType();
    function setVehicleType($vehicleType);
    function getVehicleClass();
    function setVehicleClass($vehicleClass);
    function getTransmission();
    function setTransmission($transmission);
    function getFuelType();
    function setFuelType($fuelType);
    function getDefaultVehicleImage();
    function setDefaultVehicleImage($defaultVehicleImage);
    function getVehicleImages();
    function addVehicleImage($vehicleImage);
    function getCreatedBy();
    function setCreatedBy($createdBy);
    function getYearCreated();
    function setYearCreated($yearCreated);
    function getFeetLong();
    function setFeetLong($feetLong);
    function getUniqueReference();
    function setUniqueReference($uniqueReference);
    function getStatusId();
    function setStatusId($statusId);
    function getCreatedAt();
    function setCreatedAt($createdAt);
}