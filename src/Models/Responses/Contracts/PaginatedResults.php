<?php

namespace Caravana\Core\Models\Responses\Contracts;


interface PaginatedResults extends \JsonSerializable
{
    function getTotal();
    function setTotal($total);
    function getPerPage();
    function setPerPage($perPage);
    function getCurrentPage();
    function setCurrentPage($currentPage);
    function getLastPage();
    function setLastPage($lastPage);
    function getNextPageUrl();
    function setNextPageUrl($nextPageUrl);
    function getPrevPageUrl();
    function setPrevPageUrl($prevPageUrl);
    function getFrom();
    function setFrom($from);
    function getTo();
    function setTo($to);
    function getData();
    function setData($data);
}