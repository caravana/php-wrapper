<?php

namespace Caravana\Core\Models\Responses\Contracts;


interface Rental extends \JsonSerializable
{
    function getId();
    function setId($id);
    function getName();
    function setName($name);
    function getDescription();
    function setDescription($description);
    function getSecurityDeposit();
    function setSecurityDeposit($securityDeposit);
    function getTaxPercent();
    function setTaxPercent($taxPercent);
    function getMinimumDays();
    function setMinimumDays($minimumDays);
    function getRentalStatus();
    function setRentalStatus($rentalStatus);
    function getVehicle();
    function setVehicle($vehicle);
    function getUnavailableDates();
    function setUnavailableDates($unavailableDates);
}