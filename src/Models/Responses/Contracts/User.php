<?php

namespace Caravana\Core\Models\Responses\Contracts;


interface User extends \JsonSerializable
{
    function getId();
    function setId($id);
    function getFirstName();
    function setFirstName($firstName);
    function getLastName();
    function setLastName($lastName);
    function getEmail();
    function setEmail($email);
    function getCreatedAt();
    function setCreatedAt($createdAt);
}