<?php

namespace Caravana\Core\Models\Responses\Contracts;


interface Address extends \JsonSerializable
{
    function getId();
    function setId($id);
    function getName();
    function setName($name);
    function getCompanyName();
    function setCompanyName($companyName);
    function getPhone();
    function setPhone($phone);
    function getEmail();
    function setEmail($email);
    function getStreet1();
    function setStreet1($street1);
    function getStreet2();
    function setStreet2($street2);
    function getCity();
    function setCity($city);
    function getPostalCode();
    function setPostalCode($postalCode);
    function getSubdivision();
    function setSubdivision($subdivision);
}