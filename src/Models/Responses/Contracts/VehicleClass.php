<?php

namespace Caravana\Core\Models\Responses\Contracts;


interface VehicleClass extends \JsonSerializable
{
    function getId();
    function setId($id);
    function getName();
    function setName($name);
}