<?php

namespace Caravana\Core\Models\Responses\Contracts;


interface UserInvite extends \JsonSerializable
{
    function getId();
    function setId($id);
    function getFirstName();
    function setFirstName($firstName);
    function getLastName();
    function setLastName($lastName);
    function getEmail();
    function setEmail($email);
    function getStatus();
    function setStatus($status);
    function getCreatedBy();
    function setCreatedBy($createdBy);
}