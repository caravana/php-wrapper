<?php

namespace Caravana\Core\Models\Responses\Contracts;


interface VehicleModel extends \JsonSerializable
{
    function getId();
    function setId($id);
    function getName();
    function setName($name);
    function getVehicleMake();
    function setVehicleMake($vehicleMake);
    function getVehicleType();
    function setVehicleType($vehicleType);
}