<?php

namespace Caravana\Core\Models\Responses\Contracts;


interface VehicleImage extends \JsonSerializable
{
    function getId();
    function setId($id);
    function getPath();
    function setPath($path);
}