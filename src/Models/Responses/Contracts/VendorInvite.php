<?php

namespace Caravana\Core\Models\Responses\Contracts;


interface VendorInvite extends \JsonSerializable
{
    function getId();
    function setId($id);
    function getVendor();
    function setVendor($vendor);
    function getUser();
    function setUser($user);
    function getStatus();
    function setStatus($status);
    function getCreatedBy();
    function setCreatedBy($createdBy);
    function getCreatedAt();
    function setCreatedAt($createdAt);
}