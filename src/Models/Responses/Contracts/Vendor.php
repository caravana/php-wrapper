<?php

namespace Caravana\Core\Models\Responses\Contracts;


interface Vendor extends \JsonSerializable
{
    function getId();
    function setId($id);
    function getName();
    function setName($name);
    function getOwner();
    function setOwner($owner);
    function getCreatedAt();
    function setCreatedAt($createdAt);
}