<?php

namespace Caravana\Core\Models\Responses\Contracts;


interface Country extends \JsonSerializable
{
    function getId();
    function setId($id);
    function getName();
    function setName($name);
    function getIso2();
    function setIso2($iso2);
    function getIso3();
    function setIso3($iso3);
}