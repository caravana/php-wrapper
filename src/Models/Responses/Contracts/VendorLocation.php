<?php

namespace Caravana\Core\Models\Responses\Contracts;


interface VendorLocation extends \JsonSerializable
{
    function getId();
    function setId($id);
    function getStreet1();
    function setStreet1($street1);
    function getStreet2();
    function setStreet2($street2);
    function getCity();
    function setCity($city);
    function getPostalCode();
    function setPostalCode($postalCode);
    function getSubdivision();
    function setSubdivision($subdivision);
    function getVendor();
    function setVendor($vendor);
    function getCreatedAt();
    function setCreatedAt($createdAt);
}