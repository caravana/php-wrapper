<?php

namespace Caravana\Core\Models\Responses\Contracts;


interface JobItem extends \JsonSerializable
{
    function getId();
    function setId($id);
    function getReference();
    function setReference($reference);
    function getStatus();
    function setStatus($status);
    function getNewItemLocation();
    function setNewItemLocation($newItemLocation);
}