<?php

namespace Caravana\Core\Models\Responses\Contracts;


interface DateTime extends \JsonSerializable
{
    function getDate();
    function setDate($date);
    function getTimezoneType();
    function setTimezoneType($timeZoneType);
    function getTimezone();
    function setTimezone($timeZone);
}