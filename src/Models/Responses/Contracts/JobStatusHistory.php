<?php

namespace Caravana\Core\Models\Responses\Contracts;


interface JobStatusHistory extends \JsonSerializable
{
    function getId();
    function setId($id);
    function getJobStatus();
    function setJobStatus($jobStatus);
    function getCreatedBy();
    function setCreatedBy($createdBy);
    function getCreatedAt();
    function setCreatedAt($createdAt);
}