<?php

namespace Caravana\Core\Models\Responses\Contracts;


interface Card extends \JsonSerializable
{
    function getId();
    function setId($id);
    function getName();
    function setName($name);
    function getPostalCode();
    function setPostalCode($postalCode);
    function getExpMonth();
    function setExpMonth($expMonth);
    function getExpYear();
    function setExpYear($expYear);
    function getLastFour();
    function setLastFour($lastFour);
}