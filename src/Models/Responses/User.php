<?php

namespace Caravana\Core\Models\Responses;


use Caravana\Core\Models\Responses\Base\BaseUser;
use jamesvweston\Utilities\ArrayUtil AS AU;

class User extends BaseUser
{

    /**
     * User constructor.
     * @param   array|null      $data
     */
    public function __construct($data = null)
    {
        if (is_array($data))
        {
            $this->id                       = AU::get($data['id']);
            $this->firstName                = AU::get($data['firstName']);
            $this->lastName                 = AU::get($data['lastName']);
            $this->email                    = AU::get($data['email']);
            $this->createdAt                = new DateTime(AU::get($data['createdAt']));
        }
    }

    /**
     * @return array
     */
    public function jsonSerialize()
    {
        $object['id']                       = $this->id;
        $object['firstName']                = $this->firstName;
        $object['lastName']                 = $this->lastName;
        $object['email']                    = $this->email;
        $object['createdAt']                = ($this->createdAt instanceof \JsonSerializable) ? $this->createdAt->jsonSerialize() : null;
        
        return $object;
    }
}