<?php

namespace Caravana\Core\Models\Support\Base;


use Caravana\Core\Models\Support\Contracts\VehicleCSVFormat AS VehicleCSVFormatContract;

abstract class BaseVehicleCSVFormat implements VehicleCSVFormatContract
{

    /**
     * @var string
     */
    protected $vehicleTypeName;

    /**
     * @var string
     */
    protected $vehicleMakeName;

    /**
     * @var string
     */
    protected $vehicleModelName;

    /**
     * @var string
     */
    protected $vehicleClassName;

    /**
     * @var string
     */
    protected $fuelTypeName;

    /**
     * @var string|null
     */
    protected $transmissionName;

    /**
     * @var int|null
     */
    protected $accommodates;

    /**
     * @var int
     */
    protected $yearCreated;

    /**
     * @var int
     */
    protected $feetLong;

    /**
     * @var string|null
     */
    protected $vin;

    /**
     * @var string|null
     */
    protected $licensePlate;

    /**
     * @var string|null
     */
    protected $stateRegistered;

    /**
     * @var string|null
     */
    protected $vendorLocationReference1;

    /**
     * @var string|null
     */
    protected $uniqueReference;

    /**
     * @var string|null
     */
    protected $image01URL;

    /**
     * @var string|null
     */
    protected $image02URL;

    /**
     * @var string|null
     */
    protected $image03URL;

    /**
     * @var string|null
     */
    protected $image04URL;

    /**
     * @var string|null
     */
    protected $image05URL;

    /**
     * @var string|null
     */
    protected $image06URL;

    /**
     * @var string|null
     */
    protected $image07URL;

    /**
     * @var string|null
     */
    protected $image08URL;

    /**
     * @var string|null
     */
    protected $image09URL;

    /**
     * @var string|null
     */
    protected $image10URL;

    /**
     * @var string|null
     */
    protected $image11URL;

    /**
     * @var string|null
     */
    protected $image12URL;

    /**
     * @var string|null
     */
    protected $image13URL;

    /**
     * @var string|null
     */
    protected $image14URL;

    /**
     * @var string|null
     */
    protected $image15URL;

    /**
     * @var string|null
     */
    protected $image16URL;

    /**
     * @var string|null
     */
    protected $image17URL;

    /**
     * @var string|null
     */
    protected $image18URL;

    /**
     * @var string|null
     */
    protected $image19URL;

    /**
     * @var string|null
     */
    protected $image20URL;

    /**
     * @var string|null
     */
    protected $image21URL;

    /**
     * @var string|null
     */
    protected $image22URL;

    /**
     * @var string|null
     */
    protected $image23URL;

    /**
     * @var string|null
     */
    protected $image24URL;

    /**
     * @var string|null
     */
    protected $image25URL;

    /**
     * @var string|null
     */
    protected $image26URL;

    /**
     * @var string|null
     */
    protected $image27URL;

    /**
     * @var string|null
     */
    protected $image28URL;

    /**
     * @var string|null
     */
    protected $image29URL;

    /**
     * @var string|null
     */
    protected $image30URL;

    /**
     * @return string
     */
    public function getVehicleTypeName()
    {
        return $this->vehicleTypeName;
    }

    /**
     * @param string $vehicleTypeName
     */
    public function setVehicleTypeName($vehicleTypeName)
    {
        $this->vehicleTypeName = $vehicleTypeName;
    }

    /**
     * @return string
     */
    public function getVehicleMakeName()
    {
        return $this->vehicleMakeName;
    }

    /**
     * @param string $vehicleMakeName
     */
    public function setVehicleMakeName($vehicleMakeName)
    {
        $this->vehicleMakeName = $vehicleMakeName;
    }

    /**
     * @return string
     */
    public function getVehicleModelName()
    {
        return $this->vehicleModelName;
    }

    /**
     * @param string $vehicleModelName
     */
    public function setVehicleModelName($vehicleModelName)
    {
        $this->vehicleModelName = $vehicleModelName;
    }

    /**
     * @return string
     */
    public function getVehicleClassName()
    {
        return $this->vehicleClassName;
    }

    /**
     * @param string $vehicleClassName
     */
    public function setVehicleClassName($vehicleClassName)
    {
        $this->vehicleClassName = $vehicleClassName;
    }

    /**
     * @return string
     */
    public function getFuelTypeName()
    {
        return $this->fuelTypeName;
    }

    /**
     * @param string $fuelTypeName
     */
    public function setFuelTypeName($fuelTypeName)
    {
        $this->fuelTypeName = $fuelTypeName;
    }

    /**
     * @return null|string
     */
    public function getTransmissionName()
    {
        return $this->transmissionName;
    }

    /**
     * @param null|string $transmissionName
     */
    public function setTransmissionName($transmissionName)
    {
        $this->transmissionName = $transmissionName;
    }

    /**
     * @return int|null
     */
    public function getAccommodates()
    {
        return $this->accommodates;
    }

    /**
     * @param int|null $accommodates
     */
    public function setAccommodates($accommodates)
    {
        $this->accommodates = $accommodates;
    }

    /**
     * @return int
     */
    public function getYearCreated()
    {
        return $this->yearCreated;
    }

    /**
     * @param int $yearCreated
     */
    public function setYearCreated($yearCreated)
    {
        $this->yearCreated = $yearCreated;
    }

    /**
     * @return int
     */
    public function getFeetLong()
    {
        return $this->feetLong;
    }

    /**
     * @param int $feetLong
     */
    public function setFeetLong($feetLong)
    {
        $this->feetLong = $feetLong;
    }

    /**
     * @return null|string
     */
    public function getVin()
    {
        return $this->vin;
    }

    /**
     * @param null|string $vin
     */
    public function setVin($vin)
    {
        $this->vin = $vin;
    }

    /**
     * @return null|string
     */
    public function getLicensePlate()
    {
        return $this->licensePlate;
    }

    /**
     * @param null|string $licensePlate
     */
    public function setLicensePlate($licensePlate)
    {
        $this->licensePlate = $licensePlate;
    }

    /**
     * @return null|string
     */
    public function getStateRegistered()
    {
        return $this->stateRegistered;
    }

    /**
     * @param null|string $stateRegistered
     */
    public function setStateRegistered($stateRegistered)
    {
        $this->stateRegistered = $stateRegistered;
    }


    /**
     * @return null|string
     */
    public function getVendorLocationReference1()
    {
        return $this->vendorLocationReference1;
    }

    /**
     * @param null|string $vendorLocationReference1
     */
    public function setVendorLocationReference1($vendorLocationReference1)
    {
        $this->vendorLocationReference1 = $vendorLocationReference1;
    }

    /**
     * @return null|string
     */
    public function getUniqueReference()
    {
        return $this->uniqueReference;
    }

    /**
     * @param null|string $uniqueReference
     */
    public function setUniqueReference($uniqueReference)
    {
        $this->uniqueReference = $uniqueReference;
    }

    /**
     * @return null|string
     */
    public function getImage01URL()
    {
        return $this->image01URL;
    }

    /**
     * @param null|string $image01URL
     */
    public function setImage01URL($image01URL)
    {
        $this->image01URL = $image01URL;
    }

    /**
     * @return null|string
     */
    public function getImage02URL()
    {
        return $this->image02URL;
    }

    /**
     * @param null|string $image02URL
     */
    public function setImage02URL($image02URL)
    {
        $this->image02URL = $image02URL;
    }

    /**
     * @return null|string
     */
    public function getImage03URL()
    {
        return $this->image03URL;
    }

    /**
     * @param null|string $image03URL
     */
    public function setImage03URL($image03URL)
    {
        $this->image03URL = $image03URL;
    }

    /**
     * @return null|string
     */
    public function getImage04URL()
    {
        return $this->image04URL;
    }

    /**
     * @param null|string $image04URL
     */
    public function setImage04URL($image04URL)
    {
        $this->image04URL = $image04URL;
    }

    /**
     * @return null|string
     */
    public function getImage05URL()
    {
        return $this->image05URL;
    }

    /**
     * @param null|string $image05URL
     */
    public function setImage05URL($image05URL)
    {
        $this->image05URL = $image05URL;
    }

    /**
     * @return null|string
     */
    public function getImage06URL()
    {
        return $this->image06URL;
    }

    /**
     * @param null|string $image06URL
     */
    public function setImage06URL($image06URL)
    {
        $this->image06URL = $image06URL;
    }

    /**
     * @return null|string
     */
    public function getImage07URL()
    {
        return $this->image07URL;
    }

    /**
     * @param null|string $image07URL
     */
    public function setImage07URL($image07URL)
    {
        $this->image07URL = $image07URL;
    }

    /**
     * @return null|string
     */
    public function getImage08URL()
    {
        return $this->image08URL;
    }

    /**
     * @param null|string $image08URL
     */
    public function setImage08URL($image08URL)
    {
        $this->image08URL = $image08URL;
    }

    /**
     * @return null|string
     */
    public function getImage09URL()
    {
        return $this->image09URL;
    }

    /**
     * @param null|string $image09URL
     */
    public function setImage09URL($image09URL)
    {
        $this->image09URL = $image09URL;
    }

    /**
     * @return null|string
     */
    public function getImage10URL()
    {
        return $this->image10URL;
    }

    /**
     * @param null|string $image10URL
     */
    public function setImage10URL($image10URL)
    {
        $this->image10URL = $image10URL;
    }

    /**
     * @return null|string
     */
    public function getImage11URL()
    {
        return $this->image11URL;
    }

    /**
     * @param null|string $image11URL
     */
    public function setImage11URL($image11URL)
    {
        $this->image11URL = $image11URL;
    }

    /**
     * @return null|string
     */
    public function getImage12URL()
    {
        return $this->image12URL;
    }

    /**
     * @param null|string $image12URL
     */
    public function setImage12URL($image12URL)
    {
        $this->image12URL = $image12URL;
    }

    /**
     * @return null|string
     */
    public function getImage13URL()
    {
        return $this->image13URL;
    }

    /**
     * @param null|string $image13URL
     */
    public function setImage13URL($image13URL)
    {
        $this->image13URL = $image13URL;
    }

    /**
     * @return null|string
     */
    public function getImage14URL()
    {
        return $this->image14URL;
    }

    /**
     * @param null|string $image14URL
     */
    public function setImage14URL($image14URL)
    {
        $this->image14URL = $image14URL;
    }

    /**
     * @return null|string
     */
    public function getImage15URL()
    {
        return $this->image15URL;
    }

    /**
     * @param null|string $image15URL
     */
    public function setImage15URL($image15URL)
    {
        $this->image15URL = $image15URL;
    }

    /**
     * @return null|string
     */
    public function getImage16URL()
    {
        return $this->image16URL;
    }

    /**
     * @param null|string $image16URL
     */
    public function setImage16URL($image16URL)
    {
        $this->image16URL = $image16URL;
    }

    /**
     * @return null|string
     */
    public function getImage17URL()
    {
        return $this->image17URL;
    }

    /**
     * @param null|string $image17URL
     */
    public function setImage17URL($image17URL)
    {
        $this->image17URL = $image17URL;
    }

    /**
     * @return null|string
     */
    public function getImage18URL()
    {
        return $this->image18URL;
    }

    /**
     * @param null|string $image18URL
     */
    public function setImage18URL($image18URL)
    {
        $this->image18URL = $image18URL;
    }

    /**
     * @return null|string
     */
    public function getImage19URL()
    {
        return $this->image19URL;
    }

    /**
     * @param null|string $image19URL
     */
    public function setImage19URL($image19URL)
    {
        $this->image19URL = $image19URL;
    }

    /**
     * @return null|string
     */
    public function getImage20URL()
    {
        return $this->image20URL;
    }

    /**
     * @param null|string $image20URL
     */
    public function setImage20URL($image20URL)
    {
        $this->image20URL = $image20URL;
    }

    /**
     * @return null|string
     */
    public function getImage21URL()
    {
        return $this->image21URL;
    }

    /**
     * @param null|string $image21URL
     */
    public function setImage21URL($image21URL)
    {
        $this->image21URL = $image21URL;
    }

    /**
     * @return null|string
     */
    public function getImage22URL()
    {
        return $this->image22URL;
    }

    /**
     * @param null|string $image22URL
     */
    public function setImage22URL($image22URL)
    {
        $this->image22URL = $image22URL;
    }

    /**
     * @return null|string
     */
    public function getImage23URL()
    {
        return $this->image23URL;
    }

    /**
     * @param null|string $image23URL
     */
    public function setImage23URL($image23URL)
    {
        $this->image23URL = $image23URL;
    }

    /**
     * @return null|string
     */
    public function getImage24URL()
    {
        return $this->image24URL;
    }

    /**
     * @param null|string $image24URL
     */
    public function setImage24URL($image24URL)
    {
        $this->image24URL = $image24URL;
    }

    /**
     * @return null|string
     */
    public function getImage25URL()
    {
        return $this->image25URL;
    }

    /**
     * @param null|string $image25URL
     */
    public function setImage25URL($image25URL)
    {
        $this->image25URL = $image25URL;
    }

    /**
     * @return null|string
     */
    public function getImage26URL()
    {
        return $this->image26URL;
    }

    /**
     * @param null|string $image26URL
     */
    public function setImage26URL($image26URL)
    {
        $this->image26URL = $image26URL;
    }

    /**
     * @return null|string
     */
    public function getImage27URL()
    {
        return $this->image27URL;
    }

    /**
     * @param null|string $image27URL
     */
    public function setImage27URL($image27URL)
    {
        $this->image27URL = $image27URL;
    }

    /**
     * @return null|string
     */
    public function getImage28URL()
    {
        return $this->image28URL;
    }

    /**
     * @param null|string $image28URL
     */
    public function setImage28URL($image28URL)
    {
        $this->image28URL = $image28URL;
    }

    /**
     * @return null|string
     */
    public function getImage29URL()
    {
        return $this->image29URL;
    }

    /**
     * @param null|string $image29URL
     */
    public function setImage29URL($image29URL)
    {
        $this->image29URL = $image29URL;
    }

    /**
     * @return null|string
     */
    public function getImage30URL()
    {
        return $this->image30URL;
    }

    /**
     * @param null|string $image30URL
     */
    public function setImage30URL($image30URL)
    {
        $this->image30URL = $image30URL;
    }

    /**
     * @return array
     */
    public function serializeImageUrls()
    {
        $object[]                   = $this->getImage01URL();
        $object[]                   = $this->getImage02URL();
        $object[]                   = $this->getImage03URL();
        $object[]                   = $this->getImage04URL();
        $object[]                   = $this->getImage05URL();
        $object[]                   = $this->getImage06URL();
        $object[]                   = $this->getImage07URL();
        $object[]                   = $this->getImage08URL();
        $object[]                   = $this->getImage09URL();
        $object[]                   = $this->getImage10URL();
        $object[]                   = $this->getImage11URL();
        $object[]                   = $this->getImage12URL();
        $object[]                   = $this->getImage13URL();
        $object[]                   = $this->getImage14URL();
        $object[]                   = $this->getImage15URL();
        $object[]                   = $this->getImage16URL();
        $object[]                   = $this->getImage17URL();
        $object[]                   = $this->getImage18URL();
        $object[]                   = $this->getImage19URL();
        $object[]                   = $this->getImage20URL();
        $object[]                   = $this->getImage21URL();
        $object[]                   = $this->getImage22URL();
        $object[]                   = $this->getImage23URL();
        $object[]                   = $this->getImage24URL();
        $object[]                   = $this->getImage25URL();
        $object[]                   = $this->getImage26URL();
        $object[]                   = $this->getImage27URL();
        $object[]                   = $this->getImage28URL();
        $object[]                   = $this->getImage29URL();
        $object[]                   = $this->getImage30URL();

        return $object;
    }

}