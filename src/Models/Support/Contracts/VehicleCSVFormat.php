<?php

namespace Caravana\Core\Models\Support\Contracts;

interface VehicleCSVFormat extends \JsonSerializable
{
    function getVehicleTypeName();
    function setVehicleTypeName($vehicleTypeName);
    function getVehicleMakeName();
    function setVehicleMakeName($vehicleMakeName);
    function getVehicleModelName();
    function setVehicleModelName($vehicleModelName);
    function getVehicleClassName();
    function setVehicleClassName($vehicleClassName);
    function getFuelTypeName();
    function setFuelTypeName($fuelTypeName);
    function getTransmissionName();
    function setTransmissionName($transmissionName);
    function getAccommodates();
    function setAccommodates($accommodates);
    function getYearCreated();
    function setYearCreated($yearCreated);
    function getFeetLong();
    function setFeetLong($feetLong);
    function getVin();
    function setVin($vin);
    function getLicensePlate();
    function setLicensePlate($licensePlate);
    function getStateRegistered();
    function setStateRegistered($stateRegistered);
    function getVendorLocationReference1();
    function setVendorLocationReference1($vendorLocationReference1);
    function getUniqueReference();
    function setUniqueReference($uniqueReference);
    function getImage01URL();
    function setImage01URL($image01URL);
    function getImage02URL();
    function setImage02URL($image02URL);
    function getImage03URL();
    function setImage03URL($image03URL);
    function getImage04URL();
    function setImage04URL($image04URL);
    function getImage05URL();
    function setImage05URL($image05URL);
    function getImage06URL();
    function setImage06URL($image06URL);
    function getImage07URL();
    function setImage07URL($image07URL);
}