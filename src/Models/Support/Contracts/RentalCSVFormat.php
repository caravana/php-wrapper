<?php

namespace Caravana\Core\Models\Support\Contracts;


interface RentalCSVFormat extends \JsonSerializable
{

    function getRate();

    function setRate($rate);

    function getSecurityDeposit();

    function setSecurityDeposit($securityDeposit);

    function getBookingPolicyName();

    function setBookingPolicyName($bookingPolicyName);

    function getCancellationPolicyName();

    function setCancellationPolicyName($cancellationPolicyName);

    function getFixedMileageFeeId();

    function setFixedMileageFeeId($fixedMileageFeeId);

    function getMinimumDays();

    function setMinimumDays($minimumDays);

}