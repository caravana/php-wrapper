<?php

namespace Caravana\Core\Models\Support;


use Caravana\Core\Models\Requests\Contracts\Validatable;
use Caravana\Core\Models\Support\Base\BaseVehicleCSVFormat;
use Caravana\Core\Validation\BaseVehicleCSVFormatValidation;
use jamesvweston\Utilities\ArrayUtil AS AU;

class VehicleCSVFormat extends BaseVehicleCSVFormat implements Validatable
{

    public function __construct($data = null)
    {
        //  Working with CSVs is not fun...
        if (is_array($data))
        {
            $this->vehicleTypeName              = AU::get($data['vehicleTypeName'], AU::get($data['VehicleTypeName']));
            $this->vehicleTypeName              = empty(trim($this->vehicleTypeName)) ? null : $this->vehicleTypeName;

            $this->vehicleMakeName              = AU::get($data['vehicleMakeName'], AU::get($data['VehicleMakeName']));
            $this->vehicleMakeName              = empty(trim($this->vehicleMakeName)) ? null : $this->vehicleMakeName;
            
            $this->vehicleModelName             = AU::get($data['vehicleModelName'], AU::get($data['VehicleModelName']));
            $this->vehicleModelName             = empty(trim($this->vehicleModelName)) ? null : $this->vehicleModelName;
            
            $this->vehicleClassName             = AU::get($data['vehicleClassName'], AU::get($data['VehicleClassName']));
            $this->vehicleClassName             = empty(trim($this->vehicleClassName)) ? null : $this->vehicleClassName;
            
            $this->fuelTypeName                 = AU::get($data['fuelTypeName'], AU::get($data['FuelTypeName']));
            $this->fuelTypeName                 = empty(trim($this->fuelTypeName)) ? null : $this->fuelTypeName;
            
            $this->transmissionName             = AU::get($data['transmissionName'], AU::get($data['TransmissionName']));
            $this->transmissionName             = empty(trim($this->transmissionName)) ? null : $this->transmissionName;


            $this->accommodates                 = AU::get($data['accommodates'], AU::get($data['Accommodates']));
            $this->accommodates                 = empty(trim($this->accommodates)) ? null : $this->accommodates;


            $this->yearCreated                  = AU::get($data['yearCreated'], AU::get($data['YearCreated']));
            $this->yearCreated                  = empty(trim($this->yearCreated)) ? null : $this->yearCreated;
            
            $this->feetLong                     = AU::get($data['feetLong'], AU::get($data['FeetLong']));
            $this->feetLong                     = empty(trim($this->feetLong)) ? null : $this->feetLong;
            
            $this->vin                          = AU::get($data['vin'], AU::get($data['VIN']));
            $this->vin                          = empty(trim($this->vin)) ? null : $this->vin;

            $this->licensePlate                 = AU::get($data['licensePlate'], AU::get($data['LicensePlate']));
            $this->licensePlate                 = empty(trim($this->licensePlate)) ? null : $this->licensePlate;

            $this->stateRegistered              = AU::get($data['stateRegistered'], AU::get($data['StateRegistered']));
            $this->stateRegistered              = empty(trim($this->stateRegistered)) ? null : $this->stateRegistered;

            $this->vendorLocationReference1     = AU::get($data['vendorLocationReference1'], AU::get($data['VendorLocationReference1']));
            $this->vendorLocationReference1     = empty(trim($this->vendorLocationReference1)) ? null : $this->vendorLocationReference1;

            $this->uniqueReference              = AU::get($data['uniqueReference'], AU::get($data['UniqueReference']));
            $this->uniqueReference              = empty(trim($this->uniqueReference)) ? null : $this->uniqueReference;
            
            $this->image01URL                   = AU::get($data['image01URL'], AU::get($data['Image01URL']));
            $this->image01URL                   = empty(trim($this->image01URL)) ? null : $this->image01URL;
            
            $this->image02URL                   = AU::get($data['image02URL'], AU::get($data['Image02URL']));
            $this->image02URL                   = empty(trim($this->image02URL)) ? null : $this->image02URL;
            
            $this->image03URL                   = AU::get($data['image03URL'], AU::get($data['Image03URL']));
            $this->image03URL                   = empty(trim($this->image03URL)) ? null : $this->image03URL;
            
            $this->image04URL                   = AU::get($data['image04URL'], AU::get($data['Image04URL']));
            $this->image04URL                   = empty(trim($this->image04URL)) ? null : $this->image04URL;
            
            $this->image05URL                   = AU::get($data['image05URL'], AU::get($data['Image05URL']));
            $this->image05URL                   = empty(trim($this->image05URL)) ? null : $this->image05URL;
            
            $this->image06URL                   = AU::get($data['image06URL'], AU::get($data['Image06URL']));
            $this->image06URL                   = empty(trim($this->image06URL)) ? null : $this->image06URL;
            
            $this->image07URL                   = AU::get($data['image07URL'], AU::get($data['Image07URL']));
            $this->image07URL                   = empty(trim($this->image07URL)) ? null : $this->image07URL;
            
            $this->image08URL                   = AU::get($data['image08URL'], AU::get($data['Image08URL']));
            $this->image08URL                   = empty(trim($this->image08URL)) ? null : $this->image08URL;
            
            $this->image09URL                   = AU::get($data['image09URL'], AU::get($data['Image09URL']));
            $this->image09URL                   = empty(trim($this->image09URL)) ? null : $this->image09URL;
            
            $this->image10URL                   = AU::get($data['image10URL'], AU::get($data['Image10URL']));
            $this->image10URL                   = empty(trim($this->image10URL)) ? null : $this->image10URL;
            
            $this->image11URL                   = AU::get($data['image11URL'], AU::get($data['Image11URL']));
            $this->image11URL                   = empty(trim($this->image11URL)) ? null : $this->image11URL;
            
            $this->image12URL                   = AU::get($data['image12URL'], AU::get($data['Image12URL']));
            $this->image12URL                   = empty(trim($this->image12URL)) ? null : $this->image12URL;
            
            $this->image13URL                   = AU::get($data['image13URL'], AU::get($data['Image13URL']));
            $this->image13URL                   = empty(trim($this->image13URL)) ? null : $this->image13URL;
            
            $this->image14URL                   = AU::get($data['image14URL'], AU::get($data['Image14URL']));
            $this->image14URL                   = empty(trim($this->image14URL)) ? null : $this->image14URL;
            
            $this->image15URL                   = AU::get($data['image15URL'], AU::get($data['Image15URL']));
            $this->image15URL                   = empty(trim($this->image15URL)) ? null : $this->image15URL;
            
            $this->image16URL                   = AU::get($data['image16URL'], AU::get($data['Image16URL']));
            $this->image16URL                   = empty(trim($this->image16URL)) ? null : $this->image16URL;
            
            $this->image17URL                   = AU::get($data['image17URL'], AU::get($data['Image17URL']));
            $this->image17URL                   = empty(trim($this->image17URL)) ? null : $this->image17URL;
            
            $this->image18URL                   = AU::get($data['image18URL'], AU::get($data['Image18URL']));
            $this->image18URL                   = empty(trim($this->image18URL)) ? null : $this->image18URL;
            
            $this->image19URL                   = AU::get($data['image19URL'], AU::get($data['Image19URL']));
            $this->image19URL                   = empty(trim($this->image19URL)) ? null : $this->image19URL;
            
            $this->image20URL                   = AU::get($data['image20URL'], AU::get($data['Image20URL']));
            $this->image20URL                   = empty(trim($this->image20URL)) ? null : $this->image20URL;
            
            $this->image21URL                   = AU::get($data['image21URL'], AU::get($data['Image21URL']));
            $this->image21URL                   = empty(trim($this->image21URL)) ? null : $this->image21URL;
            
            $this->image22URL                   = AU::get($data['image22URL'], AU::get($data['Image22URL']));
            $this->image22URL                   = empty(trim($this->image22URL)) ? null : $this->image22URL;
            
            $this->image23URL                   = AU::get($data['image23URL'], AU::get($data['Image23URL']));
            $this->image23URL                   = empty(trim($this->image23URL)) ? null : $this->image23URL;
            
            $this->image24URL                   = AU::get($data['image24URL'], AU::get($data['Image24URL']));
            $this->image24URL                   = empty(trim($this->image24URL)) ? null : $this->image24URL;
            
            $this->image25URL                   = AU::get($data['image25U25'], AU::get($data['Image25U25']));
            $this->image25URL                   = empty(trim($this->image25URL)) ? null : $this->image25URL;
            
            $this->image26URL                   = AU::get($data['image26URL'], AU::get($data['Image26URL']));
            $this->image26URL                   = empty(trim($this->image26URL)) ? null : $this->image26URL;
            
            $this->image27URL                   = AU::get($data['image27URL'], AU::get($data['Image27URL']));
            $this->image27URL                   = empty(trim($this->image27URL)) ? null : $this->image27URL;
            
            $this->image28URL                   = AU::get($data['image28URL'], AU::get($data['Image28URL']));
            $this->image28URL                   = empty(trim($this->image28URL)) ? null : $this->image28URL;
            
            $this->image29URL                   = AU::get($data['image29URL'], AU::get($data['Image29URL']));
            $this->image29URL                   = empty(trim($this->image29URL)) ? null : $this->image29URL;
            
            $this->image30URL                   = AU::get($data['image30URL'], AU::get($data['Image30URL']));
            $this->image30URL                   = empty(trim($this->image30URL)) ? null : $this->image30URL;
        }
    }

    public function validate()
    {
        $vehicleCSVFormatValidation             = new BaseVehicleCSVFormatValidation();
        $vehicleCSVFormatValidation->vehicleTypeName($this->vehicleTypeName);
        $vehicleCSVFormatValidation->vehicleMakeName($this->vehicleMakeName);
        $vehicleCSVFormatValidation->vehicleModelName($this->vehicleModelName);
        $vehicleCSVFormatValidation->vehicleClassName($this->vehicleClassName);
        $vehicleCSVFormatValidation->fuelTypeName($this->fuelTypeName);
        $vehicleCSVFormatValidation->yearCreated($this->yearCreated);
        $vehicleCSVFormatValidation->feetLong($this->feetLong);
    }

    /**
     * @return array
     */
    public function jsonSerialize()
    {
        $object['vehicleTypeName']              = $this->vehicleTypeName;
        $object['vehicleMakeName']              = $this->vehicleMakeName;
        $object['vehicleModelName']             = $this->vehicleModelName;
        $object['vehicleClassName']             = $this->vehicleClassName;
        $object['fuelTypeName']                 = $this->fuelTypeName;
        $object['transmissionName']             = $this->transmissionName;
        $object['accommodates']                 = $this->accommodates;
        $object['yearCreated']                  = $this->yearCreated;
        $object['feetLong']                     = $this->feetLong;
        $object['vin']                          = $this->vin;
        $object['licensePlate']                 = $this->licensePlate;
        $object['stateRegistered']              = $this->stateRegistered;
        $object['vendorLocationReference1']     = $this->vendorLocationReference1;
        $object['uniqueReference']              = $this->uniqueReference;
        $object['image01URL']                   = $this->image01URL;
        $object['image02URL']                   = $this->image02URL;
        $object['image03URL']                   = $this->image03URL;
        $object['image04URL']                   = $this->image04URL;
        $object['image05URL']                   = $this->image05URL;
        $object['image06URL']                   = $this->image06URL;
        $object['image07URL']                   = $this->image07URL;
        $object['image08URL']                   = $this->image08URL;
        $object['image09URL']                   = $this->image09URL;
        $object['image10URL']                   = $this->image10URL;
        $object['image11URL']                   = $this->image11URL;
        $object['image12URL']                   = $this->image12URL;
        $object['image13URL']                   = $this->image13URL;
        $object['image14URL']                   = $this->image14URL;
        $object['image15URL']                   = $this->image15URL;
        $object['image16URL']                   = $this->image16URL;
        $object['image17URL']                   = $this->image17URL;
        $object['image18URL']                   = $this->image18URL;
        $object['image19URL']                   = $this->image19URL;
        $object['image20URL']                   = $this->image20URL;
        $object['image21URL']                   = $this->image21URL;
        $object['image22URL']                   = $this->image22URL;
        $object['image23URL']                   = $this->image23URL;
        $object['image24URL']                   = $this->image24URL;
        $object['image25URL']                   = $this->image25URL;
        $object['image26URL']                   = $this->image26URL;
        $object['image27URL']                   = $this->image27URL;
        $object['image28URL']                   = $this->image28URL;
        $object['image29URL']                   = $this->image29URL;
        $object['image30URL']                   = $this->image30URL;

        return $object;
    }
}