<?php

namespace Caravana\Core\Models\Support;


use Caravana\Core\Models\Requests\Contracts\Validatable;
use Caravana\Core\Models\Support\Contracts\RentalCSVFormat AS RentalCSVFormatContract;
use jamesvweston\Utilities\ArrayUtil AS AU;

class RentalCSVFormat extends VehicleCSVFormat implements RentalCSVFormatContract, Validatable
{

    /**
     * @var string|null
     */
    protected $name;

    /**
     * @var string|null
     */
    protected $description;

    /**
     * @var float|null
     */
    protected $rate;

    /**
     * @var float|null
     */
    protected $securityDeposit;

    /**
     * @var string|null
     */
    protected $bookingPolicyName;

    /**
     * @var string|null
     */
    protected $cancellationPolicyName;

    /**
     * @var int|null
     */
    protected $fixedMileageFeeId;

    /**
     * @var int|null
     */
    protected $minimumDays;


    public function __construct($data = null)
    {
        parent::__construct($data);

        $this->name                         = AU::get($data['name'], AU::get($data['Name']));
        $this->name                         = empty(trim($this->name)) ? null : $this->name;

        $this->description                  = AU::get($data['description'], AU::get($data['Description']));
        $this->description                  = empty(trim($this->description)) ? null : $this->description;


        $this->bookingPolicyName            = AU::get($data['bookingPolicyName'], AU::get($data['BookingPolicyName']));
        $this->bookingPolicyName            = empty(trim($this->bookingPolicyName)) ? null : $this->bookingPolicyName;

        $this->cancellationPolicyName       = AU::get($data['cancellationPolicyName'], AU::get($data['CancellationPolicyName']));
        $this->cancellationPolicyName       = empty(trim($this->cancellationPolicyName)) ? null : $this->cancellationPolicyName;


        $this->rate                         = AU::get($data['rate'], AU::get($data['Rate']));
        $this->rate                         = empty(trim($this->rate)) ? null : $this->rate;

        $this->fixedMileageFeeId            = AU::get($data['fixedMileageFeeId'], AU::get($data['FixedMileageFeeId']));
        $this->fixedMileageFeeId            = empty(trim($this->fixedMileageFeeId)) ? null : $this->fixedMileageFeeId;


        $this->minimumDays                  = AU::get($data['minimumDays'], AU::get($data['MinimumDays']));
        $this->minimumDays                  = empty(trim($this->minimumDays)) ? null : $this->minimumDays;


        $this->securityDeposit              = AU::get($data['securityDeposit'], AU::get($data['SecurityDeposit']));
        $this->securityDeposit              = empty(trim($this->securityDeposit)) ? null : $this->securityDeposit;
    }

    public function validate()
    {
        parent::validate();
    }

    /**
     * @return array
     */
    public function jsonSerialize()
    {
        $object                             = parent::jsonSerialize();
        $object['name']                     = $this->name;
        $object['description']              = $this->description;
        $object['bookingPolicyName']        = $this->bookingPolicyName;
        $object['cancellationPolicyName']   = $this->cancellationPolicyName;
        $object['rate']                     = $this->rate;
        $object['fixedMileageFeeId']        = $this->fixedMileageFeeId;
        $object['minimumDays']              = $this->minimumDays;
        $object['securityDeposit']          = $this->securityDeposit;

        return $object;
    }

    /**
     * @return null|string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param null|string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return null|string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param null|string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return float|null
     */
    public function getRate()
    {
        return $this->rate;
    }

    /**
     * @param float|null $rate
     */
    public function setRate($rate)
    {
        $this->rate = $rate;
    }

    /**
     * @return float|null
     */
    public function getSecurityDeposit()
    {
        return $this->securityDeposit;
    }

    /**
     * @param float|null $securityDeposit
     */
    public function setSecurityDeposit($securityDeposit)
    {
        $this->securityDeposit = $securityDeposit;
    }

    /**
     * @return null|string
     */
    public function getBookingPolicyName()
    {
        return $this->bookingPolicyName;
    }

    /**
     * @param null|string $bookingPolicyName
     */
    public function setBookingPolicyName($bookingPolicyName)
    {
        $this->bookingPolicyName = $bookingPolicyName;
    }

    /**
     * @return null|string
     */
    public function getCancellationPolicyName()
    {
        return $this->cancellationPolicyName;
    }

    /**
     * @param null|string $cancellationPolicyName
     */
    public function setCancellationPolicyName($cancellationPolicyName)
    {
        $this->cancellationPolicyName = $cancellationPolicyName;
    }

    /**
     * @return int|null
     */
    public function getMinimumDays()
    {
        return $this->minimumDays;
    }

    /**
     * @param int|null $minimumDays
     */
    public function setMinimumDays($minimumDays)
    {
        $this->minimumDays = $minimumDays;
    }

    /**
     * @return int|null
     */
    public function getFixedMileageFeeId()
    {
        return $this->fixedMileageFeeId;
    }

    /**
     * @param int|null $fixedMileageFeeId
     */
    public function setFixedMileageFeeId($fixedMileageFeeId)
    {
        $this->fixedMileageFeeId = $fixedMileageFeeId;
    }




}